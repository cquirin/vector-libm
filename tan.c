/* This program implements a show-case vector (vectorizable) double
   precision tangent with a 8 ulp error bound.

   Author: Christoph Lauter,

           Sorbonne Université - LIP6 - PEQUAN team.

   This program uses code generated using Sollya and Metalibm; see the
   licences and exception texts below.

   This program is

   Copyright 2014-2018 Christoph Lauter Sorbonne Université

   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions
   are met:

   1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.

   2. Redistributions in binary form must reproduce the above
   copyright notice, this list of conditions and the following
   disclaimer in the documentation and/or other materials provided
   with the distribution.

   3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived
   from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
   FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
   COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
   INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
   (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
   SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
   HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
   STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
   OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*

    This code was generated using non-trivial code generation commands
    of the Metalibm software program.

    Before using, modifying and/or integrating this code into other
    software, review the copyright and license status of this
    generated code. In particular, see the exception below.

    This generated program is partly or entirely based on a program
    generated using non-trivial code generation commands of the Sollya
    software program. See the copyright notice and exception text
    referring to that Sollya-generated part of this program generated
    with Metalibm below.

    Metalibm is

    Copyright 2008-2013 by

    Laboratoire de l'Informatique du Parallélisme,
    UMR CNRS - ENS Lyon - UCB Lyon 1 - INRIA 5668

    and by

    Laboratoire d'Informatique de Paris 6, equipe PEQUAN,
    UPMC Universite Paris 06 - CNRS - UMR 7606 - LIP6, Paris, France.

    Contributors: Christoph Quirin Lauter
                  (UPMC LIP6 PEQUAN formerly LIP/ENS Lyon)
                  christoph.lauter@lip6.fr

                  and

                  Olga Kupriianova
                  (UPMC LIP6 PEQUAN)
                  olga.kupriianova@lip6.fr

    Metalibm was formerly developed by the Arenaire project at Ecole
    Normale Superieure de Lyon and is now developed by Equipe PEQUAN
    at Universite Pierre et Marie Curie Paris 6.

    The Metalibm software program is free software; you can
    redistribute it and/or modify it under the terms of the GNU Lesser
    General Public License as published by the Free Software
    Foundation; either version 2 of the License, or (at your option)
    any later version.

    Metalibm is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with the Metalibm program; if not, write to the Free
    Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
    02111-1307, USA.

    This generated program is distributed WITHOUT ANY WARRANTY; without
    even the implied warranty of MERCHANTABILITY or FITNESS FOR A
    PARTICULAR PURPOSE.

    As a special exception, you may create a larger work that contains
    part or all of this software generated using Metalibm and
    distribute that work under terms of your choice, so long as that
    work isn't itself a numerical code generator using the skeleton of
    this code or a modified version thereof as a code skeleton.
    Alternatively, if you modify or redistribute this generated code
    itself, or its skeleton, you may (at your option) remove this
    special exception, which will cause this generated code and its
    skeleton and the resulting Metalibm output files to be licensed
    under the General Public licence (version 2) without this special
    exception.

    This special exception was added by the Metalibm copyright holders
    on November 20th 2013.

*/



/*
    This code was generated using non-trivial code generation commands of
    the Sollya software program.

    Before using, modifying and/or integrating this code into other
    software, review the copyright and license status of this generated
    code. In particular, see the exception below.

    Sollya is

    Copyright 2006-2013 by

    Laboratoire de l'Informatique du Parallelisme, UMR CNRS - ENS Lyon -
    UCB Lyon 1 - INRIA 5668,

    Laboratoire d'Informatique de Paris 6, equipe PEQUAN, UPMC Universite
    Paris 06 - CNRS - UMR 7606 - LIP6, Paris, France

    and by

    Centre de recherche INRIA Sophia-Antipolis Mediterranee, equipe APICS,
    Sophia Antipolis, France.

    Contributors Ch. Lauter, S. Chevillard, M. Joldes

    christoph.lauter@ens-lyon.org
    sylvain.chevillard@ens-lyon.org
    joldes@lass.fr

    The Sollya software is a computer program whose purpose is to provide
    an environment for safe floating-point code development. It is
    particularily targeted to the automatized implementation of
    mathematical floating-point libraries (libm). Amongst other features,
    it offers a certified infinity norm, an automatic polynomial
    implementer and a fast Remez algorithm.

    The Sollya software is governed by the CeCILL-C license under French
    law and abiding by the rules of distribution of free software.  You
    can use, modify and/ or redistribute the software under the terms of
    the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
    following URL "http://www.cecill.info".

    As a counterpart to the access to the source code and rights to copy,
    modify and redistribute granted by the license, users are provided
    only with a limited warranty and the software's author, the holder of
    the economic rights, and the successive licensors have only limited
    liability.

    In this respect, the user's attention is drawn to the risks associated
    with loading, using, modifying and/or developing or reproducing the
    software by the user in light of its specific status of free software,
    that may mean that it is complicated to manipulate, and that also
    therefore means that it is reserved for developers and experienced
    professionals having in-depth computer knowledge. Users are therefore
    encouraged to load and test the software's suitability as regards
    their requirements in conditions enabling the security of their
    systems and/or data to be ensured and, more generally, to use and
    operate it in the same conditions as regards security.

    The fact that you are presently reading this means that you have had
    knowledge of the CeCILL-C license and that you accept its terms.

    The Sollya program is distributed WITHOUT ANY WARRANTY; without even
    the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
    PURPOSE.

    This generated program is distributed WITHOUT ANY WARRANTY; without
    even the implied warranty of MERCHANTABILITY or FITNESS FOR A
    PARTICULAR PURPOSE.

    As a special exception, you may create a larger work that contains
    part or all of this software generated using Sollya and distribute
    that work under terms of your choice, so long as that work isn't
    itself a numerical code generator using the skeleton of this code or a
    modified version thereof as a code skeleton.  Alternatively, if you
    modify or redistribute this generated code itself, or its skeleton,
    you may (at your option) remove this special exception, which will
    cause this generated code and its skeleton and the resulting Sollya
    output files to be licensed under the CeCILL-C licence without this
    special exception.

    This special exception was added by the Sollya copyright holders in
    version 4.1 of Sollya.

*/

#include <stdint.h>
#include "trigo.h"
#include "tan.h"

/* Two caster types */
typedef union _dblcast {
  double   d;
  uint64_t i;
} dblcast;

typedef union {
  int64_t l;
  double d;
} db_number;

/* Macro implementations of some double-double operations */
#define Add12(s, r, a, b)                       \
  {double _z, _a=a, _b=b;                       \
    s = _a + _b;                                \
    _z = s - _a;                                \
    r = _b - _z;   }

#define Add12Cond(s, r, a, b)                   \
  {                                             \
    double _u1, _u2, _u3, _u4;                  \
    double  _a=a, _b=b;                         \
                                                \
    s = _a + _b;                                \
    _u1 = s - _a;                               \
    _u2 = s - _u1;                              \
    _u3 = _b - _u1;                             \
    _u4 = _a - _u2;                             \
    r = _u4 + _u3;                              \
  }

#define Mul12(rh,rl,u,v)                                \
  {                                                     \
    CONST double c  = 134217729.; /* 2^27 +1 */         \
    double up, u1, u2, vp, v1, v2;                      \
    double _u =u, _v=v;                                 \
                                                        \
    up = _u*c;        vp = _v*c;                        \
    u1 = (_u-up)+up;  v1 = (_v-vp)+vp;                  \
    u2 = _u-u1;       v2 = _v-v1;                       \
                                                        \
    *rh = _u*_v;                                        \
    *rl = (((u1*v1-*rh)+(u1*v2))+(u2*v1))+(u2*v2);      \
  }

#define Mul122(resh,resl,a,bh,bl)               \
  {                                             \
    double _t1, _t2, _t3, _t4;                  \
                                                \
    Mul12(&_t1,&_t2,(a),(bh));                  \
    _t3 = (a) * (bl);                           \
    _t4 = _t2 + _t3;                            \
    Add12((*(resh)),(*(resl)),_t1,_t4);         \
  }

#define Mul22(zh,zl,xh,xl,yh,yl)                        \
  {                                                     \
    double mh, ml;                                      \
                                                        \
    CONST double c = 134217729.;                        \
    double up, u1, u2, vp, v1, v2;                      \
                                                        \
    up = (xh)*c;        vp = (yh)*c;                    \
    u1 = ((xh)-up)+up;  v1 = ((yh)-vp)+vp;              \
    u2 = (xh)-u1;       v2 = (yh)-v1;                   \
                                                        \
    mh = (xh)*(yh);                                     \
    ml = (((u1*v1-mh)+(u1*v2))+(u2*v1))+(u2*v2);        \
                                                        \
    ml += (xh)*(yl) + (xl)*(yh);                        \
    *zh = mh+ml;                                        \
    *zl = mh - (*zh) + ml;                              \
  }

#define Add122(resh,resl,a,bh,bl)               \
  {                                             \
    double _t1, _t2, _t3;                       \
                                                \
    Add12(_t1,_t2,(a),(bh));                    \
    _t3 = _t2 + (bl);                           \
    Add12((*(resh)),(*(resl)),_t1,_t3);         \
  }

#define Renormalize3(resh, resm, resl, ah, am, al)      \
  {                                                     \
    double _t1h, _t1l, _t2l;                            \
                                                        \
    Add12(_t1h, _t1l, (am), (al));                      \
    Add12((*(resh)), _t2l, (ah), (_t1h));               \
    Add12((*(resm)), (*(resl)), _t2l, _t1l);            \
  }

#define Mul133(resh, resm, resl, a, bh, bm, bl)         \
  {                                                     \
    double _t2, _t3, _t4, _t5, _t7, _t8, _t9, _t10;     \
                                                        \
    Mul12((resh),&_t2,(a),(bh));                        \
    Mul12(&_t3,&_t4,(a),(bm));                          \
    _t5 = (a) * (bl);                                   \
    Add12Cond(_t9,_t7,_t2,_t3);                         \
    _t8 = _t4 + _t5;                                    \
    _t10 = _t7 + _t8;                                   \
    Add12Cond((*(resm)),(*(resl)),_t9,_t10);            \
  }

#define MulAdd22(resh,resl,ch,cl,ah,al,bh,bl)           \
  {                                                     \
    double _t1, _t2, _t3, _t4, _t5, _t6, _t7, _t8;      \
    double _t9, _t10;                                   \
                                                        \
    Mul12(&_t1,&_t2,(ah),(bh));                         \
    Add12(_t3,_t4,(ch),_t1);                            \
    _t5 = (ah) * (bl);                                  \
    _t6 = (al) * (bh);                                  \
    _t7 = _t2 + (cl);                                   \
    _t8 = _t4 + _t7;                                    \
    _t9 = _t5 + _t6;                                    \
    _t10 = _t8 + _t9;                                   \
    Add12((*(resh)),(*(resl)),_t3,_t10);                \
  }

/* Need fabs */
double fabs(double);


/* Some constants */
#define TANGENT_EASY_PATH_BOUND             2.573592701820758520625531673431396484375e4
#define TANGENT_INPUT_UNDERFLOW_BOUND       3.0549363634996046820519793932136176997894027405723e-151
#define TANGENT_RCPR_PI                     0.318309886183790691216444201927515678107738494873047
#define SHIFTER                             6755399441055744.0
#define TANGENT_PI_HI                       3.1415926535919425077736377716064453125                     /* 14 trailing zeros in double precision */
#define TANGENT_PI_MI                       (-2.1492693109956619903751304928585241782457160297781e-12)  /* 14 trailing zeros in double precision */
#define TANGENT_PI_LO                       1.27366343270274062722736216588063328297815573234383e-24    /* 14 trailing zeros in double precision */
#define TANGENT_ARG_RED_BOUND               0.80000000000000004440892098500626161694526672363281
#define SCALAR_TANGENT_SHORT_PATH           0x3e50000000000000ull                                       /* 2^-26 */
#define SCALAR_TANGENT_NO_ARG_RED_BOUND     0.80000000000000004440892098500626161694526672363281
#define SCALAR_TANGENT_CONST_PI             3.141592653589793115997963468544185161590576171875
#define SCALAR_TANGENT_NEAREST_INT_TWO_53   9007199254740992.0
#define SCALAR_TANGENT_NEAREST_INT_TWO_52   4503599627370496.0
#define TANGENT_SHIFT_PI_HALF               1.5707963267948965579989817342720925807952880859375
#define TANGENT_TAN_OF_TANGENT_SHIFT_PI_HALF 6.1232339957367660358688201472919830231284606233879e-17

/* A generated polynomial for the scalar callout */
#define scalar_tan_callout_poly_inner_coeff_1h 1.00000000000000000000000000000000000000000000000000000000000000000000000000000000e+00
#define scalar_tan_callout_poly_inner_coeff_3h 3.33333333333334813630699500208720564842224121093750000000000000000000000000000000e-01
#define scalar_tan_callout_poly_inner_coeff_5h 1.33333333333094578021515985710720997303724288940429687500000000000000000000000000e-01
#define scalar_tan_callout_poly_inner_coeff_7h 5.39682539812406103507669286045711487531661987304687500000000000000000000000000000e-02
#define scalar_tan_callout_poly_inner_coeff_9h 2.18694881815044775419387690362782450392842292785644531250000000000000000000000000e-02
#define scalar_tan_callout_poly_inner_coeff_11h 8.86324126882762249046709968069990281946957111358642578125000000000000000000000000e-03
#define scalar_tan_callout_poly_inner_coeff_13h 3.59206775054937829314005703906786948209628462791442871093750000000000000000000000e-03
#define scalar_tan_callout_poly_inner_coeff_15h 1.45626877224202242767925330468870015465654432773590087890625000000000000000000000e-03
#define scalar_tan_callout_poly_inner_coeff_17h 5.87806910842813895232139564939188858261331915855407714843750000000000000000000000e-04
#define scalar_tan_callout_poly_inner_coeff_19h 2.47337404345491151599900625512873375555500388145446777343750000000000000000000000e-04
#define scalar_tan_callout_poly_inner_coeff_21h 7.48148386050411683600644408009827657224377617239952087402343750000000000000000000e-05
#define scalar_tan_callout_poly_inner_coeff_23h 8.24277960675155930590427000304032389976782724261283874511718750000000000000000000e-05
#define scalar_tan_callout_poly_inner_coeff_25h -4.40352618393067142110036682467466562229674309492111206054687500000000000000000000e-05
#define scalar_tan_callout_poly_inner_coeff_27h 6.33593130553556602721845814052414880279684439301490783691406250000000000000000000e-05
#define scalar_tan_callout_poly_inner_coeff_29h -3.12752180059639158589586405145865910526481457054615020751953125000000000000000000e-05
#define scalar_tan_callout_poly_inner_coeff_30h -8.11554440808171956604098374073031135835054974449198628150270451442338526248931885e-16
#define scalar_tan_callout_poly_inner_coeff_31h 1.12971475271700765312561653974654518606257624924182891845703125000000000000000000e-05


STATIC INLINE void scalar_tan_callout_poly_inner(double * RESTRICT scalar_tan_callout_poly_inner_resh, double * RESTRICT scalar_tan_callout_poly_inner_resm, double x) {
  double scalar_tan_callout_poly_inner_x_0_pow2h, scalar_tan_callout_poly_inner_x_0_pow2m;


  Mul12(&scalar_tan_callout_poly_inner_x_0_pow2h,&scalar_tan_callout_poly_inner_x_0_pow2m,x,x);


  double scalar_tan_callout_poly_inner_t_1_0h;
  double scalar_tan_callout_poly_inner_t_2_0h;
  double scalar_tan_callout_poly_inner_t_3_0h;
  double scalar_tan_callout_poly_inner_t_4_0h;
  double scalar_tan_callout_poly_inner_t_5_0h;
  double scalar_tan_callout_poly_inner_t_6_0h;
  double scalar_tan_callout_poly_inner_t_7_0h;
  double scalar_tan_callout_poly_inner_t_8_0h;
  double scalar_tan_callout_poly_inner_t_9_0h;
  double scalar_tan_callout_poly_inner_t_10_0h;
  double scalar_tan_callout_poly_inner_t_11_0h;
  double scalar_tan_callout_poly_inner_t_12_0h;
  double scalar_tan_callout_poly_inner_t_13_0h;
  double scalar_tan_callout_poly_inner_t_14_0h;
  double scalar_tan_callout_poly_inner_t_15_0h;
  double scalar_tan_callout_poly_inner_t_16_0h;
  double scalar_tan_callout_poly_inner_t_17_0h;
  double scalar_tan_callout_poly_inner_t_18_0h;
  double scalar_tan_callout_poly_inner_t_19_0h;
  double scalar_tan_callout_poly_inner_t_20_0h;
  double scalar_tan_callout_poly_inner_t_21_0h;
  double scalar_tan_callout_poly_inner_t_22_0h;
  double scalar_tan_callout_poly_inner_t_23_0h;
  double scalar_tan_callout_poly_inner_t_24_0h;
  double scalar_tan_callout_poly_inner_t_25_0h;
  double scalar_tan_callout_poly_inner_t_26_0h;
  double scalar_tan_callout_poly_inner_t_27_0h;
  double scalar_tan_callout_poly_inner_t_28_0h;
  double scalar_tan_callout_poly_inner_t_29_0h;
  double scalar_tan_callout_poly_inner_t_30_0h;
  double scalar_tan_callout_poly_inner_t_31_0h, scalar_tan_callout_poly_inner_t_31_0m;
  double scalar_tan_callout_poly_inner_t_32_0h, scalar_tan_callout_poly_inner_t_32_0m;
  double scalar_tan_callout_poly_inner_t_33_0h, scalar_tan_callout_poly_inner_t_33_0m;
  double scalar_tan_callout_poly_inner_t_34_0h, scalar_tan_callout_poly_inner_t_34_0m;
 


  scalar_tan_callout_poly_inner_t_1_0h = scalar_tan_callout_poly_inner_coeff_31h;
  scalar_tan_callout_poly_inner_t_2_0h = scalar_tan_callout_poly_inner_t_1_0h * x;
  scalar_tan_callout_poly_inner_t_3_0h = scalar_tan_callout_poly_inner_coeff_30h + scalar_tan_callout_poly_inner_t_2_0h;
  scalar_tan_callout_poly_inner_t_4_0h = scalar_tan_callout_poly_inner_t_3_0h * x;
  scalar_tan_callout_poly_inner_t_5_0h = scalar_tan_callout_poly_inner_coeff_29h + scalar_tan_callout_poly_inner_t_4_0h;
  scalar_tan_callout_poly_inner_t_6_0h = scalar_tan_callout_poly_inner_t_5_0h * scalar_tan_callout_poly_inner_x_0_pow2h;
  scalar_tan_callout_poly_inner_t_7_0h = scalar_tan_callout_poly_inner_coeff_27h + scalar_tan_callout_poly_inner_t_6_0h;
  scalar_tan_callout_poly_inner_t_8_0h = scalar_tan_callout_poly_inner_t_7_0h * scalar_tan_callout_poly_inner_x_0_pow2h;
  scalar_tan_callout_poly_inner_t_9_0h = scalar_tan_callout_poly_inner_coeff_25h + scalar_tan_callout_poly_inner_t_8_0h;
  scalar_tan_callout_poly_inner_t_10_0h = scalar_tan_callout_poly_inner_t_9_0h * scalar_tan_callout_poly_inner_x_0_pow2h;
  scalar_tan_callout_poly_inner_t_11_0h = scalar_tan_callout_poly_inner_coeff_23h + scalar_tan_callout_poly_inner_t_10_0h;
  scalar_tan_callout_poly_inner_t_12_0h = scalar_tan_callout_poly_inner_t_11_0h * scalar_tan_callout_poly_inner_x_0_pow2h;
  scalar_tan_callout_poly_inner_t_13_0h = scalar_tan_callout_poly_inner_coeff_21h + scalar_tan_callout_poly_inner_t_12_0h;
  scalar_tan_callout_poly_inner_t_14_0h = scalar_tan_callout_poly_inner_t_13_0h * scalar_tan_callout_poly_inner_x_0_pow2h;
  scalar_tan_callout_poly_inner_t_15_0h = scalar_tan_callout_poly_inner_coeff_19h + scalar_tan_callout_poly_inner_t_14_0h;
  scalar_tan_callout_poly_inner_t_16_0h = scalar_tan_callout_poly_inner_t_15_0h * scalar_tan_callout_poly_inner_x_0_pow2h;
  scalar_tan_callout_poly_inner_t_17_0h = scalar_tan_callout_poly_inner_coeff_17h + scalar_tan_callout_poly_inner_t_16_0h;
  scalar_tan_callout_poly_inner_t_18_0h = scalar_tan_callout_poly_inner_t_17_0h * scalar_tan_callout_poly_inner_x_0_pow2h;
  scalar_tan_callout_poly_inner_t_19_0h = scalar_tan_callout_poly_inner_coeff_15h + scalar_tan_callout_poly_inner_t_18_0h;
  scalar_tan_callout_poly_inner_t_20_0h = scalar_tan_callout_poly_inner_t_19_0h * scalar_tan_callout_poly_inner_x_0_pow2h;
  scalar_tan_callout_poly_inner_t_21_0h = scalar_tan_callout_poly_inner_coeff_13h + scalar_tan_callout_poly_inner_t_20_0h;
  scalar_tan_callout_poly_inner_t_22_0h = scalar_tan_callout_poly_inner_t_21_0h * scalar_tan_callout_poly_inner_x_0_pow2h;
  scalar_tan_callout_poly_inner_t_23_0h = scalar_tan_callout_poly_inner_coeff_11h + scalar_tan_callout_poly_inner_t_22_0h;
  scalar_tan_callout_poly_inner_t_24_0h = scalar_tan_callout_poly_inner_t_23_0h * scalar_tan_callout_poly_inner_x_0_pow2h;
  scalar_tan_callout_poly_inner_t_25_0h = scalar_tan_callout_poly_inner_coeff_9h + scalar_tan_callout_poly_inner_t_24_0h;
  scalar_tan_callout_poly_inner_t_26_0h = scalar_tan_callout_poly_inner_t_25_0h * scalar_tan_callout_poly_inner_x_0_pow2h;
  scalar_tan_callout_poly_inner_t_27_0h = scalar_tan_callout_poly_inner_coeff_7h + scalar_tan_callout_poly_inner_t_26_0h;
  scalar_tan_callout_poly_inner_t_28_0h = scalar_tan_callout_poly_inner_t_27_0h * scalar_tan_callout_poly_inner_x_0_pow2h;
  scalar_tan_callout_poly_inner_t_29_0h = scalar_tan_callout_poly_inner_coeff_5h + scalar_tan_callout_poly_inner_t_28_0h;
  scalar_tan_callout_poly_inner_t_30_0h = scalar_tan_callout_poly_inner_t_29_0h * scalar_tan_callout_poly_inner_x_0_pow2h;
  Add12(scalar_tan_callout_poly_inner_t_31_0h,scalar_tan_callout_poly_inner_t_31_0m,scalar_tan_callout_poly_inner_coeff_3h,scalar_tan_callout_poly_inner_t_30_0h);
  Mul22(&scalar_tan_callout_poly_inner_t_32_0h,&scalar_tan_callout_poly_inner_t_32_0m,scalar_tan_callout_poly_inner_t_31_0h,scalar_tan_callout_poly_inner_t_31_0m,scalar_tan_callout_poly_inner_x_0_pow2h,scalar_tan_callout_poly_inner_x_0_pow2m);
  Add122(&scalar_tan_callout_poly_inner_t_33_0h,&scalar_tan_callout_poly_inner_t_33_0m,scalar_tan_callout_poly_inner_coeff_1h,scalar_tan_callout_poly_inner_t_32_0h,scalar_tan_callout_poly_inner_t_32_0m);
  Mul122(&scalar_tan_callout_poly_inner_t_34_0h,&scalar_tan_callout_poly_inner_t_34_0m,x,scalar_tan_callout_poly_inner_t_33_0h,scalar_tan_callout_poly_inner_t_33_0m);
  *scalar_tan_callout_poly_inner_resh = scalar_tan_callout_poly_inner_t_34_0h; *scalar_tan_callout_poly_inner_resm = scalar_tan_callout_poly_inner_t_34_0m;


}

STATIC INLINE double scalar_tan_callout_poly(double x) {
  double yh, yl;

  scalar_tan_callout_poly_inner(&yh, &yl, x);

  return yh + yl;
}

/* A generated polynomial for the scalar callout - the approximated function actually is a sine */
#define scalar_sin_callout_poly_inner_coeff_1h 1.00000000000000000000000000000000000000000000000000000000000000000000000000000000e+00
#define scalar_sin_callout_poly_inner_coeff_1m -1.62630325872825665101117920130491256713867187500000000000000000000000000000000000e-19
#define scalar_sin_callout_poly_inner_coeff_3h -1.66666666666666657414808128123695496469736099243164062500000000000000000000000000e-01
#define scalar_sin_callout_poly_inner_coeff_5h 8.33333333333324301206435080757728428579866886138916015625000000000000000000000000e-03
#define scalar_sin_callout_poly_inner_coeff_7h -1.98412698412288475684900346251993141777347773313522338867187500000000000000000000e-04
#define scalar_sin_callout_poly_inner_coeff_9h 2.75573192147396554477342331401779063071444397792220115661621093750000000000000000e-06
#define scalar_sin_callout_poly_inner_coeff_11h -2.50521072192190611285147469134218511932488127058604732155799865722656250000000000e-08
#define scalar_sin_callout_poly_inner_coeff_13h 1.60589577762058795605632216422847206166446554220783582422882318496704101562500000e-10
#define scalar_sin_callout_poly_inner_coeff_15h -7.64347514877658421132747122591473231236272600774839247605996206402778625488281250e-13
#define scalar_sin_callout_poly_inner_coeff_16h 7.40291633116420616644505617330616528362248819307330864338104236058168972078874503e-24
#define scalar_sin_callout_poly_inner_coeff_17h 2.72637469088703711893726261136987611880097472777384481901208346243947744369506836e-15


STATIC INLINE void scalar_sin_callout_poly_inner(double * RESTRICT scalar_sin_callout_poly_inner_resh, double * RESTRICT scalar_sin_callout_poly_inner_resm, double x) {
  double scalar_sin_callout_poly_inner_x_0_pow2h, scalar_sin_callout_poly_inner_x_0_pow2m;


  Mul12(&scalar_sin_callout_poly_inner_x_0_pow2h,&scalar_sin_callout_poly_inner_x_0_pow2m,x,x);


  double scalar_sin_callout_poly_inner_t_1_0h;
  double scalar_sin_callout_poly_inner_t_2_0h;
  double scalar_sin_callout_poly_inner_t_3_0h;
  double scalar_sin_callout_poly_inner_t_4_0h;
  double scalar_sin_callout_poly_inner_t_5_0h;
  double scalar_sin_callout_poly_inner_t_6_0h;
  double scalar_sin_callout_poly_inner_t_7_0h;
  double scalar_sin_callout_poly_inner_t_8_0h;
  double scalar_sin_callout_poly_inner_t_9_0h;
  double scalar_sin_callout_poly_inner_t_10_0h;
  double scalar_sin_callout_poly_inner_t_11_0h;
  double scalar_sin_callout_poly_inner_t_12_0h;
  double scalar_sin_callout_poly_inner_t_13_0h;
  double scalar_sin_callout_poly_inner_t_14_0h;
  double scalar_sin_callout_poly_inner_t_15_0h;
  double scalar_sin_callout_poly_inner_t_16_0h;
  double scalar_sin_callout_poly_inner_t_17_0h, scalar_sin_callout_poly_inner_t_17_0m;
  double scalar_sin_callout_poly_inner_t_18_0h, scalar_sin_callout_poly_inner_t_18_0m;
  double scalar_sin_callout_poly_inner_t_19_0h, scalar_sin_callout_poly_inner_t_19_0m;



  scalar_sin_callout_poly_inner_t_1_0h = scalar_sin_callout_poly_inner_coeff_17h;
  scalar_sin_callout_poly_inner_t_2_0h = scalar_sin_callout_poly_inner_t_1_0h * x;
  scalar_sin_callout_poly_inner_t_3_0h = scalar_sin_callout_poly_inner_coeff_16h + scalar_sin_callout_poly_inner_t_2_0h;
  scalar_sin_callout_poly_inner_t_4_0h = scalar_sin_callout_poly_inner_t_3_0h * x;
  scalar_sin_callout_poly_inner_t_5_0h = scalar_sin_callout_poly_inner_coeff_15h + scalar_sin_callout_poly_inner_t_4_0h;
  scalar_sin_callout_poly_inner_t_6_0h = scalar_sin_callout_poly_inner_t_5_0h * scalar_sin_callout_poly_inner_x_0_pow2h;
  scalar_sin_callout_poly_inner_t_7_0h = scalar_sin_callout_poly_inner_coeff_13h + scalar_sin_callout_poly_inner_t_6_0h;
  scalar_sin_callout_poly_inner_t_8_0h = scalar_sin_callout_poly_inner_t_7_0h * scalar_sin_callout_poly_inner_x_0_pow2h;
  scalar_sin_callout_poly_inner_t_9_0h = scalar_sin_callout_poly_inner_coeff_11h + scalar_sin_callout_poly_inner_t_8_0h;
  scalar_sin_callout_poly_inner_t_10_0h = scalar_sin_callout_poly_inner_t_9_0h * scalar_sin_callout_poly_inner_x_0_pow2h;
  scalar_sin_callout_poly_inner_t_11_0h = scalar_sin_callout_poly_inner_coeff_9h + scalar_sin_callout_poly_inner_t_10_0h;
  scalar_sin_callout_poly_inner_t_12_0h = scalar_sin_callout_poly_inner_t_11_0h * scalar_sin_callout_poly_inner_x_0_pow2h;
  scalar_sin_callout_poly_inner_t_13_0h = scalar_sin_callout_poly_inner_coeff_7h + scalar_sin_callout_poly_inner_t_12_0h;
  scalar_sin_callout_poly_inner_t_14_0h = scalar_sin_callout_poly_inner_t_13_0h * scalar_sin_callout_poly_inner_x_0_pow2h;
  scalar_sin_callout_poly_inner_t_15_0h = scalar_sin_callout_poly_inner_coeff_5h + scalar_sin_callout_poly_inner_t_14_0h;
  scalar_sin_callout_poly_inner_t_16_0h = scalar_sin_callout_poly_inner_t_15_0h * scalar_sin_callout_poly_inner_x_0_pow2h;
  Add12(scalar_sin_callout_poly_inner_t_17_0h,scalar_sin_callout_poly_inner_t_17_0m,scalar_sin_callout_poly_inner_coeff_3h,scalar_sin_callout_poly_inner_t_16_0h);
  MulAdd22(&scalar_sin_callout_poly_inner_t_18_0h,&scalar_sin_callout_poly_inner_t_18_0m,scalar_sin_callout_poly_inner_coeff_1h,scalar_sin_callout_poly_inner_coeff_1m,scalar_sin_callout_poly_inner_x_0_pow2h,scalar_sin_callout_poly_inner_x_0_pow2m,scalar_sin_callout_poly_inner_t_17_0h,scalar_sin_callout_poly_inner_t_17_0m);
  Mul122(&scalar_sin_callout_poly_inner_t_19_0h,&scalar_sin_callout_poly_inner_t_19_0m,x,scalar_sin_callout_poly_inner_t_18_0h,scalar_sin_callout_poly_inner_t_18_0m);
  *scalar_sin_callout_poly_inner_resh = scalar_sin_callout_poly_inner_t_19_0h; *scalar_sin_callout_poly_inner_resm = scalar_sin_callout_poly_inner_t_19_0m;


}

STATIC INLINE double scalar_sin_callout_poly(double x) {
  double yh, yl;

  scalar_sin_callout_poly_inner(&yh,&yl,x);

  return yh + yl;
}

STATIC INLINE void scalar_tan_callout_easy_arg_red(double * RESTRICT signS, double * RESTRICT rs, 
                                                   double * RESTRICT signC, double * RESTRICT rc,
                                                   double x) {
  double xTimesRcprPi;
  double shiftedXTimesRcprPi;
  double ks;
  double kTimesPiHi, kTimesPiMi, kTimesPiLo;
  double tempR1s, tempR2s;
  dblcast signBitS;
  double xTimesRcprPiPlusHalf, shiftedXTimesRcprPiPlusHalf;
  double kc, kMinusHalf, kMinusHalfTimesPiHi, kMinusHalfTimesPiMi;
  double kMinusHalfTimesPiLo, tempR1c, tempR2c;
  dblcast signBitC;

  xTimesRcprPiPlusHalf = x * TANGENT_RCPR_PI + 0.5;                                             xTimesRcprPi = x * TANGENT_RCPR_PI;
  shiftedXTimesRcprPiPlusHalf = xTimesRcprPiPlusHalf + SHIFTER;                                 shiftedXTimesRcprPi = xTimesRcprPi + SHIFTER;
  kc = shiftedXTimesRcprPiPlusHalf - SHIFTER;                                                   ks = shiftedXTimesRcprPi - SHIFTER;
  kMinusHalf = kc - 0.5;                              /* exact, k integer, k <= 2^13, */        kTimesPiHi = ks * TANGENT_PI_HI;  /* exact, trailing zeros */
                                                      /* k - 0.5 on 14 bits. */                 kTimesPiMi = ks * TANGENT_PI_MI;  /* exact, trailing zeros */
  kMinusHalfTimesPiHi = kMinusHalf * TANGENT_PI_HI;   /* exact, trailing zeros */               kTimesPiLo = ks * TANGENT_PI_LO;  /* exact, trailing zeros */
  kMinusHalfTimesPiMi = kMinusHalf * TANGENT_PI_MI;   /* exact, trailing zeros */               tempR1s = x - kTimesPiHi;         /* exact, Sterbenz */
  kMinusHalfTimesPiLo = kMinusHalf * TANGENT_PI_LO;   /* exact, trailing zeros */               tempR2s = tempR1s - kTimesPiMi;   /* exact or one rounding error */
  tempR1c = x - kMinusHalfTimesPiHi;                  /* exact, Sterbenz */                     *rs = tempR2s - kTimesPiLo;       /* rs = tempR2s if operation yielding */
  tempR2c = tempR1c - kMinusHalfTimesPiMi;            /* exact or one rounding error */                                           /* tempR2s has not been exact */
  *rc = tempR2c - kMinusHalfTimesPiLo;                /* r = tempR2 if operation yielding */
                                                      /* tempR2 has not been exact */

  signBitC.d = shiftedXTimesRcprPiPlusHalf;                                                     signBitS.d = shiftedXTimesRcprPi;	 
  signBitC.i <<= 63;										signBitS.i <<= 63;			 
  signBitC.i |= 0x3ff0000000000000ull;								signBitS.i |= 0x3ff0000000000000ull;	 
  *signC = signBitC.d;										*signS = signBitS.d;                     
}

/* Computes a triple-double approximation to

   t(E) = 2^E * 1/pi - 2 * floor(2^(E - 1) * 1/pi)

   where E is in the range

   -39 <= E <= 971.

   For the time being, we use a simple table. In principle, the value
   of t(E) for any E can be computed out of a single
   multiple-precision expansion of 1/pi.

*/
STATIC INLINE void scalar_tan_callout_large_arg_red_get_rcpr_pi(double * RESTRICT tHi, double * RESTRICT tMi, double * RESTRICT tLo, int E) {
  *tHi = trigo_Payne_Hanek_Table_Hi[E - TRIGO_PAYNE_HANEK_E_MIN];
  *tMi = trigo_Payne_Hanek_Table_Mi[E - TRIGO_PAYNE_HANEK_E_MIN];
  *tLo = trigo_Payne_Hanek_Table_Lo[E - TRIGO_PAYNE_HANEK_E_MIN];
}

/* Computes n = nearestint(x) and sets odd to zero if n is even or to
   a non-zero value if n is odd.

   The input x is supposed to be non-negative and larger than 2.

*/
STATIC INLINE void scalar_tan_callout_large_arg_red_nearestint_large(double * RESTRICT n, int * RESTRICT odd, double x) {
  dblcast temp, shiftedX;

  /* Double precision floating-point numbers larger than or equal to
     2^53 are even integers.
  */
  if (x >= SCALAR_TANGENT_NEAREST_INT_TWO_53) {
    *n = x;
    *odd = 0;
    return;
  }

  /* Double precision floating-point numbers larger than or equal to
     2^52 are integers; their parity is in their ulp bit.
  */
  if (x >= SCALAR_TANGENT_NEAREST_INT_TWO_52) {
    *n = x;
    temp.d = x;
    *odd = temp.i & 1;
    return;
  }

  /* Here, the input is non-negative, greater than 2 and less than
     2^52.  The shifter trick therefore works, taking 2^52 as a
     shifter constant (and not 2^52 + 2^51).

     The parity of the nearest integer is in the ulp bit of the
     shifted value.
  */
  shiftedX.d = SCALAR_TANGENT_NEAREST_INT_TWO_52 + x;
  *odd = shiftedX.i & 1;
  *n = shiftedX.d - SCALAR_TANGENT_NEAREST_INT_TWO_52;
}

/* Computes n = nearestint(x) and sets odd to zero if n is even or to
   a non-zero value if n is odd.

   The input x is supposed to be smaller than 2^48 in magnitude.

*/
STATIC INLINE void scalar_tan_callout_large_arg_red_nearestint_small(double * RESTRICT n, int * RESTRICT odd, double x) {
  dblcast shiftedX;

  /* The input is less than 2^48, hence less than 2^51-1. The shifter
     trick therefore works. The parity of the integer is in the ulp
     bit of the shifted value.
  */
  shiftedX.d = SHIFTER + x;
  *odd = shiftedX.i & 1;
  *n = shiftedX.d - SHIFTER;
}

/* Computes signS and rs as well as signC and rc such that 

   signS * sin(rs) approximates sin(x) for x such
   that abs(x) >= 2^15

   and 

   signC * sin(rc) approximates cos(x) for x such
   that abs(x) >= 2^15

   The reduced arguments satisfy

    abs(rs) <= 157/100

    abs(rc) <= 157/100

   and

    signS * sin(rs) = sin(x) * (1 + epsS)
    
    signC * sin(rc) = cos(x) * (1 + epsC)

   where

    abs(epsS) <= 2^-51.5 and abs(epsC) <= 2^-51.5.

*/
STATIC INLINE void scalar_tan_callout_large_arg_red(double * RESTRICT signS, double * RESTRICT rs, 
                                                    double * RESTRICT signC, double * RESTRICT rc, 
                                                    double x) {
  dblcast xAbs;
  int E;
  double n;
  double tHi, tMi, tLo;
  double hHi, hMi, hLo;
  int oddHHi, oddHMi, oddHLo, oddQ;
  double nTHi, nTMi, nTLo, nTRHi, nTRMi, nTRLo;
  double t1, t2, t3, t4, t5;
  double b;
  double bPlusHalf, q, oneHalfMinusQ, t6, t7, t8, t9, t10, w;


  /* Strip off sign of x */
  *signC = 1.0;
  if (x > 0.0) {
    xAbs.d = x;
    *signS = 1.0;
  } else {
    xAbs.d = -x;
    *signS = -1.0;
  }

  /* Decompose abs(x) into E and n such that 2^E * n = abs(x) */
  E = (xAbs.i >> 52) - 1023 - 52;
  xAbs.i = (xAbs.i & 0x000fffffffffffffull) | 0x4330000000000000ull;
  n = xAbs.d;

  /* Get a triple-double approximation to

     t(E) = 2^E * 1/pi - 2 * floor(2^(E - 1) * 1/pi)

     Possible values of E that may reach this stage are in the range

     -39 <= E <= 971

  */
  scalar_tan_callout_large_arg_red_get_rcpr_pi(&tHi, &tMi, &tLo, E);


  /* Compute a renormalized triple-double approximation to

     nTHi + nTMi + nTlo = n * (tHi + tMi + tLo) * (1 + eps)

     with

     abs(eps) <= 2^150.

  */
  Mul133(&nTRHi, &nTRMi, &nTRLo, n, tHi, tMi, tLo);
  Renormalize3(&nTHi, &nTMi, &nTLo, nTRHi, nTRMi, nTRLo);

  /* Compute hHi = nearestint(nTHi).

     Also compute the parity of hHi.

  */
  scalar_tan_callout_large_arg_red_nearestint_large(&hHi, &oddHHi, nTHi);

  /* Compute t1 = nTHi - hHi */
  t1 = nTHi - hHi; /* exact, Sterbenz */

  /* Compute hMi = nearestint(nTMi).

     Also compute the parity of hMi.

  */
  scalar_tan_callout_large_arg_red_nearestint_small(&hMi, &oddHMi, nTMi);

  /* Compute t2 = nTMi - hMi */
  t2 = nTMi - hMi; /* exact, Sterbenz or hMi zero */

  /* Now we have

     hHi + hMi + t1 + t2 + nTLo = n * t * (1 + eps)

     with abs(eps) <= 2^-150.

     Now normalize t1 + t2 into t3 + t4, such that

     t3 + t4 = t1 + t2

     and t3 = RN(t3 + t4) where RN() is rounding to the nearest.

  */
  Add12Cond(t3, t4, t1, t2);

  /* Compute hLo = nearestint(t3).

     Also compute the parity of hLo.

  */
  scalar_tan_callout_large_arg_red_nearestint_small(&hLo, &oddHLo, t3);

  /* Compute t5 = t3 - hLo */
  t5 = t3 - hLo; /* exact, Sterbenz or hLo zero */

  /* We now have

     hHi + hMi + hLo + t5 + t4 + nTLo = n * t * (1 + eps)

     where abs(eps) <= 2^-150 and hHi, hMi and hLo are integer.

     Now compute an approximation b to t5 + t4 + nTLo, with at most 2
     roundings.

  */
  b = t5 + (t4 + nTLo);

  /* Multiply b by pi, yielding rs = b * pi */
  *rs = b * SCALAR_TANGENT_CONST_PI; /* one rounding error */

  /* Now compute q = nearestint(b + 1/2)

     Also compute the parity of q.

  */
  bPlusHalf = b + 0.5;
  scalar_tan_callout_large_arg_red_nearestint_small(&q, &oddQ, bPlusHalf);

  /* As we have -6/10 <= b <= 6/10, we have -1/10 <= b + 1/2 <= 11/10
     and hence q is either zero or one. Therefore 0.5 - q, one of -0.5 or 0.5,
     is representable.
  */
  oneHalfMinusQ = 0.5 - q; /* exact, see above */

  /* Now compute

     t6 + t7 = t5 + oneHalfMinusQ
     t8 + t9 = t7 + t4
     t10 = (t9 + nTLo) * (1 + eps)

     where abs(eps) <= 2^-53

  */
  Add12Cond(t6, t7, t5, oneHalfMinusQ);
  Add12Cond(t8, t9, t7, t4);
  t10 = t9 + nTLo;

  /* Now we have

     hHi + hMi + hLo + q + t5 + (0.5 - q) + t4 + nTLo - 0.5 = n * t * (1 + eps)

     yielding

     hHi + hMi + hLo + q + t6 + t7 + t4 + nTLo - 0.5 = n * t * (1 + eps)

     further

     hHi + hMi + hLo + q + t6 + t8 + t9 + nTLo - 0.5 = n * t * (1 + eps)

     and finally

     hHi + hMi + hLo + q + t6 + t8 + t10 - 0.5 = n * t * (1 + eps').

     Compute now w = t6 + t8 + t10 with at most 2 roundings.

  */
  w = t6 + (t8 + t10);

  /* Multiply w by pi, yielding rc = w * pi */
  *rc = w * SCALAR_TANGENT_CONST_PI; /* one rounding error */

  /* Now, nonwithstanding rounding and approximation errors we have:

     s = 2 * floor(2^(E - 1) * 1/pi), an even integer

     and therefore

     2^E * 1/pi = s + t

     which yields

     1/pi = 2^-E * (s + t)

     Taking hs = hHi + hMi + hLo and setting ks = n * s + hs, which is an
     integer while n * s is an even integer, we obtain

     sin(x) = sign * sin(abs(x)) 
            = sign * sin(2^E * n)
	    = sign * (-1)^hs * sin(2^E * n - ks * pi)
            = sign * (-1)^hs * sin(pi * (2^E * n * 1/pi - ks))
	    = sign * (-1)^hs * sin(pi * (2^E * n * 2^(-E) * (s + t) - ks))
	    = sign * (-1)^hs * sin(pi * (n * s + n * t - ks))
	    = sign * (-1)^hs * sin(pi * (n * s + n * t - n * s - hs))
	    = sign * (-1)^hs * sin(pi * (n * t - hs))
	    = sign * (-1)^hs * sin(pi * b)
	    = sign * (-1)^hs * sin(rs)
	    = sign * (-1)^hHi * (-1)^hMi * (-1)^hLo * sin(rs)

     Further, taking hc = hHi + hMi + hLo + q and setting kc = n * s +
     hc, which is an integer while n * s is an even integer, we obtain

     cos(x) = sign * cos(abs(x))
            = sign * cos(2^E * n)
            = sign * (-1)^hc * cos(2^E * n - kc * pi)
            = sign * (-1)^hc * cos(pi * (2^E * n * 1/pi - kc))
            = sign * (-1)^hc * cos(pi * (2^E * n * 2^(-E) * (s + t) - kc))
            = sign * (-1)^hc * cos(pi * (n * s + n * t - kc))
            = sign * (-1)^hc * cos(pi * (n * s + n * t - n * s - hc))
            = sign * (-1)^hc * cos(pi * (n * t - hc + 1/2) - pi/2)
            = sign * (-1)^hc * sin(pi * (n * t - (hc - 1/2)))
            = sign * (-1)^hc * sin(pi * w)
            = sign * (-1)^hc * sin(rc)
            = sign * (-1)^hHi * (-1)^hMi * (-1)^hLo * (-1)^q * sin(rc)

     So we are left with accounting for the signs.
  */
  if (oddHHi) {
    *signS = - *signS;
    *signC = - *signC;
  }
  if (oddHMi) {
    *signS = - *signS;
    *signC = - *signC;
  }
  if (oddHLo) {
    *signS = - *signS;
    *signC = - *signC;
  }
  if (oddQ) {
    *signC = - *signC;
  }
}

/* A scalar tangent for the callout -- main case

   This function assumes that x is real and
   larger than the short-path bound.

*/
STATIC INLINE double scalar_tan_callout_inner(double x) {
  double xAbs, rs, rc, signS, signC, ys, yc, y;

  /* We have three paths:

     (i)   abs(x) <= SCALAR_TANGENT_NO_ARG_RED_BOUND: we simply use a polynomial
     (ii)  abs(x) <= TANGENT_EASY_PATH_BOUND: we perform easy argument reduction and
                                           reuse the same polynomial
     (iii) otherwise: we perform large input argument reduction and
                      reuse the same polynomial

  */
  xAbs = fabs(x);
  if (xAbs <= SCALAR_TANGENT_NO_ARG_RED_BOUND) {
    return scalar_tan_callout_poly(x);
  }

  if (xAbs <= TANGENT_EASY_PATH_BOUND) {
    scalar_tan_callout_easy_arg_red(&signS, &rs, &signC, &rc, x);
    ys = signS * scalar_sin_callout_poly(rs);
    yc = signC * scalar_sin_callout_poly(rc);
    y = ys / yc; /* No overflow or invalid possible, one rounding */
    return y;
  }

  scalar_tan_callout_large_arg_red(&signS, &rs, &signC, &rc, x);
  ys = signS * scalar_sin_callout_poly(rs);
  yc = signC * scalar_sin_callout_poly(rc);
  y = ys / yc; /* No overflow or invalid possible, one rounding */
  return y;
}

/* A scalar tangent for the callout */
STATIC INLINE double scalar_tan_callout(double x) {
  double xAbs;
  dblcast xAbsdb;

  /* A short-path for a lot of entries */
  xAbs = fabs(x);
  xAbsdb.d = xAbs;
  if (xAbsdb.i < SCALAR_TANGENT_SHORT_PATH) {
    /* For x s.t. abs(x) < 2^-26, abs(x/tan(x)-1)<2^-53.5 */
    return x;
  }

  /* Perform special case handling */
  if (xAbsdb.i >= 0x7ff0000000000000ull) {
    /* Input is Infinity or NaN

       tan(+/- Infinity) = NaN, raising Invalid
       tan(q/sNaN) = qNaN, signaling for signaling NaN

       Computing x - x does exactly that.

    */
    return x - x;
  }

  /* The input is real and larger that the short-part bound

     Encapsulate that code in a function.

  */
  return scalar_tan_callout_inner(x);
}

/* A vector tangent callout */
STATIC INLINE void vector_tan_callout(double * RESTRICT y, CONST double * RESTRICT x) {
  int i;

  for (i=0;i<VECTOR_LENGTH;i++) {
    y[i] = scalar_tan_callout(x[i]);
  }
}

/* Generated polynomial for vector tangent */
#define vector_tan_poly_coeff_1h 1.00000000000000000000000000000000000000000000000000000000000000000000000000000000e+00
#define vector_tan_poly_coeff_3h 3.33333333333324710601175411284202709794044494628906250000000000000000000000000000e-01
#define vector_tan_poly_coeff_5h 1.33333333334434228634179930850223172456026077270507812500000000000000000000000000e-01
#define vector_tan_poly_coeff_7h 5.39682539143767556200081969564052997156977653503417968750000000000000000000000000e-02
#define vector_tan_poly_coeff_9h 2.18694899035280151955440430810995167121291160583496093750000000000000000000000000e-02
#define vector_tan_poly_coeff_11h 8.86321482226312164220960454485975787974894046783447265625000000000000000000000000e-03
#define vector_tan_poly_coeff_13h 3.59233066470266131781086116347978531848639249801635742187500000000000000000000000e-03
#define vector_tan_poly_coeff_15h 1.45448966274447930371871429144903231645002961158752441406250000000000000000000000e-03
#define vector_tan_poly_coeff_17h 5.96257314450354420493072282738467038143426179885864257812500000000000000000000000e-04
#define vector_tan_poly_coeff_19h 2.18691497295519973178765105181753369834041222929954528808593750000000000000000000e-04
#define vector_tan_poly_coeff_21h 1.44435747648741523354448679228312357736285775899887084960937500000000000000000000e-04
#define vector_tan_poly_coeff_23h -3.79120839557876130221543420351082431807299144566059112548828125000000000000000000e-05
#define vector_tan_poly_coeff_25h 1.00331501342874697727501620558854256159975193440914154052734375000000000000000000e-04
#define vector_tan_poly_coeff_27h -5.08736164243974461982716883667876572872046381235122680664062500000000000000000000e-05
#define vector_tan_poly_coeff_28h 1.24415843339543026712493574501815650602727116296603027123524043418001383543014526e-15
#define vector_tan_poly_coeff_29h 2.23286673098861075106510987176022808853304013609886169433593750000000000000000000e-05


STATIC INLINE void vector_tan_poly(double * RESTRICT vector_tan_poly_resh, double x) {
  double vector_tan_poly_x_0_pow2h;


  vector_tan_poly_x_0_pow2h = x * x;


  double vector_tan_poly_t_1_0h;
  double vector_tan_poly_t_2_0h;
  double vector_tan_poly_t_3_0h;
  double vector_tan_poly_t_4_0h;
  double vector_tan_poly_t_5_0h;
  double vector_tan_poly_t_6_0h;
  double vector_tan_poly_t_7_0h;
  double vector_tan_poly_t_8_0h;
  double vector_tan_poly_t_9_0h;
  double vector_tan_poly_t_10_0h;
  double vector_tan_poly_t_11_0h;
  double vector_tan_poly_t_12_0h;
  double vector_tan_poly_t_13_0h;
  double vector_tan_poly_t_14_0h;
  double vector_tan_poly_t_15_0h;
  double vector_tan_poly_t_16_0h;
  double vector_tan_poly_t_17_0h;
  double vector_tan_poly_t_18_0h;
  double vector_tan_poly_t_19_0h;
  double vector_tan_poly_t_20_0h;
  double vector_tan_poly_t_21_0h;
  double vector_tan_poly_t_22_0h;
  double vector_tan_poly_t_23_0h;
  double vector_tan_poly_t_24_0h;
  double vector_tan_poly_t_25_0h;
  double vector_tan_poly_t_26_0h;
  double vector_tan_poly_t_27_0h;
  double vector_tan_poly_t_28_0h;
  double vector_tan_poly_t_29_0h;
  double vector_tan_poly_t_30_0h;
  double vector_tan_poly_t_31_0h;
  double vector_tan_poly_t_32_0h;
 


  vector_tan_poly_t_1_0h = vector_tan_poly_coeff_29h;
  vector_tan_poly_t_2_0h = vector_tan_poly_t_1_0h * x;
  vector_tan_poly_t_3_0h = vector_tan_poly_coeff_28h + vector_tan_poly_t_2_0h;
  vector_tan_poly_t_4_0h = vector_tan_poly_t_3_0h * x;
  vector_tan_poly_t_5_0h = vector_tan_poly_coeff_27h + vector_tan_poly_t_4_0h;
  vector_tan_poly_t_6_0h = vector_tan_poly_t_5_0h * vector_tan_poly_x_0_pow2h;
  vector_tan_poly_t_7_0h = vector_tan_poly_coeff_25h + vector_tan_poly_t_6_0h;
  vector_tan_poly_t_8_0h = vector_tan_poly_t_7_0h * vector_tan_poly_x_0_pow2h;
  vector_tan_poly_t_9_0h = vector_tan_poly_coeff_23h + vector_tan_poly_t_8_0h;
  vector_tan_poly_t_10_0h = vector_tan_poly_t_9_0h * vector_tan_poly_x_0_pow2h;
  vector_tan_poly_t_11_0h = vector_tan_poly_coeff_21h + vector_tan_poly_t_10_0h;
  vector_tan_poly_t_12_0h = vector_tan_poly_t_11_0h * vector_tan_poly_x_0_pow2h;
  vector_tan_poly_t_13_0h = vector_tan_poly_coeff_19h + vector_tan_poly_t_12_0h;
  vector_tan_poly_t_14_0h = vector_tan_poly_t_13_0h * vector_tan_poly_x_0_pow2h;
  vector_tan_poly_t_15_0h = vector_tan_poly_coeff_17h + vector_tan_poly_t_14_0h;
  vector_tan_poly_t_16_0h = vector_tan_poly_t_15_0h * vector_tan_poly_x_0_pow2h;
  vector_tan_poly_t_17_0h = vector_tan_poly_coeff_15h + vector_tan_poly_t_16_0h;
  vector_tan_poly_t_18_0h = vector_tan_poly_t_17_0h * vector_tan_poly_x_0_pow2h;
  vector_tan_poly_t_19_0h = vector_tan_poly_coeff_13h + vector_tan_poly_t_18_0h;
  vector_tan_poly_t_20_0h = vector_tan_poly_t_19_0h * vector_tan_poly_x_0_pow2h;
  vector_tan_poly_t_21_0h = vector_tan_poly_coeff_11h + vector_tan_poly_t_20_0h;
  vector_tan_poly_t_22_0h = vector_tan_poly_t_21_0h * vector_tan_poly_x_0_pow2h;
  vector_tan_poly_t_23_0h = vector_tan_poly_coeff_9h + vector_tan_poly_t_22_0h;
  vector_tan_poly_t_24_0h = vector_tan_poly_t_23_0h * vector_tan_poly_x_0_pow2h;
  vector_tan_poly_t_25_0h = vector_tan_poly_coeff_7h + vector_tan_poly_t_24_0h;
  vector_tan_poly_t_26_0h = vector_tan_poly_t_25_0h * vector_tan_poly_x_0_pow2h;
  vector_tan_poly_t_27_0h = vector_tan_poly_coeff_5h + vector_tan_poly_t_26_0h;
  vector_tan_poly_t_28_0h = vector_tan_poly_t_27_0h * vector_tan_poly_x_0_pow2h;
  vector_tan_poly_t_29_0h = vector_tan_poly_coeff_3h + vector_tan_poly_t_28_0h;
  vector_tan_poly_t_30_0h = vector_tan_poly_t_29_0h * vector_tan_poly_x_0_pow2h;
  vector_tan_poly_t_31_0h = vector_tan_poly_coeff_1h + vector_tan_poly_t_30_0h;
  vector_tan_poly_t_32_0h = vector_tan_poly_t_31_0h * x;
  *vector_tan_poly_resh = vector_tan_poly_t_32_0h;


}

/* Generated polynomial for vector tangent - the approximated function actually is a sine */
#define vector_sin_poly_coeff_1h 9.99999999999999888977697537484345957636833190917968750000000000000000000000000000e-01
#define vector_sin_poly_coeff_3h -1.66666666666660162610114070957934018224477767944335937500000000000000000000000000e-01
#define vector_sin_poly_coeff_5h 8.33333333327826789038716981394827598705887794494628906250000000000000000000000000e-03
#define vector_sin_poly_coeff_7h -1.98412698236926553435957587545601654710480943322181701660156250000000000000000000e-04
#define vector_sin_poly_coeff_9h 2.75573164694192727480195545530783363119553541764616966247558593750000000000000000e-06
#define vector_sin_poly_coeff_11h -2.50518735179577099054529720160294781194920687994454056024551391601562500000000000e-08
#define vector_sin_poly_coeff_13h 1.60479180503090747315623245663682161499252742942189797759056091308593750000000000e-10
#define vector_sin_poly_coeff_14h -7.17095729411287097962112479975201123250413928394855684375196885893544163081969600e-22
#define vector_sin_poly_coeff_15h -7.37148114231591202713508385790312608942809191603373619727790355682373046875000000e-13


STATIC INLINE void vector_sin_poly(double * RESTRICT vector_sin_poly_resh, double x) {
  double vector_sin_poly_x_0_pow2h;


  vector_sin_poly_x_0_pow2h = x * x;


  double vector_sin_poly_t_1_0h;
  double vector_sin_poly_t_2_0h;
  double vector_sin_poly_t_3_0h;
  double vector_sin_poly_t_4_0h;
  double vector_sin_poly_t_5_0h;
  double vector_sin_poly_t_6_0h;
  double vector_sin_poly_t_7_0h;
  double vector_sin_poly_t_8_0h;
  double vector_sin_poly_t_9_0h;
  double vector_sin_poly_t_10_0h;
  double vector_sin_poly_t_11_0h;
  double vector_sin_poly_t_12_0h;
  double vector_sin_poly_t_13_0h;
  double vector_sin_poly_t_14_0h;
  double vector_sin_poly_t_15_0h;
  double vector_sin_poly_t_16_0h;
  double vector_sin_poly_t_17_0h;
  double vector_sin_poly_t_18_0h;



  vector_sin_poly_t_1_0h = vector_sin_poly_coeff_15h;
  vector_sin_poly_t_2_0h = vector_sin_poly_t_1_0h * x;
  vector_sin_poly_t_3_0h = vector_sin_poly_coeff_14h + vector_sin_poly_t_2_0h;
  vector_sin_poly_t_4_0h = vector_sin_poly_t_3_0h * x;
  vector_sin_poly_t_5_0h = vector_sin_poly_coeff_13h + vector_sin_poly_t_4_0h;
  vector_sin_poly_t_6_0h = vector_sin_poly_t_5_0h * vector_sin_poly_x_0_pow2h;
  vector_sin_poly_t_7_0h = vector_sin_poly_coeff_11h + vector_sin_poly_t_6_0h;
  vector_sin_poly_t_8_0h = vector_sin_poly_t_7_0h * vector_sin_poly_x_0_pow2h;
  vector_sin_poly_t_9_0h = vector_sin_poly_coeff_9h + vector_sin_poly_t_8_0h;
  vector_sin_poly_t_10_0h = vector_sin_poly_t_9_0h * vector_sin_poly_x_0_pow2h;
  vector_sin_poly_t_11_0h = vector_sin_poly_coeff_7h + vector_sin_poly_t_10_0h;
  vector_sin_poly_t_12_0h = vector_sin_poly_t_11_0h * vector_sin_poly_x_0_pow2h;
  vector_sin_poly_t_13_0h = vector_sin_poly_coeff_5h + vector_sin_poly_t_12_0h;
  vector_sin_poly_t_14_0h = vector_sin_poly_t_13_0h * vector_sin_poly_x_0_pow2h;
  vector_sin_poly_t_15_0h = vector_sin_poly_coeff_3h + vector_sin_poly_t_14_0h;
  vector_sin_poly_t_16_0h = vector_sin_poly_t_15_0h * vector_sin_poly_x_0_pow2h;
  vector_sin_poly_t_17_0h = vector_sin_poly_coeff_1h + vector_sin_poly_t_16_0h;
  vector_sin_poly_t_18_0h = vector_sin_poly_t_17_0h * x;
  *vector_sin_poly_resh = vector_sin_poly_t_18_0h;


}

/* A vector tangent */
void vector_tan(double * RESTRICT yArg, CONST double * RESTRICT xArg) {
  int i;
  int okaySlots, argRedSlots;
  double * RESTRICT y;
  CONST double * RESTRICT x;
  double xTimesRcprPiPlusHalf, shiftedXTimesRcprPiPlusHalf;
  double xTimesRcprPi, shiftedXTimesRcprPi;
  double ks, kc, kMinusHalf;
  double kMinusHalfTimesPiHi, kMinusHalfTimesPiMi, kMinusHalfTimesPiLo;
  double kTimesPiHi, kTimesPiMi, kTimesPiLo;
  double tempR1s, tempR2s, tempR1c, tempR2c, rc, rs;
  double currX;
  dblcast signBitS, signBitC, rcc, rcs;
  double sinRs, sinRc;
  double xAbs, yt, ys, yc;

  /* Assume alignment */
#ifdef NO_ASSUME_ALIGNED
  x = xArg;
  y = yArg;
#else
  x = __builtin_assume_aligned(xArg, VECTOR_LENGTH * __alignof__(double));
  y = __builtin_assume_aligned(yArg, VECTOR_LENGTH * __alignof__(double));
#endif

  /* Check if we can handle all inputs on the easy path and compute
     indication if argument reduction is not needed anywhere.
  */
  okaySlots = 0;
  argRedSlots = 0;
  for (i=0;i<VECTOR_LENGTH;i++) {
    xAbs = fabs(x[i]);
    okaySlots += ((xAbs < TANGENT_EASY_PATH_BOUND) & (xAbs > TANGENT_INPUT_UNDERFLOW_BOUND));
    argRedSlots += (xAbs > TANGENT_ARG_RED_BOUND);
  }

  /* Perform a callout if we cannot handle the input in one slot */
  if (okaySlots != VECTOR_LENGTH) {
    vector_tan_callout(yArg, xArg);
    return;
  }

  /* If no argument reduction is needed anywhere, just use a
     polynomial
  */
  if (argRedSlots == 0) {
    for (i=0;i<VECTOR_LENGTH;i++) {
      vector_tan_poly(&(y[i]), x[i]);
    }
    return;
  }

  /* Here we know that all inputs are real and small enough to
     allow for our easy argument reduction, using only a 122 bit accurate
     approximation to pi.
  */
  for (i=0;i<VECTOR_LENGTH;i++) {
    currX = x[i];

    /* Compute the reduced arguments

       rs = x - ks * pi

       where ks = nearestint(x / pi)

       and

       rc = x - (kc - 0.5) * pi

       where kc = nearestint(x / pi + 0.5)
    */
    xTimesRcprPiPlusHalf = currX * TANGENT_RCPR_PI + 0.5;                                         xTimesRcprPi = currX * TANGENT_RCPR_PI;
    shiftedXTimesRcprPiPlusHalf = xTimesRcprPiPlusHalf + SHIFTER;                                 shiftedXTimesRcprPi = xTimesRcprPi + SHIFTER;
    kc = shiftedXTimesRcprPiPlusHalf - SHIFTER;                                                   ks = shiftedXTimesRcprPi - SHIFTER;
    kMinusHalf = kc - 0.5;                              /* exact, k integer, k <= 2^13, */        kTimesPiHi = ks * TANGENT_PI_HI;  /* exact, trailing zeros */
                                                        /* k - 0.5 on 14 bits. */                 kTimesPiMi = ks * TANGENT_PI_MI;  /* exact, trailing zeros */
    kMinusHalfTimesPiHi = kMinusHalf * TANGENT_PI_HI;   /* exact, trailing zeros */               kTimesPiLo = ks * TANGENT_PI_LO;  /* exact, trailing zeros */
    kMinusHalfTimesPiMi = kMinusHalf * TANGENT_PI_MI;   /* exact, trailing zeros */               tempR1s = currX - kTimesPiHi;     /* exact, Sterbenz */
    kMinusHalfTimesPiLo = kMinusHalf * TANGENT_PI_LO;   /* exact, trailing zeros */               tempR2s = tempR1s - kTimesPiMi;   /* exact or one rounding error */
    tempR1c = currX - kMinusHalfTimesPiHi;              /* exact, Sterbenz */                     rs = tempR2s - kTimesPiLo;        /* rs = tempR2s if operation yielding */
    tempR2c = tempR1c - kMinusHalfTimesPiMi;            /* exact or one rounding error */                                           /* tempR2s has not been exact */
    rc = tempR2c - kMinusHalfTimesPiLo;                 /* r = tempR2 if operation yielding */
                                                        /* tempR2 has not been exact */

    /* Overall, we have

       * rs = (x - k * pi) * (1 + eps), abs(eps) <= 2^-55.3 

       * abs(rs) <= 157/100.

       and 
       
       * rc = (x - (kc - 0.5) * pi) * (1 + eps), abs(eps) <= 2^-55.3

       * abs(rc) <= 157/100.

       We have

       sin(x) = (-1)^ks * sin(x - ks * pi) = (-1)^ks * sin(rs)

       and

       cos(x) = (-1)^kc * cos(x - kc * pi + pi/2 - pi/2) = (-1)^kc * sin(rc).

       The parities of ks and kc are reflected by the last bit of
       shiftedXTimesRcprPi and shiftedXTimesRcprPiPlusHalf. This last bit 
       is equal to the sign bit of (-1)^ks resp. (-1)^kc.

    */
    signBitC.d = shiftedXTimesRcprPiPlusHalf;       signBitS.d = shiftedXTimesRcprPi;
    signBitC.i <<= 63;                              signBitS.i <<= 63;              
    rcc.d = rc;                                     rcs.d = rs;                     
    rcc.i = rcc.i ^ signBitC.i;                     rcs.i = rcs.i ^ signBitS.i;     
    rc = rcc.d;                                     rs = rcs.d;                       

    /* Now compute an approximation to sin(rs) and sin(rc) using a polynomial */
    vector_sin_poly(&sinRs, rs);
    vector_sin_poly(&sinRc, rc);

    /* sinRs represents sin(x) and sinRc represents cos(x) */
    ys = sinRs;
    yc = sinRc;

    /* Compute tangent as sin(x) / tan(x), one rounding, no overflow nor invalid possible */
    yt = ys / yc;

    /* Write back result */
    y[i] = yt;
  }
}
