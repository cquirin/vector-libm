/* This program implements a show-case vector (vectorizable) double
   precision arccosine with a 6 ulp error bound.

   Author: Christoph Lauter,

           Sorbonne Université - LIP6 - PEQUAN team.

   This program is

   Copyright 2014-2018 Christoph Lauter Sorbonne Université

   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions
   are met:

   1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.

   2. Redistributions in binary form must reproduce the above
   copyright notice, this list of conditions and the following
   disclaimer in the documentation and/or other materials provided
   with the distribution.

   3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived
   from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
   FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
   COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
   INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
   (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
   SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
   HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
   STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
   OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#include <stdint.h>
#include "acos.h"

/* Two caster types */
typedef union _dblcast {
  double   d;
  uint64_t i;
} dblcast;

typedef union {
  int64_t l;
  double d;
} db_number;

/* Need fabs and sqrt */
double fabs(double);
double sqrt(double);


/* Some constants and polynomial coefficients */
#define ACOS_EASY_BOUND         2.77555756156289135105907917022705078125e-17
#define ACOS_REALLY_EASY_BOUND  7.62939453125e-6
#define ACOS_PI_HALF            1.5707963267948965579989817342720925807952880859375
#define ACOS_PI                 3.141592653589793115997963468544185161590576171875
#define ACOS_REALLY_SMALL       8.67361737988403547205962240695953369140625e-19

#define acos_p_coeff_0   1.5707963267948965579989817342720925807952880859375
#define acos_p_coeff_1   -0.99999999999995348165526820594095624983310699462890625
#define acos_p_coeff_2   0.78539816339075196882646423546248115599155426025390625
#define acos_p_coeff_3   -0.6666666662746616456303172526531852781772613525390625
#define acos_p_coeff_4   0.58904861026524646394619821876403875648975372314453125
#define acos_p_coeff_5   -0.5333330959494499978745807311497628688812255859375
#define acos_p_coeff_6   0.490870768738022544841470562460017390549182891845703125
#define acos_p_coeff_7   -0.457114363980312854440768433050834573805332183837890625
#define acos_p_coeff_8   0.429319653781617793786296033431426621973514556884765625
#define acos_p_coeff_9   -0.405331979894171745559816599779878742992877960205078125
#define acos_p_coeff_10   0.382425383208780667043669154736562632024288177490234375
#define acos_p_coeff_11   -0.356054314765573642898033313031191937625408172607421875
#define acos_p_coeff_12   0.31965283448653336595413065879256464540958404541015625
#define acos_p_coeff_13   -0.26744342534339005101884367832099087536334991455078125
#define acos_p_coeff_14   0.2000559696415042110206883307910175062716007232666015625
#define acos_p_coeff_15   -0.1279235903827879161642755434513674117624759674072265625
#define acos_p_coeff_16   6.668167436316407847840537215233780443668365478515625e-2
#define acos_p_coeff_17   -2.6832487978937273609414404518247465603053569793701171875e-2
#define acos_p_coeff_18   7.7456436772695320958082021434165653772652149200439453125e-3
#define acos_p_coeff_19   -1.4183235554110734492894163594201017986051738262176513671875e-3
#define acos_p_coeff_20   1.2321977686252869326095915614160958284628577530384063720703125e-4


STATIC INLINE double scalar_acos_callout_inner(double x) {
  double xAbs, t, g, z, pihalf, offset;
  dblcast xdb, zdb, pihalfdb;
  uint64_t xsign;
  double y;
  
  /* Get absolute value of x */
  xAbs = fabs(x);

  /* If x is smaller than 2^-17, we can simply return pi/2 - x */
  if (xAbs < ACOS_REALLY_EASY_BOUND) {
    y = ACOS_PI_HALF - x;
    return y;
  }

  /* Compute 

     t = sqrt(1 - x^2) = sqrt((1 + x) * (1 - x))

     The cancellation is not an issue as it provides
     an exact result; x is deemed exact.

  */
  t = sqrt((1.0 + x) * (1.0 - x));

  /* Compute 

     g(abs(x)) = acos(abs(x))/sqrt(1 - x^2)

     by evaluating a polynomial approximating it.

  */
  g =       acos_p_coeff_0 +
    xAbs * (acos_p_coeff_1 +
    xAbs * (acos_p_coeff_2 +
    xAbs * (acos_p_coeff_3 +
    xAbs * (acos_p_coeff_4 +
    xAbs * (acos_p_coeff_5 +
    xAbs * (acos_p_coeff_6 +
    xAbs * (acos_p_coeff_7 +
    xAbs * (acos_p_coeff_8 +
    xAbs * (acos_p_coeff_9 +
    xAbs * (acos_p_coeff_10 +
    xAbs * (acos_p_coeff_11 +
    xAbs * (acos_p_coeff_12 +
    xAbs * (acos_p_coeff_13 +
    xAbs * (acos_p_coeff_14 +
    xAbs * (acos_p_coeff_15 +
    xAbs * (acos_p_coeff_16 +
    xAbs * (acos_p_coeff_17 +
    xAbs * (acos_p_coeff_18 +
    xAbs * (acos_p_coeff_19 +
    xAbs *  acos_p_coeff_20)))))))))))))))))));

  /* Compute 

     z = acos(abs(x)) = g * sqrt(1 - x^2) = g * t 

  */
  z = g * t;

  /* Reconstruct acos(x) out of acos(abs(x)) as follows

     acos(x) = sgn(x) * acos(abs(x)) + (pi/2 - (sgn(x) * pi/2))

     The multiplications by sgn(x) can be implemented with 
     copysign operations.

  */
  xdb.d = x;
  xsign = xdb.i & 0x8000000000000000ull;
  zdb.d = z;
  zdb.i |= xsign;
  pihalf = ACOS_PI_HALF;
  pihalfdb.d = pihalf;
  pihalfdb.i |= xsign;
  offset = pihalf - pihalfdb.d;
  y = zdb.d + offset;

  /* Return the result */
  return y;
}

/* A scalar arccosine for the callout */
STATIC INLINE double scalar_acos_callout(double x) {
  dblcast xdb, xAbsdb;
  double yh, yl;
  double temp;

  xdb.d = x;
  xAbsdb.i = xdb.i & 0x7fffffffffffffffull;
  if (xAbsdb.i > 0x3ff0000000000000ull) {
    /* If we are here, the input is larger than 1.0 or Inf or NaN.
       
       Continue with testing if the input is NaN.

    */
    if (xAbsdb.i > 0x7ff0000000000000ull) {
      /* Here the input is NaN, return that (quietized) NaN */
      return 1.0 + x;
    }

    /* Here the input is larger than 1.0, but not NaN.

       Raise invalid and return a NaN.
       
    */
    temp = 0.0;
    return temp / temp;
  }

  /* If x = 1.0, return zero, without touching inexact */
  if (x == 1.0) {
    return 0.0;
  }

  /* If x = -1.0, return pi, raising inexact */
  if (x == -1.0) {
    yh = ACOS_PI;
    yl = ACOS_REALLY_SMALL;
    return yh + yl;
  }
  
  /* Here, the input is real and in range. */
  return scalar_acos_callout_inner(x);
}

/* A vector arccosine callout */
STATIC INLINE void vector_acos_callout(double * RESTRICT y, CONST double * RESTRICT x) {
  int i;

  for (i=0;i<VECTOR_LENGTH;i++) {
    y[i] = scalar_acos_callout(x[i]);
  }
}

/* A vector arccosine */
void vector_acos(double * RESTRICT yArg, CONST double * RESTRICT xArg) {
  int i;
  int okaySlots;
  double * RESTRICT y;
  CONST double * RESTRICT x;
  double currX, currY;
  double xAbs, t, g, z, pihalf, offset;
  dblcast xdb, zdb, pihalfdb;
  uint64_t xsign;
  
  /* Assume alignment */
#ifdef NO_ASSUME_ALIGNED
  x = xArg;
  y = yArg;
#else
  x = __builtin_assume_aligned(xArg, VECTOR_LENGTH * __alignof__(double));
  y = __builtin_assume_aligned(yArg, VECTOR_LENGTH * __alignof__(double));
#endif  

  /* Check if we can handle all inputs */
  okaySlots = 0;
  for (i=0;i<VECTOR_LENGTH;i++) {
    xAbs = fabs(x[i]);
    okaySlots += ((xAbs < 1.0) & (xAbs >= ACOS_EASY_BOUND)); 
  }

  /* Perform a callout if we cannot handle the input in one slot */
  if (okaySlots != VECTOR_LENGTH) {
    vector_acos_callout(yArg, xArg);
    return;
  }

  /* Here we know that all inputs are real and in range. */
  for (i=0;i<VECTOR_LENGTH;i++) {
    /* Get current element in vector */
    currX = x[i];

    /* Get absolute value of x */
    xAbs = fabs(currX);
    
    /* Compute 

       t = sqrt(1 - x^2) = sqrt((1 + x) * (1 - x))

       The cancellation is not an issue as it provides
       an exact result; x is deemed exact.

    */
    t = sqrt((1.0 + currX) * (1.0 - currX));

    /* Compute 

       g(abs(x)) = acos(abs(x))/sqrt(1 - x^2)

       by evaluating a polynomial approximating it.

    */
    g =       acos_p_coeff_0 +
      xAbs * (acos_p_coeff_1 +
      xAbs * (acos_p_coeff_2 +
      xAbs * (acos_p_coeff_3 +
      xAbs * (acos_p_coeff_4 +
      xAbs * (acos_p_coeff_5 +
      xAbs * (acos_p_coeff_6 +
      xAbs * (acos_p_coeff_7 +
      xAbs * (acos_p_coeff_8 +
      xAbs * (acos_p_coeff_9 +
      xAbs * (acos_p_coeff_10 +
      xAbs * (acos_p_coeff_11 +
      xAbs * (acos_p_coeff_12 +
      xAbs * (acos_p_coeff_13 +
      xAbs * (acos_p_coeff_14 +
      xAbs * (acos_p_coeff_15 +
      xAbs * (acos_p_coeff_16 +
      xAbs * (acos_p_coeff_17 +
      xAbs * (acos_p_coeff_18 +
      xAbs * (acos_p_coeff_19 +
      xAbs *  acos_p_coeff_20)))))))))))))))))));

    /* Compute 

       z = acos(abs(x)) = g * sqrt(1 - x^2) = g * t 

    */
    z = g * t;

    /* Reconstruct acos(x) out of acos(abs(x)) as follows

       acos(x) = sgn(x) * acos(abs(x)) + (pi/2 - (sgn(x) * pi/2))

       The multiplications by sgn(x) can be implemented with 
       copysign operations.

    */
    xdb.d = currX;
    xsign = xdb.i & 0x8000000000000000ull;
    zdb.d = z;
    zdb.i |= xsign;
    pihalf = ACOS_PI_HALF;
    pihalfdb.d = pihalf;
    pihalfdb.i |= xsign;
    offset = pihalf - pihalfdb.d;
    currY = zdb.d + offset; 
    
    /* Write result back into output vector */
    y[i] = currY;
  }
}
