/* This program implements a show-case vector (vectorizable) double
   precision cubic root with a 6 ulp error bound.

   Author: Christoph Lauter,

           Sorbonne Université - LIP6 - PEQUAN team.

   This program uses code generated using Sollya and Metalibm; see the
   licences and exception texts below.

   This program is

   Copyright 2014-2018 Christoph Lauter Sorbonne Université

   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions
   are met:

   1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.

   2. Redistributions in binary form must reproduce the above
   copyright notice, this list of conditions and the following
   disclaimer in the documentation and/or other materials provided
   with the distribution.

   3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived
   from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
   FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
   COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
   INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
   (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
   SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
   HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
   STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
   OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/* 

    This code was generated using non-trivial code generation commands
    of the Metalibm software program.
    
    Before using, modifying and/or integrating this code into other
    software, review the copyright and license status of this
    generated code. In particular, see the exception below.

    This generated program is partly or entirely based on a program
    generated using non-trivial code generation commands of the Sollya
    software program. See the copyright notice and exception text
    referring to that Sollya-generated part of this program generated
    with Metalibm below.

    Metalibm is
 
    Copyright 2008-2013 by 

    Laboratoire de l'Informatique du Parallélisme, 
    UMR CNRS - ENS Lyon - UCB Lyon 1 - INRIA 5668

    and by

    Laboratoire d'Informatique de Paris 6, equipe PEQUAN,
    UPMC Universite Paris 06 - CNRS - UMR 7606 - LIP6, Paris, France.

    Contributors: Christoph Quirin Lauter 
                  (UPMC LIP6 PEQUAN formerly LIP/ENS Lyon) 
                  christoph.lauter@lip6.fr

		  and

		  Olga Kupriianova 
		  (UPMC LIP6 PEQUAN)
		  olga.kupriianova@lip6.fr

    Metalibm was formerly developed by the Arenaire project at Ecole
    Normale Superieure de Lyon and is now developed by Equipe PEQUAN
    at Universite Pierre et Marie Curie Paris 6.

    The Metalibm software program is free software; you can
    redistribute it and/or modify it under the terms of the GNU Lesser
    General Public License as published by the Free Software
    Foundation; either version 2 of the License, or (at your option)
    any later version.

    Metalibm is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with the Metalibm program; if not, write to the Free
    Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
    02111-1307, USA.

    This generated program is distributed WITHOUT ANY WARRANTY; without
    even the implied warranty of MERCHANTABILITY or FITNESS FOR A
    PARTICULAR PURPOSE.
    
    As a special exception, you may create a larger work that contains
    part or all of this software generated using Metalibm and
    distribute that work under terms of your choice, so long as that
    work isn't itself a numerical code generator using the skeleton of
    this code or a modified version thereof as a code skeleton.
    Alternatively, if you modify or redistribute this generated code
    itself, or its skeleton, you may (at your option) remove this
    special exception, which will cause this generated code and its
    skeleton and the resulting Metalibm output files to be licensed
    under the General Public licence (version 2) without this special
    exception.
    
    This special exception was added by the Metalibm copyright holders 
    on November 20th 2013.
    
*/

/*
    This code was generated using non-trivial code generation commands of
    the Sollya software program.
    
    Before using, modifying and/or integrating this code into other
    software, review the copyright and license status of this generated
    code. In particular, see the exception below.
    
    Sollya is
    
    Copyright 2006-2016 by
    
    Laboratoire de l'Informatique du Parallelisme, UMR CNRS - ENS Lyon -
    UCB Lyon 1 - INRIA 5668,
    
    LORIA (CNRS, INPL, INRIA, UHP, U-Nancy 2)
    
    Laboratoire d'Informatique de Paris 6 - Équipe PEQUAN
    Sorbonne Universités
    UPMC Univ Paris 06
    UMR 7606, LIP6
    Boîte Courrier 169
    4, place Jussieu
    F-75252 Paris Cedex 05
    France
    
    CNRS, LIP6, UPMC
    Sorbonne Universités, UPMC Univ Paris 06,
    CNRS, LIP6 UMR 7606, 4 place Jussieu 75005 Paris
    
    and by
    
    Centre de recherche INRIA Sophia-Antipolis Mediterranee, equipe APICS,
    Sophia Antipolis, France.
    
    Contributors Ch. Lauter, S. Chevillard, M. Joldes, M. Mezzarobba
    
    christoph.lauter@ens-lyon.org
    sylvain.chevillard@ens-lyon.org
    joldes@laas.fr
    marc@mezzarobba.net
    
    The Sollya software is a computer program whose purpose is to provide
    an environment for safe floating-point code development. It is
    particularly targeted to the automated implementation of
    mathematical floating-point libraries (libm). Amongst other features,
    it offers a certified infinity norm, an automatic polynomial
    implementer and a fast Remez algorithm.
    
    The Sollya software is governed by the CeCILL-C license under French
    law and abiding by the rules of distribution of free software.  You
    can use, modify and/ or redistribute the software under the terms of
    the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
    following URL "http://www.cecill.info".
    
    As a counterpart to the access to the source code and rights to copy,
    modify and redistribute granted by the license, users are provided
    only with a limited warranty and the software's author, the holder of
    the economic rights, and the successive licensors have only limited
    liability.
    
    In this respect, the user's attention is drawn to the risks associated
    with loading, using, modifying and/or developing or reproducing the
    software by the user in light of its specific status of free software,
    that may mean that it is complicated to manipulate, and that also
    therefore means that it is reserved for developers and experienced
    professionals having in-depth computer knowledge. Users are therefore
    encouraged to load and test the software's suitability as regards
    their requirements in conditions enabling the security of their
    systems and/or data to be ensured and, more generally, to use and
    operate it in the same conditions as regards security.
    
    The fact that you are presently reading this means that you have had
    knowledge of the CeCILL-C license and that you accept its terms.
    
    The Sollya program is distributed WITHOUT ANY WARRANTY; without even
    the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
    PURPOSE.
    
    This generated program is distributed WITHOUT ANY WARRANTY; without
    even the implied warranty of MERCHANTABILITY or FITNESS FOR A
    PARTICULAR PURPOSE.
    
    As a special exception, you may create a larger work that contains
    part or all of this software generated using Sollya and distribute
    that work under terms of your choice, so long as that work isn't
    itself a numerical code generator using the skeleton of this code or a
    modified version thereof as a code skeleton.  Alternatively, if you
    modify or redistribute this generated code itself, or its skeleton,
    you may (at your option) remove this special exception, which will
    cause this generated code and its skeleton and the resulting Sollya
    output files to be licensed under the CeCILL-C licence without this
    special exception.
    
    This special exception was added by the Sollya copyright holders in
    version 4.1 of Sollya.
    
*/

#include <stdint.h>
#include "cbrt.h"

/* Two caster types */
typedef union _dblcast {
  double   d;
  uint64_t i;
} dblcast;

typedef union {
  int64_t l;
  double d;
} db_number;

/* Need fabs */
double fabs(double);

/* Some constants and polynomial coefficients */
#define CBRT_ONE_THIRD_41_BITS_TIMES_TWO_43  2932031007404ull
#define CBRT_TWO_54                          18014398509481984
#define CBRT_TWO_M_18                        3.814697265625e-6
#define CBRT_ONE_THIRD_HI                    0.3333333333333339254522798000834882259368896484375
#define CBRT_ONE_THIRD_LO                    -5.9211894646675120670714385099751138230475371038164e-16
#define CBRT_SHIFTER                         6755399441055744.0

/* Generated code */

#define vector_cbrt_log8_poly_coeff_1h 4.80898346962987832675651134195504710078239440917968750000000000000000000000000000e-01
#define vector_cbrt_log8_poly_coeff_2h -2.40449173481496247806177279926487244665622711181640625000000000000000000000000000e-01
#define vector_cbrt_log8_poly_coeff_3h 1.60299448987638121222332188153814058750867843627929687500000000000000000000000000e-01
#define vector_cbrt_log8_poly_coeff_4h -1.20224586739694008774570477271481649950146675109863281250000000000000000000000000e-01
#define vector_cbrt_log8_poly_coeff_5h 9.61796693952883113842844409191457089036703109741210937500000000000000000000000000e-02
#define vector_cbrt_log8_poly_coeff_6h -8.01497246334077900753456447091593872755765914916992187500000000000000000000000000e-02
#define vector_cbrt_log8_poly_coeff_7h 6.86997639524980496794626105838688090443611145019531250000000000000000000000000000e-02
#define vector_cbrt_log8_poly_coeff_8h -6.01122854168874912184072911713883513584733009338378906250000000000000000000000000e-02
#define vector_cbrt_log8_poly_coeff_9h 5.34331262787363300836851465192012256011366844177246093750000000000000000000000000e-02
#define vector_cbrt_log8_poly_coeff_10h -4.80900373962606861311108730205887695774435997009277343750000000000000000000000000e-02
#define vector_cbrt_log8_poly_coeff_11h 4.37191204390758939024586027244367869570851325988769531250000000000000000000000000e-02
#define vector_cbrt_log8_poly_coeff_12h -4.00735312093061304961061352969409199431538581848144531250000000000000000000000000e-02
#define vector_cbrt_log8_poly_coeff_13h 3.69715488649621851213744605502142803743481636047363281250000000000000000000000000e-02
#define vector_cbrt_log8_poly_coeff_14h -3.43145701915099909928841270811972208321094512939453125000000000000000000000000000e-02
#define vector_cbrt_log8_poly_coeff_15h 3.21825792565942139789036957608914235606789588928222656250000000000000000000000000e-02
#define vector_cbrt_log8_poly_coeff_16h -3.06694292252183142721833064570091664791107177734375000000000000000000000000000000e-02
#define vector_cbrt_log8_poly_coeff_17h 2.90579810009469058362796545225137379020452499389648437500000000000000000000000000e-02
#define vector_cbrt_log8_poly_coeff_18h -2.53732668729571043952919495723108411766588687896728515625000000000000000000000000e-02
#define vector_cbrt_log8_poly_coeff_19h 1.82611085014750540445049864501925185322761535644531250000000000000000000000000000e-02
#define vector_cbrt_log8_poly_coeff_20h -9.59544620212397433856033046595257474109530448913574218750000000000000000000000000e-03
#define vector_cbrt_log8_poly_coeff_21h 3.16918981327120232588390180694659647997468709945678710937500000000000000000000000e-03
#define vector_cbrt_log8_poly_coeff_22h -4.86521217067883429089475422202326626575086265802383422851562500000000000000000000e-04

#define vector_cbrt_exp2_poly_coeff_0h 1.00000000000000000000000000000000000000000000000000000000000000000000000000000000e+00
#define vector_cbrt_exp2_poly_coeff_1h 6.93147180559945397249066445510834455490112304687500000000000000000000000000000000e-01
#define vector_cbrt_exp2_poly_coeff_2h 2.40226506959102387162019454081018920987844467163085937500000000000000000000000000e-01
#define vector_cbrt_exp2_poly_coeff_3h 5.55041086648163373151732002952485345304012298583984375000000000000000000000000000e-02
#define vector_cbrt_exp2_poly_coeff_4h 9.61812910755120709094256881144247017800807952880859375000000000000000000000000000e-03
#define vector_cbrt_exp2_poly_coeff_5h 1.33335581473512836472861131653644406469538807868957519531250000000000000000000000e-03
#define vector_cbrt_exp2_poly_coeff_6h 1.54035305097614230902813847734478258644230663776397705078125000000000000000000000e-04
#define vector_cbrt_exp2_poly_coeff_7h 1.52527331434212287859537166001544505888887215405702590942382812500000000000000000e-05
#define vector_cbrt_exp2_poly_coeff_8h 1.32154100457461323110176266981685699875015416182577610015869140625000000000000000e-06
#define vector_cbrt_exp2_poly_coeff_9h 1.01782833699768429649077890793845702077646819816436618566513061523437500000000000e-07
#define vector_cbrt_exp2_poly_coeff_10h 7.07792472369775565893610023396505248616250582927023060619831085205078125000000000e-09
#define vector_cbrt_exp2_poly_coeff_11h 4.42918101614569258150827116358571461374182831605139654129743576049804687500000000e-10


STATIC INLINE void vector_cbrt_log8_poly(double *vector_cbrt_log8_poly_resh, double x) {




  double vector_cbrt_log8_poly_t_1_0h;
  double vector_cbrt_log8_poly_t_2_0h;
  double vector_cbrt_log8_poly_t_3_0h;
  double vector_cbrt_log8_poly_t_4_0h;
  double vector_cbrt_log8_poly_t_5_0h;
  double vector_cbrt_log8_poly_t_6_0h;
  double vector_cbrt_log8_poly_t_7_0h;
  double vector_cbrt_log8_poly_t_8_0h;
  double vector_cbrt_log8_poly_t_9_0h;
  double vector_cbrt_log8_poly_t_10_0h;
  double vector_cbrt_log8_poly_t_11_0h;
  double vector_cbrt_log8_poly_t_12_0h;
  double vector_cbrt_log8_poly_t_13_0h;
  double vector_cbrt_log8_poly_t_14_0h;
  double vector_cbrt_log8_poly_t_15_0h;
  double vector_cbrt_log8_poly_t_16_0h;
  double vector_cbrt_log8_poly_t_17_0h;
  double vector_cbrt_log8_poly_t_18_0h;
  double vector_cbrt_log8_poly_t_19_0h;
  double vector_cbrt_log8_poly_t_20_0h;
  double vector_cbrt_log8_poly_t_21_0h;
  double vector_cbrt_log8_poly_t_22_0h;
  double vector_cbrt_log8_poly_t_23_0h;
  double vector_cbrt_log8_poly_t_24_0h;
  double vector_cbrt_log8_poly_t_25_0h;
  double vector_cbrt_log8_poly_t_26_0h;
  double vector_cbrt_log8_poly_t_27_0h;
  double vector_cbrt_log8_poly_t_28_0h;
  double vector_cbrt_log8_poly_t_29_0h;
  double vector_cbrt_log8_poly_t_30_0h;
  double vector_cbrt_log8_poly_t_31_0h;
  double vector_cbrt_log8_poly_t_32_0h;
  double vector_cbrt_log8_poly_t_33_0h;
  double vector_cbrt_log8_poly_t_34_0h;
  double vector_cbrt_log8_poly_t_35_0h;
  double vector_cbrt_log8_poly_t_36_0h;
  double vector_cbrt_log8_poly_t_37_0h;
  double vector_cbrt_log8_poly_t_38_0h;
  double vector_cbrt_log8_poly_t_39_0h;
  double vector_cbrt_log8_poly_t_40_0h;
  double vector_cbrt_log8_poly_t_41_0h;
  double vector_cbrt_log8_poly_t_42_0h;
  double vector_cbrt_log8_poly_t_43_0h;
  double vector_cbrt_log8_poly_t_44_0h;
 


  vector_cbrt_log8_poly_t_1_0h = vector_cbrt_log8_poly_coeff_22h;
  vector_cbrt_log8_poly_t_2_0h = vector_cbrt_log8_poly_t_1_0h * x;
  vector_cbrt_log8_poly_t_3_0h = vector_cbrt_log8_poly_coeff_21h + vector_cbrt_log8_poly_t_2_0h;
  vector_cbrt_log8_poly_t_4_0h = vector_cbrt_log8_poly_t_3_0h * x;
  vector_cbrt_log8_poly_t_5_0h = vector_cbrt_log8_poly_coeff_20h + vector_cbrt_log8_poly_t_4_0h;
  vector_cbrt_log8_poly_t_6_0h = vector_cbrt_log8_poly_t_5_0h * x;
  vector_cbrt_log8_poly_t_7_0h = vector_cbrt_log8_poly_coeff_19h + vector_cbrt_log8_poly_t_6_0h;
  vector_cbrt_log8_poly_t_8_0h = vector_cbrt_log8_poly_t_7_0h * x;
  vector_cbrt_log8_poly_t_9_0h = vector_cbrt_log8_poly_coeff_18h + vector_cbrt_log8_poly_t_8_0h;
  vector_cbrt_log8_poly_t_10_0h = vector_cbrt_log8_poly_t_9_0h * x;
  vector_cbrt_log8_poly_t_11_0h = vector_cbrt_log8_poly_coeff_17h + vector_cbrt_log8_poly_t_10_0h;
  vector_cbrt_log8_poly_t_12_0h = vector_cbrt_log8_poly_t_11_0h * x;
  vector_cbrt_log8_poly_t_13_0h = vector_cbrt_log8_poly_coeff_16h + vector_cbrt_log8_poly_t_12_0h;
  vector_cbrt_log8_poly_t_14_0h = vector_cbrt_log8_poly_t_13_0h * x;
  vector_cbrt_log8_poly_t_15_0h = vector_cbrt_log8_poly_coeff_15h + vector_cbrt_log8_poly_t_14_0h;
  vector_cbrt_log8_poly_t_16_0h = vector_cbrt_log8_poly_t_15_0h * x;
  vector_cbrt_log8_poly_t_17_0h = vector_cbrt_log8_poly_coeff_14h + vector_cbrt_log8_poly_t_16_0h;
  vector_cbrt_log8_poly_t_18_0h = vector_cbrt_log8_poly_t_17_0h * x;
  vector_cbrt_log8_poly_t_19_0h = vector_cbrt_log8_poly_coeff_13h + vector_cbrt_log8_poly_t_18_0h;
  vector_cbrt_log8_poly_t_20_0h = vector_cbrt_log8_poly_t_19_0h * x;
  vector_cbrt_log8_poly_t_21_0h = vector_cbrt_log8_poly_coeff_12h + vector_cbrt_log8_poly_t_20_0h;
  vector_cbrt_log8_poly_t_22_0h = vector_cbrt_log8_poly_t_21_0h * x;
  vector_cbrt_log8_poly_t_23_0h = vector_cbrt_log8_poly_coeff_11h + vector_cbrt_log8_poly_t_22_0h;
  vector_cbrt_log8_poly_t_24_0h = vector_cbrt_log8_poly_t_23_0h * x;
  vector_cbrt_log8_poly_t_25_0h = vector_cbrt_log8_poly_coeff_10h + vector_cbrt_log8_poly_t_24_0h;
  vector_cbrt_log8_poly_t_26_0h = vector_cbrt_log8_poly_t_25_0h * x;
  vector_cbrt_log8_poly_t_27_0h = vector_cbrt_log8_poly_coeff_9h + vector_cbrt_log8_poly_t_26_0h;
  vector_cbrt_log8_poly_t_28_0h = vector_cbrt_log8_poly_t_27_0h * x;
  vector_cbrt_log8_poly_t_29_0h = vector_cbrt_log8_poly_coeff_8h + vector_cbrt_log8_poly_t_28_0h;
  vector_cbrt_log8_poly_t_30_0h = vector_cbrt_log8_poly_t_29_0h * x;
  vector_cbrt_log8_poly_t_31_0h = vector_cbrt_log8_poly_coeff_7h + vector_cbrt_log8_poly_t_30_0h;
  vector_cbrt_log8_poly_t_32_0h = vector_cbrt_log8_poly_t_31_0h * x;
  vector_cbrt_log8_poly_t_33_0h = vector_cbrt_log8_poly_coeff_6h + vector_cbrt_log8_poly_t_32_0h;
  vector_cbrt_log8_poly_t_34_0h = vector_cbrt_log8_poly_t_33_0h * x;
  vector_cbrt_log8_poly_t_35_0h = vector_cbrt_log8_poly_coeff_5h + vector_cbrt_log8_poly_t_34_0h;
  vector_cbrt_log8_poly_t_36_0h = vector_cbrt_log8_poly_t_35_0h * x;
  vector_cbrt_log8_poly_t_37_0h = vector_cbrt_log8_poly_coeff_4h + vector_cbrt_log8_poly_t_36_0h;
  vector_cbrt_log8_poly_t_38_0h = vector_cbrt_log8_poly_t_37_0h * x;
  vector_cbrt_log8_poly_t_39_0h = vector_cbrt_log8_poly_coeff_3h + vector_cbrt_log8_poly_t_38_0h;
  vector_cbrt_log8_poly_t_40_0h = vector_cbrt_log8_poly_t_39_0h * x;
  vector_cbrt_log8_poly_t_41_0h = vector_cbrt_log8_poly_coeff_2h + vector_cbrt_log8_poly_t_40_0h;
  vector_cbrt_log8_poly_t_42_0h = vector_cbrt_log8_poly_t_41_0h * x;
  vector_cbrt_log8_poly_t_43_0h = vector_cbrt_log8_poly_coeff_1h + vector_cbrt_log8_poly_t_42_0h;
  vector_cbrt_log8_poly_t_44_0h = vector_cbrt_log8_poly_t_43_0h * x;
  *vector_cbrt_log8_poly_resh = vector_cbrt_log8_poly_t_44_0h;


}

STATIC INLINE void vector_cbrt_exp2_poly(double *vector_cbrt_exp2_poly_resh, double x) {




  double vector_cbrt_exp2_poly_t_1_0h;
  double vector_cbrt_exp2_poly_t_2_0h;
  double vector_cbrt_exp2_poly_t_3_0h;
  double vector_cbrt_exp2_poly_t_4_0h;
  double vector_cbrt_exp2_poly_t_5_0h;
  double vector_cbrt_exp2_poly_t_6_0h;
  double vector_cbrt_exp2_poly_t_7_0h;
  double vector_cbrt_exp2_poly_t_8_0h;
  double vector_cbrt_exp2_poly_t_9_0h;
  double vector_cbrt_exp2_poly_t_10_0h;
  double vector_cbrt_exp2_poly_t_11_0h;
  double vector_cbrt_exp2_poly_t_12_0h;
  double vector_cbrt_exp2_poly_t_13_0h;
  double vector_cbrt_exp2_poly_t_14_0h;
  double vector_cbrt_exp2_poly_t_15_0h;
  double vector_cbrt_exp2_poly_t_16_0h;
  double vector_cbrt_exp2_poly_t_17_0h;
  double vector_cbrt_exp2_poly_t_18_0h;
  double vector_cbrt_exp2_poly_t_19_0h;
  double vector_cbrt_exp2_poly_t_20_0h;
  double vector_cbrt_exp2_poly_t_21_0h;
  double vector_cbrt_exp2_poly_t_22_0h;
  double vector_cbrt_exp2_poly_t_23_0h;
 


  vector_cbrt_exp2_poly_t_1_0h = vector_cbrt_exp2_poly_coeff_11h;
  vector_cbrt_exp2_poly_t_2_0h = vector_cbrt_exp2_poly_t_1_0h * x;
  vector_cbrt_exp2_poly_t_3_0h = vector_cbrt_exp2_poly_coeff_10h + vector_cbrt_exp2_poly_t_2_0h;
  vector_cbrt_exp2_poly_t_4_0h = vector_cbrt_exp2_poly_t_3_0h * x;
  vector_cbrt_exp2_poly_t_5_0h = vector_cbrt_exp2_poly_coeff_9h + vector_cbrt_exp2_poly_t_4_0h;
  vector_cbrt_exp2_poly_t_6_0h = vector_cbrt_exp2_poly_t_5_0h * x;
  vector_cbrt_exp2_poly_t_7_0h = vector_cbrt_exp2_poly_coeff_8h + vector_cbrt_exp2_poly_t_6_0h;
  vector_cbrt_exp2_poly_t_8_0h = vector_cbrt_exp2_poly_t_7_0h * x;
  vector_cbrt_exp2_poly_t_9_0h = vector_cbrt_exp2_poly_coeff_7h + vector_cbrt_exp2_poly_t_8_0h;
  vector_cbrt_exp2_poly_t_10_0h = vector_cbrt_exp2_poly_t_9_0h * x;
  vector_cbrt_exp2_poly_t_11_0h = vector_cbrt_exp2_poly_coeff_6h + vector_cbrt_exp2_poly_t_10_0h;
  vector_cbrt_exp2_poly_t_12_0h = vector_cbrt_exp2_poly_t_11_0h * x;
  vector_cbrt_exp2_poly_t_13_0h = vector_cbrt_exp2_poly_coeff_5h + vector_cbrt_exp2_poly_t_12_0h;
  vector_cbrt_exp2_poly_t_14_0h = vector_cbrt_exp2_poly_t_13_0h * x;
  vector_cbrt_exp2_poly_t_15_0h = vector_cbrt_exp2_poly_coeff_4h + vector_cbrt_exp2_poly_t_14_0h;
  vector_cbrt_exp2_poly_t_16_0h = vector_cbrt_exp2_poly_t_15_0h * x;
  vector_cbrt_exp2_poly_t_17_0h = vector_cbrt_exp2_poly_coeff_3h + vector_cbrt_exp2_poly_t_16_0h;
  vector_cbrt_exp2_poly_t_18_0h = vector_cbrt_exp2_poly_t_17_0h * x;
  vector_cbrt_exp2_poly_t_19_0h = vector_cbrt_exp2_poly_coeff_2h + vector_cbrt_exp2_poly_t_18_0h;
  vector_cbrt_exp2_poly_t_20_0h = vector_cbrt_exp2_poly_t_19_0h * x;
  vector_cbrt_exp2_poly_t_21_0h = vector_cbrt_exp2_poly_coeff_1h + vector_cbrt_exp2_poly_t_20_0h;
  vector_cbrt_exp2_poly_t_22_0h = vector_cbrt_exp2_poly_t_21_0h * x;
  vector_cbrt_exp2_poly_t_23_0h = vector_cbrt_exp2_poly_coeff_0h + vector_cbrt_exp2_poly_t_22_0h;
  *vector_cbrt_exp2_poly_resh = vector_cbrt_exp2_poly_t_23_0h;


}


/* A scalar cubic root for normal, real inputs */
STATIC INLINE double scalar_cbrt_callout_inner(double x) {
  dblcast xdb, ydb;
  uint64_t sign;
  int64_t E, F, G;
  uint64_t R;
  double a, z, y;
  uint64_t tui1, tui2, tui3;
  int EE;
  double eeDouble, m, r, ll, eelog82h, eelog82l, p, lh, t1, t2, shiftedLh;
  double gDouble, t;
  
  /* Strip off sign and absolute value */
  xdb.d = x;
  sign = (xdb.i >> 63) << 63;
  xdb.i = (xdb.i << 1) >> 1;

  /* Get exponent */
  E = ((int64_t) (xdb.i >> 52)) - ((int64_t) 1023);

  /* Compute F = floor(E / 3) as 

     F = floor(((E + 1074) * (round(1/3,41,RU) * 2^43)) * 2^-43) - 358

     The equivalence can be shown with a simple test loop.

  */
  F = ((int64_t) ((((uint64_t) (E + ((int64_t) 1074))) *
		   ((uint64_t) CBRT_ONE_THIRD_41_BITS_TIMES_TWO_43)) >> 43)) -
    ((int64_t) 358);
  
  /* Compute R = E - F * 3 
     
     It is clear that 0 <= R <= 2.

  */
  R = ((uint64_t) (E - F * ((int64_t) 3)));

  /* Compute a = 2^R * m, where m is the mantissa of x

     Therefore 

     abs(x) = 2^(3 * F) * a

     The value a is held in xdb.d but never written back.

  */
  xdb.i = (xdb.i << 12) >> 12;
  xdb.i |= (R + ((uint64_t) 1023)) << 52;

  /* Compute logarithm log8(a) = log(a)/log(8)

     as non-evaluated overlapping sum lh + ll. 

  */
  tui1 = xdb.i;
  tui2 = tui1 + 0x0008000000000000ull;
  tui1 >>= 52;
  tui2 >>= 52;
  tui3 = tui2 - tui1;
  tui3 <<= 52;
  EE = ((int) tui2) - 1023;
  eeDouble = (double) EE;
  xdb.i = ((xdb.i & 0x000fffffffffffffull) | 0x3ff0000000000000ull) - tui3;
  m = xdb.d;                     
  r = m - 1.0;                   
  vector_cbrt_log8_poly(&p, r);
  eelog82h = eeDouble * CBRT_ONE_THIRD_HI; 
  eelog82l = eeDouble * CBRT_ONE_THIRD_LO;
  lh = eelog82h + p;
  t2 = lh - eelog82h;
  t1 = p - t2;                  
  ll = eelog82l + t1;

  /* Compute 2^G * z = 2^(lh + ll) = cbrt(a) */
  shiftedLh = lh + CBRT_SHIFTER;
  gDouble = shiftedLh - CBRT_SHIFTER;
  G = (int64_t) ((int) gDouble);
  t = lh - gDouble;
  r = t + ll;
  vector_cbrt_exp2_poly(&z, r);
  
  /* Reconstruct cbrt(x) as follows:

     y = cbrt(x) = sgn(x) * cbrt(abs(x)) =
                 = sgn(x) * 2^F * cbrt(a) =
                 = sgn(x) * 2^(F + G) * z

  */
  ydb.d = z;
  ydb.i += (F + G) << 52;
  ydb.i |= sign;
  y = ydb.d;

  /* Return the result */
  return y;
}

/* A scalar cubic root for the callout */
STATIC INLINE double scalar_cbrt_callout(double x) {
  dblcast xdb, xAbsdb;

  xdb.d = x;
  xAbsdb.i = xdb.i & 0x7fffffffffffffffull;
  if (xAbsdb.i >= 0x7ff0000000000000ull) {
    /* Here, the input is NaN or Inf 

       cbrt(q/sNaN) = qNaN, quietized if sNaN
       cbrt(+/-Inf) = +/-Inf

    */
    if (xAbsdb.i == 0x7ff0000000000000ull) {
      /* Infinity, return the value as-is */
      return x;
    }

    /* NaN, return the quietized NaN */
    return x + 1.0;
  }

  /* Check for zero input 

     cbrt(+/-0) = +/-0

  */
  if (x == 0.0) {
    return x;
  }
  
  /* Here the input is a non-zero real 

     Check for subnormals first.

  */
  if (xAbsdb.i < 0x0010000000000000ull) {
    /* Subnormal input */
    return CBRT_TWO_M_18 * scalar_cbrt_callout_inner(CBRT_TWO_54 * x);
  }

  /* Here, the input is real and normalized thru the scaling. */  
  return scalar_cbrt_callout_inner(x);
}

/* A vector cubic root callout */
STATIC INLINE void vector_cbrt_callout(double * RESTRICT y, CONST double * RESTRICT x) {
  int i;

  for (i=0;i<VECTOR_LENGTH;i++) {
    y[i] = scalar_cbrt_callout(x[i]);
  }
}

/* A vector cubic root */
void vector_cbrt(double * RESTRICT yArg, CONST double * RESTRICT xArg) {
  int i;
  int okaySlots;
  double * RESTRICT y;
  CONST double * RESTRICT x;
  double currX, currY, xAbs;
  dblcast xdb, ydb;
  uint64_t sign;
  int64_t E, F, G;
  uint64_t R;
  double a, z;
  uint64_t tui1, tui2, tui3;
  int EE;
  double eeDouble, m, r, ll, eelog82h, eelog82l, p, lh, t1, t2, shiftedLh;
  double gDouble, t;
  
  /* Assume alignment */
#ifdef NO_ASSUME_ALIGNED
  x = xArg;
  y = yArg;
#else
  x = __builtin_assume_aligned(xArg, VECTOR_LENGTH * __alignof__(double));
  y = __builtin_assume_aligned(yArg, VECTOR_LENGTH * __alignof__(double));
#endif  

  /* Check if we can handle all inputs */
  okaySlots = 0;
  for (i=0;i<VECTOR_LENGTH;i++) {
    xdb.d = x[i];
    xdb.i &= 0x7fffffffffffffffull;
    okaySlots += ((xdb.i >= 0x0010000000000000ull) && (xdb.i < 0x7ff0000000000000ull));
  }

  /* Perform a callout if we cannot handle the input in one slot */
  if (okaySlots != VECTOR_LENGTH) {
    vector_cbrt_callout(yArg, xArg);
    return;
  }
  
  /* Here we know that all inputs are real and in range. */
  for (i=0;i<VECTOR_LENGTH;i++) {
    /* Get current element in vector */
    currX = x[i];

    /* Strip off sign and absolute value */
    xdb.d = currX;
    sign = (xdb.i >> 63) << 63;
    xdb.i = (xdb.i << 1) >> 1;

    /* Get exponent */
    E = ((int64_t) (xdb.i >> 52)) - ((int64_t) 1023);

    /* Compute F = floor(E / 3) as 

       F = floor(((E + 1074) * (round(1/3,41,RU) * 2^43)) * 2^-43) - 358

       The equivalence can be shown with a simple test loop.

    */
    F = ((int64_t) ((((uint64_t) (E + ((int64_t) 1074))) *
		     ((uint64_t) CBRT_ONE_THIRD_41_BITS_TIMES_TWO_43)) >> 43)) -
      ((int64_t) 358);
  
    /* Compute R = E - F * 3 
     
       It is clear that 0 <= R <= 2.

    */
    R = ((uint64_t) (E - F * ((int64_t) 3)));

    /* Compute a = 2^R * m, where m is the mantissa of x

       Therefore 

       abs(x) = 2^(3 * F) * a

       The value a is held in xdb.d but never written back.

    */
    xdb.i = (xdb.i << 12) >> 12;
    xdb.i |= (R + ((uint64_t) 1023)) << 52;

    /* Compute logarithm log8(a) = log(a)/log(8)

       as non-evaluated overlapping sum lh + ll. 

    */
    tui1 = xdb.i;
    tui2 = tui1 + 0x0008000000000000ull;
    tui1 >>= 52;
    tui2 >>= 52;
    tui3 = tui2 - tui1;
    tui3 <<= 52;
    EE = ((int) tui2) - 1023;
    eeDouble = (double) EE;
    xdb.i = ((xdb.i & 0x000fffffffffffffull) | 0x3ff0000000000000ull) - tui3;
    m = xdb.d;                     
    r = m - 1.0;                   
    vector_cbrt_log8_poly(&p, r);
    eelog82h = eeDouble * CBRT_ONE_THIRD_HI; 
    eelog82l = eeDouble * CBRT_ONE_THIRD_LO;
    lh = eelog82h + p;
    t2 = lh - eelog82h;
    t1 = p - t2;                  
    ll = eelog82l + t1;

    /* Compute 2^G * z = 2^(lh + ll) = cbrt(a) */
    shiftedLh = lh + CBRT_SHIFTER;
    gDouble = shiftedLh - CBRT_SHIFTER;
    G = (int64_t) ((int) gDouble);
    t = lh - gDouble;
    r = t + ll;
    vector_cbrt_exp2_poly(&z, r);
  
    /* Reconstruct cbrt(x) as follows:

       y = cbrt(x) = sgn(x) * cbrt(abs(x)) =
       = sgn(x) * 2^F * cbrt(a) =
       = sgn(x) * 2^(F + G) * z

    */
    ydb.d = z;
    ydb.i += (F + G) << 52;
    ydb.i |= sign;
    currY = ydb.d;

    /* Write result back into output vector */
    y[i] = currY;
  }
}
