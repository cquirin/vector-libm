/* This program implements a show-case vector (vectorizable) double
   precision arctangent with a 6 ulp error bound.

   Author: Christoph Lauter,

           Sorbonne Université - LIP6 - PEQUAN team.

   This program is

   Copyright 2014-2018 Christoph Lauter Sorbonne Université

   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions
   are met:

   1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.

   2. Redistributions in binary form must reproduce the above
   copyright notice, this list of conditions and the following
   disclaimer in the documentation and/or other materials provided
   with the distribution.

   3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived
   from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
   FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
   COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
   INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
   (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
   SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
   HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
   STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
   OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#include <stdint.h>
#include "atan.h"

/* Two caster types */
typedef union _dblcast {
  double   d;
  uint64_t i;
} dblcast;

typedef union {
  int64_t l;
  double d;
} db_number;

/* Need fabs and sqrt */
double fabs(double);


/* Constants and polynomial coefficients */
#define ATAN_PI_OVER_TWO         1.5707963267948965579989817342720925807952880859375
#define ATAN_EASY_BOUND          1.86264514923095703125e-9
#define ATAN_REDUC_BOUND         1.8586365008139461851843776685200282372534275054931640625e-2
#define ATAN_MAX_BOUND           5805358775541311.0
#define ATAN_ARG_RED_MAX_BOUND   2853.07976884962909025489352643489837646484375
#define ATAN_PI_HALF             1.5707963267948965579989817342720925807952880859375
#define ATAN_REALLY_SMALL        8.67361737988403547205962240695953369140625e-19

#define atan_seed_coeff_0   0.63661977236758138243288840385503135621547698974609375
#define atan_seed_coeff_1   0.4377590354789315796324444818310439586639404296875
#define atan_seed_coeff_2   0.1980918322449655988304328957383404485881328582763671875
#define atan_seed_coeff_3   -2.8285150539027303917549716061330400407314300537109375e-2
#define atan_seed_coeff_4   1.8069858063243450681245594324764169869013130664825439453125e-3
#define atan_seed_coeff_5   -5.19928852837294952168346873211390857250080443918704986572265625e-5
#define atan_seed_coeff_6   5.496657304820126682310299677525211592410414596088230609893798828125e-7

#define atan_sin_coeff_1    1.0
#define atan_sin_coeff_3    -0.1666666666666666574148081281236954964697360992431640625
#define atan_sin_coeff_5    8.3333333333331961745304994337857351638376712799072265625e-3
#define atan_sin_coeff_7    -1.98412698412076785210722551511253186617977917194366455078125e-4
#define atan_sin_coeff_9    2.7557319210712928579123069672274226604713476262986660003662109375e-6
#define atan_sin_coeff_11    -2.50521068275393895412688741348994359015023292158730328083038330078125e-8
#define atan_sin_coeff_13    1.605893730039107593532370989411364563259621007773603196255862712860107421875e-10
#define atan_sin_coeff_15    -7.64292797322630491634536327970794307939093881198999724801979027688503265380859375e-13
#define atan_sin_coeff_17    2.7205099806011472209783930384455285382661157873862567413425495033152401447296142578125e-15

#define atan_cos_coeff_0    1.0
#define atan_cos_coeff_2    -0.499999999999999833466546306226518936455249786376953125
#define atan_cos_coeff_4    4.16666666666644369687588778106146492063999176025390625e-2
#define atan_cos_coeff_6    -1.3888888888787683482950452429349752492271363735198974609375e-3
#define atan_cos_coeff_8    2.480158727945032249412836422575168171533732675015926361083984375e-5
#define atan_cos_coeff_10    -2.755731654976660736857015099321444040469941683113574981689453125e-7
#define atan_cos_coeff_12    2.08765688589344469212105283675330602566333482172922231256961822509765625e-9
#define atan_cos_coeff_14    -1.146306609681090947095966068840634626002650264808835345320403575897216796875e-11
#define atan_cos_coeff_16    4.61055538664679155560546551674512047623512474292528651176326093263924121856689453125e-14

#define atan_reduc_coeff_1   1.0
#define atan_reduc_coeff_3   -0.33333333333247494589812731646816246211528778076171875
#define atan_reduc_coeff_5   0.1999999850903287634817928619668236933648586273193359375
#define atan_reduc_coeff_7   -0.1427831456348924443755521451748791150748729705810546875

#define atan_large_poly_coeff_0   1.5707963267948965579989817342720925807952880859375
#define atan_large_poly_coeff_1   -0.99999999999860544885876834086957387626171112060546875
#define atan_large_poly_coeff_2   -7.9402326386164272717482122691985690909888262467575259506702423095703125e-9
#define atan_large_poly_coeff_3   0.33334623574317612249018338843598030507564544677734375

STATIC INLINE double scalar_atan_callout_inner(double x) {
  double xAbs, xsq;
  double y;
  dblcast xdb, ydb;
  double q, h, hsq, s, c, t, r, rsq, p, yAbs;

  /* Get absolute value of x for checks */
  xAbs = fabs(x);

  /* If x is smaller than 2^-29 (in magnitude), we can simply return
     x 
  */
  if (xAbs < ATAN_EASY_BOUND) {
    return x;
  }

  /* If x is larger than a certain maximum bound, we can simply return
     +/- pi/2 (with the sign of x), setting inexact 
  */
  if (xAbs > ATAN_MAX_BOUND) {
    y = ATAN_PI_HALF;
    if (x < 0.0) {
      y = -y;
    }
    y += ATAN_REALLY_SMALL; /* setting inexact */
    return y;
  }

  /* If x is smaller than the bound for the reduced-range polynomial,
     we need to evaluate nothing but that polynomial.
  */
  if (xAbs < ATAN_REDUC_BOUND) {
    /* Square x once */
    xsq = x * x;

    /* Evaluate the polynomial */
    y = x * (atan_reduc_coeff_1 +
      xsq * (atan_reduc_coeff_3 +
      xsq * (atan_reduc_coeff_5 +
      xsq * atan_reduc_coeff_7)));

    /* Return the result */
    return y;
  }

  /* Here the input is at least the bound for the reduced-range
     polynomial.

     If the argument is larger that what is allowed for the main
     path argument reduction, we have to compute atan with 
     a special path. We proceed as follows:

     atan(x) = g(r)  

     where r = 1/x and

     g(x) = atan(1/x) is approximated with a polynomial.
     
  */
  if (xAbs >= ATAN_ARG_RED_MAX_BOUND) {
    /* Compute r = 1/abs(x) */
    r = 1.0 / xAbs;

    /* Evaluate the polynomial */
    yAbs = atan_large_poly_coeff_0 +
      r * (atan_large_poly_coeff_1 + 
      r * (atan_large_poly_coeff_2 +
      r * (atan_large_poly_coeff_3)));
    
    /* atan(x) is atan(abs(x)) followed by copy-sign */
    xdb.d = x;
    ydb.d = yAbs;
    xdb.i >>= 63;
    xdb.i <<= 63;
    ydb.i |= xdb.i;
    y = ydb.d;

    /* Return the result */
    return y;
  }
 
  /* Main path, using argument reduction 

     Get a first guess h on atan(x), by evaluating first q 

  */
  q =       atan_seed_coeff_0 +
    xAbs * (atan_seed_coeff_1 +
    xAbs * (atan_seed_coeff_2 +
    xAbs * (atan_seed_coeff_3 +
    xAbs * (atan_seed_coeff_4 +
    xAbs * (atan_seed_coeff_5 +
    xAbs *  atan_seed_coeff_6)))));

  /* Now compute h as h(x) = pi/2 - 1/q 

     This operation will heavily cancel around 0 but we
     need only the absolute error to be bounded.

  */
  h = ATAN_PI_OVER_TWO - 1.0/q;

  /* Square h once */
  hsq = h * h;
  
  /* Now compute tan(h) as sin(h) / cos(h), 
     approximating sine by s and cosine by c 
  */
  s = h * (atan_sin_coeff_1 +
    hsq * (atan_sin_coeff_3 +
    hsq * (atan_sin_coeff_5 +
    hsq * (atan_sin_coeff_7 +
    hsq * (atan_sin_coeff_9 +
    hsq * (atan_sin_coeff_11 +
    hsq * (atan_sin_coeff_13 +
    hsq * (atan_sin_coeff_15 +
    hsq *  atan_sin_coeff_17))))))));

  c =      atan_cos_coeff_0 +
    hsq * (atan_cos_coeff_2 +
    hsq * (atan_cos_coeff_4 +
    hsq * (atan_cos_coeff_6 +
    hsq * (atan_cos_coeff_8 +
    hsq * (atan_cos_coeff_10 +
    hsq * (atan_cos_coeff_12 +
    hsq * (atan_cos_coeff_14 +
    hsq *  atan_cos_coeff_16)))))));

  /* Compute t = tan(h) out of s = sin(h) and c = cos(h) */
  t = s / c;

  /* We have 

     atan(x) = atan(t) + atan((x - t) / (1 + x * t))
             = h       + atan(r)

     with the reduced argument 

     r = (x - t) / (1 + x * t)

     The subtraction x - t will cancel, this is however 
     not a problem as we need only an absolute error bound on r.

     The product x * t, which typically behaves like x^2, 
     will stay sufficiently small to avoid overflow and sufficiently
     large to avoid underflow.
     
  */
  r = (xAbs - t) / (1.0 + xAbs * t);

  /* Square r once */
  rsq = r * r;

  /* Compute atan(r) by evaluating an approximation polynomial p */
  p = r * (atan_reduc_coeff_1 +
    rsq * (atan_reduc_coeff_3 +
    rsq * (atan_reduc_coeff_5 +
    rsq * atan_reduc_coeff_7)));

  /* Reconstruct atan(x) as atan(abs(x)) = h + p followed by copysign */
  yAbs = h + p;
  xdb.d = x;
  ydb.d = yAbs;
  xdb.i >>= 63;
  xdb.i <<= 63;
  ydb.i |= xdb.i;
  y = ydb.d;
  
  /* Return the result */
  return y;
}

/* A scalar arctangent for the callout */
STATIC INLINE double scalar_atan_callout(double x) {
  dblcast xdb, xabsdb, tempdb;

  /* Perform special case handling */
  xdb.d = x;
  xabsdb.i = xabsdb.i & 0x7fffffffffffffffull;
  if (xabsdb.i >= 0x7ff0000000000000ull) {
    /* Input is +/-infinity or s/qNaN */
    if (xabsdb.i == 0x7ff0000000000000ull) {
      /* Input is +/- infinity 

	 atan(+/-infinity) = +/-pi/2, setting inexact.

      */
      tempdb.d = ATAN_PI_HALF;
      tempdb.i |= xdb.i & 0x8000000000000000ull;
      return tempdb.d + ATAN_REALLY_SMALL;
    }

    /* Here, the input is q/sNaN 

       Return the quietized NaN.

    */
    return x + 1.0;
  }

  /* The input is real and larger that the short-part bound 

     Encapsulate that code in a function.

  */
  return scalar_atan_callout_inner(x);
}

/* A vector arctangent callout */
STATIC INLINE void vector_atan_callout(double * RESTRICT y, CONST double * RESTRICT x) {
  int i;

  for (i=0;i<VECTOR_LENGTH;i++) {
    y[i] = scalar_atan_callout(x[i]);
  }
}

/* A vector arctangent */
void vector_atan(double * RESTRICT yArg, CONST double * RESTRICT xArg) {
  int i;
  int okaySlots;
  double * RESTRICT y;
  CONST double * RESTRICT x;
  double currX, currY;
  double xAbs;
  double q, h, hsq, s, c, t, r, rsq, p, yAbs;
  dblcast xdb, ydb;

  /* Assume alignment */
#ifdef NO_ASSUME_ALIGNED
  x = xArg;
  y = yArg;
#else
  x = __builtin_assume_aligned(xArg, VECTOR_LENGTH * __alignof__(double));
  y = __builtin_assume_aligned(yArg, VECTOR_LENGTH * __alignof__(double));
#endif  

  /* Check if we can handle all inputs */
  okaySlots = 0;
  for (i=0;i<VECTOR_LENGTH;i++) {
    xAbs = fabs(x[i]);
    okaySlots += ((ATAN_EASY_BOUND <= xAbs) & (xAbs <= ATAN_ARG_RED_MAX_BOUND)); 
  }

  /* Perform a callout if we cannot handle the input in one slot */
  if (okaySlots != VECTOR_LENGTH) {
    vector_atan_callout(yArg, xArg);
    return;
  }

  /* Here we know that all inputs are real and in range. */
  for (i=0;i<VECTOR_LENGTH;i++) {
    /* Get current element in vector */
    currX = x[i];

    /* Get absolute value of current x 

       We will compute atan(x) as atan(abs(x)) followed by a copysign.

    */
    xAbs = fabs(currX);

    /* Get a first guess h on atan(x), by evaluating first q */
    q =       atan_seed_coeff_0 +
      xAbs * (atan_seed_coeff_1 +
      xAbs * (atan_seed_coeff_2 +
      xAbs * (atan_seed_coeff_3 +
      xAbs * (atan_seed_coeff_4 +
      xAbs * (atan_seed_coeff_5 +
      xAbs *  atan_seed_coeff_6)))));

    /* Now compute h as h(x) = pi/2 - 1/q 

       This operation will heavily cancel around 0 but we
       need only the absolute error to be bounded.

    */
    h = ATAN_PI_OVER_TWO - 1.0/q;

    /* Square h once */
    hsq = h * h;
    
    /* Now compute tan(h) as sin(h) / cos(h), 
       approximating sine by s and cosine by c 
    */
    s = h * (atan_sin_coeff_1 +
      hsq * (atan_sin_coeff_3 +
      hsq * (atan_sin_coeff_5 +
      hsq * (atan_sin_coeff_7 +
      hsq * (atan_sin_coeff_9 +
      hsq * (atan_sin_coeff_11 +
      hsq * (atan_sin_coeff_13 +
      hsq * (atan_sin_coeff_15 +
      hsq *  atan_sin_coeff_17))))))));

    c =      atan_cos_coeff_0 +
      hsq * (atan_cos_coeff_2 +
      hsq * (atan_cos_coeff_4 +
      hsq * (atan_cos_coeff_6 +
      hsq * (atan_cos_coeff_8 +
      hsq * (atan_cos_coeff_10 +
      hsq * (atan_cos_coeff_12 +
      hsq * (atan_cos_coeff_14 +
      hsq *  atan_cos_coeff_16)))))));

    /* Compute t = tan(h) out of s = sin(h) and c = cos(h) */
    t = s / c;

    /* We have 

       atan(x) = atan(t) + atan((x - t) / (1 + x * t))
               = h       + atan(r)

       with the reduced argument 

       r = (x - t) / (1 + x * t)

       The subtraction x - t will cancel, this is however 
       not a problem as we need only an absolute error bound on r.

       The product x * t, which typically behaves like x^2, 
       will stay sufficiently small to avoid overflow and sufficiently
       large to avoid underflow.
       
    */
    r = (xAbs - t) / (1.0 + xAbs * t);

    /* Square r once */
    rsq = r * r;

    /* Compute atan(r) by evaluating an approximation polynomial p */
    p = r * (atan_reduc_coeff_1 +
      rsq * (atan_reduc_coeff_3 +
      rsq * (atan_reduc_coeff_5 +
      rsq * atan_reduc_coeff_7)));

    /* Reconstruct atan(x) as atan(abs(x)) = h + p followed by copysign */
    yAbs = h + p;
    xdb.d = currX;
    ydb.d = yAbs;
    xdb.i >>= 63;
    xdb.i <<= 63;
    ydb.i |= xdb.i;
    currY = ydb.d;
    
    /* Write result back into output vector */
    y[i] = currY;
  }
}
