Implements the most common functions found in a libm (math library) in
vectorizable C.

The following functions are currently available:

* exp
* log
* sin
* cos
* tan
* asin
* acos
* atan
* cbrt

