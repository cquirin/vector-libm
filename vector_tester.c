/* This program serves as a test driver for a vectorized double
   precision libm.

   Author: Christoph Lauter,

           Sorbonne Université - LIP6 - PEQUAN team.

   This program is

   Copyright 2014-2018 Christoph Lauter Sorbonne Université

   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions
   are met:

   1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.

   2. Redistributions in binary form must reproduce the above
   copyright notice, this list of conditions and the following
   disclaimer in the documentation and/or other materials provided
   with the distribution.

   3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived
   from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
   FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
   COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
   INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
   (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
   SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
   HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
   STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
   OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <sys/time.h>
#include <string.h>
#include <mpfr.h>
#include "vector.h"
#include "exp.h"
#include "log.h"
#include "sin.h"
#include "cos.h"
#include "tan.h"
#include "asin.h"
#include "acos.h"
#include "atan.h"
#include "cbrt.h"

#define __ALWAYS_FALSE (0)

double fabs(double);
double exp(double);
double log(double);
double sin(double);
double cos(double);
double tan(double);
double asin(double);
double acos(double);
double atan(double);
double cbrt(double);

#define TESTCASE_VALID_TESTCASES     (40)
typedef enum __testcase_t {
  TESTCASE_NORMAL_EXP,
  TESTCASE_REGULAR_EXP,
  TESTCASE_NORMAL_LOG,
  TESTCASE_REGULAR_LOG,
  TESTCASE_SMALL_SINE,
  TESTCASE_REASONABLE_SINE,
  TESTCASE_LARGE_SINE,
  TESTCASE_SUBNORMAL_SINE,
  TESTCASE_SMALL_COSINE,
  TESTCASE_REASONABLE_COSINE,
  TESTCASE_LARGE_COSINE,
  TESTCASE_SUBNORMAL_INPUT_COSINE,
  TESTCASE_SMALL_TANGENT,
  TESTCASE_REASONABLE_TANGENT,
  TESTCASE_LARGE_TANGENT,
  TESTCASE_SUBNORMAL_TANGENT,
  TESTCASE_NORMAL_ASIN,
  TESTCASE_REGULAR_ASIN,
  TESTCASE_SMALL_ASIN,
  TESTCASE_REASONABLE_ASIN,
  TESTCASE_REGULAR_ACOS,
  TESTCASE_SMALL_ACOS,
  TESTCASE_REASONABLE_ACOS,
  TESTCASE_NORMAL_ATAN,
  TESTCASE_REGULAR_ATAN,
  TESTCASE_SMALL_ATAN,
  TESTCASE_REASONABLE_ATAN,
  TESTCASE_NORMAL_CBRT,
  TESTCASE_REGULAR_CBRT,
  TESTCASE_REASONABLE_CBRT,  
  TESTCASE_QNAN,
  TESTCASE_SNAN,
  TESTCASE_P_INF,
  TESTCASE_N_INF,
  TESTCASE_ALL_REAL,
  TESTCASE_P_REAL,
  TESTCASE_N_REAL,
  TESTCASE_P_ZERO,
  TESTCASE_N_ZERO,
  TESTCASE_ANY_ZERO,
  TESTCASE_ALL,
  TESTCASE_UNKNOWN
} testcase_t;

typedef union _dblcast {
  double   d;
  uint64_t i;
} dblcast;

typedef enum __testfunc_t {
  TESTFUNC_EXP,
  TESTFUNC_LOG,
  TESTFUNC_SIN,
  TESTFUNC_COS,
  TESTFUNC_TAN,
  TESTFUNC_ASIN,
  TESTFUNC_ACOS,
  TESTFUNC_ATAN,
  TESTFUNC_CBRT
} testfunc_t;

typedef struct __testfuncdescription_t {
  testfunc_t testFunc;
  testcase_t testCases[TESTCASE_VALID_TESTCASES];
  void (*naive_long_vector_func)(double * RESTRICT, CONST double * RESTRICT, unsigned int);
  void (*optimized_long_vector_func)(double * RESTRICT, CONST double * RESTRICT, unsigned int);
  void (*check_func)(double * RESTRICT, int *, double, double, double, int);
} testfuncdescription_t;

void naive_long_vector_exp(double * RESTRICT, CONST double * RESTRICT, unsigned int);
void optimized_long_vector_exp(double * RESTRICT, CONST double * RESTRICT, unsigned int);
void check_exp(double * RESTRICT, int *, double, double, double, int);

void naive_long_vector_log(double * RESTRICT, CONST double * RESTRICT, unsigned int);
void optimized_long_vector_log(double * RESTRICT, CONST double * RESTRICT, unsigned int);
void check_log(double * RESTRICT, int *, double, double, double, int);

void naive_long_vector_sin(double * RESTRICT, CONST double * RESTRICT, unsigned int);
void optimized_long_vector_sin(double * RESTRICT, CONST double * RESTRICT, unsigned int);
void check_sin(double * RESTRICT, int *, double, double, double, int);

void naive_long_vector_cos(double * RESTRICT, CONST double * RESTRICT, unsigned int);
void optimized_long_vector_cos(double * RESTRICT, CONST double * RESTRICT, unsigned int);
void check_cos(double * RESTRICT, int *, double, double, double, int);

void naive_long_vector_tan(double * RESTRICT, CONST double * RESTRICT, unsigned int);
void optimized_long_vector_tan(double * RESTRICT, CONST double * RESTRICT, unsigned int);
void check_tan(double * RESTRICT, int *, double, double, double, int);

void naive_long_vector_asin(double * RESTRICT, CONST double * RESTRICT, unsigned int);
void optimized_long_vector_asin(double * RESTRICT, CONST double * RESTRICT, unsigned int);
void check_asin(double * RESTRICT, int *, double, double, double, int);

void naive_long_vector_acos(double * RESTRICT, CONST double * RESTRICT, unsigned int);
void optimized_long_vector_acos(double * RESTRICT, CONST double * RESTRICT, unsigned int);
void check_acos(double * RESTRICT, int *, double, double, double, int);

void naive_long_vector_atan(double * RESTRICT, CONST double * RESTRICT, unsigned int);
void optimized_long_vector_atan(double * RESTRICT, CONST double * RESTRICT, unsigned int);
void check_atan(double * RESTRICT, int *, double, double, double, int);

void naive_long_vector_cbrt(double * RESTRICT, CONST double * RESTRICT, unsigned int);
void optimized_long_vector_cbrt(double * RESTRICT, CONST double * RESTRICT, unsigned int);
void check_cbrt(double * RESTRICT, int *, double, double, double, int);


#define TESTCASE_VALID_TESTFUNCDESCRIPTIONS     (9)
STATIC testfuncdescription_t testFuncDescriptions[TESTCASE_VALID_TESTFUNCDESCRIPTIONS] = {
  { /* exponential */
    TESTFUNC_EXP,
    { TESTCASE_NORMAL_EXP, 
      TESTCASE_REGULAR_EXP, 
      TESTCASE_QNAN, 
      TESTCASE_SNAN, 
      TESTCASE_P_INF, 
      TESTCASE_N_INF, 
      TESTCASE_ALL_REAL, 
      TESTCASE_ALL, 
      TESTCASE_UNKNOWN 
    },
    naive_long_vector_exp,
    optimized_long_vector_exp,
    check_exp
  },
  { /* logarithm */
    TESTFUNC_LOG,
    { TESTCASE_NORMAL_LOG, 
      TESTCASE_REGULAR_LOG, 
      TESTCASE_QNAN, 
      TESTCASE_SNAN, 
      TESTCASE_P_INF, 
      TESTCASE_N_INF, 
      TESTCASE_P_REAL, 
      TESTCASE_P_ZERO, 
      TESTCASE_N_ZERO, 
      TESTCASE_ANY_ZERO, 
      TESTCASE_ALL_REAL, 
      TESTCASE_ALL, 
      TESTCASE_UNKNOWN 
    },
    naive_long_vector_log,
    optimized_long_vector_log,
    check_log
  },
  { /* sine */
    TESTFUNC_SIN,
    { TESTCASE_SMALL_SINE,
      TESTCASE_REASONABLE_SINE,
      TESTCASE_LARGE_SINE,
      TESTCASE_SUBNORMAL_SINE,
      TESTCASE_QNAN, 
      TESTCASE_SNAN, 
      TESTCASE_P_INF, 
      TESTCASE_N_INF, 
      TESTCASE_P_ZERO, 
      TESTCASE_N_ZERO, 
      TESTCASE_ANY_ZERO, 
      TESTCASE_ALL_REAL, 
      TESTCASE_ALL, 
      TESTCASE_UNKNOWN 
    },
    naive_long_vector_sin,
    optimized_long_vector_sin,
    check_sin
  },
  { /* cosine */
    TESTFUNC_COS,
    { TESTCASE_SMALL_COSINE,
      TESTCASE_REASONABLE_COSINE,
      TESTCASE_LARGE_COSINE,
      TESTCASE_SUBNORMAL_INPUT_COSINE,
      TESTCASE_QNAN, 
      TESTCASE_SNAN, 
      TESTCASE_P_INF, 
      TESTCASE_N_INF, 
      TESTCASE_P_ZERO, 
      TESTCASE_N_ZERO, 
      TESTCASE_ANY_ZERO, 
      TESTCASE_ALL_REAL, 
      TESTCASE_ALL, 
      TESTCASE_UNKNOWN 
    },
    naive_long_vector_cos,
    optimized_long_vector_cos,
    check_cos
  },
  { /* tangent */
    TESTFUNC_TAN,
    { TESTCASE_SMALL_TANGENT,
      TESTCASE_REASONABLE_TANGENT,
      TESTCASE_LARGE_TANGENT,
      TESTCASE_SUBNORMAL_TANGENT,
      TESTCASE_QNAN, 
      TESTCASE_SNAN, 
      TESTCASE_P_INF, 
      TESTCASE_N_INF, 
      TESTCASE_P_ZERO, 
      TESTCASE_N_ZERO, 
      TESTCASE_ANY_ZERO, 
      TESTCASE_ALL_REAL, 
      TESTCASE_ALL, 
      TESTCASE_UNKNOWN 
    },
    naive_long_vector_tan,
    optimized_long_vector_tan,
    check_tan
  },
  { /* arcsine */
    TESTFUNC_ASIN,
    { TESTCASE_NORMAL_ASIN,
      TESTCASE_REGULAR_ASIN,
      TESTCASE_SMALL_ASIN,
      TESTCASE_REASONABLE_ASIN,
      TESTCASE_QNAN, 
      TESTCASE_SNAN, 
      TESTCASE_P_INF, 
      TESTCASE_N_INF, 
      TESTCASE_P_ZERO, 
      TESTCASE_N_ZERO, 
      TESTCASE_ANY_ZERO, 
      TESTCASE_ALL_REAL, 
      TESTCASE_ALL, 
      TESTCASE_UNKNOWN 
    },
    naive_long_vector_asin,
    optimized_long_vector_asin,
    check_asin
  },
  { /* arccosine */
    TESTFUNC_ACOS,
    { TESTCASE_REGULAR_ACOS,
      TESTCASE_SMALL_ACOS,
      TESTCASE_REASONABLE_ACOS,
      TESTCASE_QNAN, 
      TESTCASE_SNAN, 
      TESTCASE_P_INF, 
      TESTCASE_N_INF, 
      TESTCASE_P_ZERO, 
      TESTCASE_N_ZERO, 
      TESTCASE_ANY_ZERO, 
      TESTCASE_ALL_REAL, 
      TESTCASE_ALL, 
      TESTCASE_UNKNOWN 
    },
    naive_long_vector_acos,
    optimized_long_vector_acos,
    check_acos
  },
  { /* arctangent */
    TESTFUNC_ATAN,
    { TESTCASE_NORMAL_ATAN,
      TESTCASE_REGULAR_ATAN,
      TESTCASE_SMALL_ATAN,
      TESTCASE_REASONABLE_ATAN,
      TESTCASE_QNAN, 
      TESTCASE_SNAN, 
      TESTCASE_P_INF, 
      TESTCASE_N_INF, 
      TESTCASE_P_ZERO, 
      TESTCASE_N_ZERO, 
      TESTCASE_ANY_ZERO, 
      TESTCASE_ALL_REAL, 
      TESTCASE_ALL, 
      TESTCASE_UNKNOWN 
    },
    naive_long_vector_atan,
    optimized_long_vector_atan,
    check_atan
  },
  { /* cubic root */
    TESTFUNC_CBRT,
    { TESTCASE_NORMAL_CBRT,
      TESTCASE_REGULAR_CBRT,
      TESTCASE_REASONABLE_CBRT,
      TESTCASE_QNAN, 
      TESTCASE_SNAN, 
      TESTCASE_P_INF, 
      TESTCASE_N_INF, 
      TESTCASE_P_ZERO, 
      TESTCASE_N_ZERO, 
      TESTCASE_ANY_ZERO, 
      TESTCASE_ALL_REAL, 
      TESTCASE_ALL, 
      TESTCASE_UNKNOWN 
    },
    naive_long_vector_cbrt,
    optimized_long_vector_cbrt,
    check_cbrt
  }  
};


#ifdef NO_ASSEMBLY_TIMING

#define READ_TIME_COUNTER(time) do {                   \
  struct timeval t;                                    \
  gettimeofday(&t,NULL);                               \
  *time = (((uint64_t) t.tv_sec) *                     \
	   ((uint64_t) 1000000)) +                     \
    ((uint64_t) t.tv_usec);                            \
} while ( __ALWAYS_FALSE );

#else

#define READ_TIME_COUNTER(time)                        \
  __asm__ __volatile__(                                \
          "xorl %%eax,%%eax\n\t"                       \
          "cpuid\n\t"                                  \
          "rdtsc\n\t"                                  \
          "movl %%eax,(%0)\n\t"                        \
          "movl %%edx,4(%0)\n\t"                       \
          "xorl %%eax,%%eax\n\t"                       \
          "cpuid\n\t"                                  \
          : /* nothing */                              \
          : "S"((time))                                \
          : "eax", "ebx", "ecx", "edx", "memory")

#endif



void optimized_long_vector_exp(double * RESTRICT y, CONST double * RESTRICT x, unsigned int n) {
  unsigned int i;

  for (i=0;i<=(((n/VECTOR_LENGTH)-1)*VECTOR_LENGTH);i+=VECTOR_LENGTH) {
    vector_exp(y + i, x + i);
  }
  for (i=((n/VECTOR_LENGTH)*VECTOR_LENGTH);i<n;i++) {
    y[i] = exp(x[i]);
  }

}

void naive_long_vector_exp(double * RESTRICT y, CONST double * RESTRICT x, unsigned int n) {
  unsigned int i;

  for (i=0;i<n;i++) {
    y[i] = exp(x[i]);
  }
}

void optimized_long_vector_log(double * RESTRICT y, CONST double * RESTRICT x, unsigned int n) {
  unsigned int i;

  for (i=0;i<=(((n/VECTOR_LENGTH)-1)*VECTOR_LENGTH);i+=VECTOR_LENGTH) {
    vector_log(y + i, x + i);
  }
  for (i=((n/VECTOR_LENGTH)*VECTOR_LENGTH);i<n;i++) {
    y[i] = log(x[i]);
  }

}

void naive_long_vector_log(double * RESTRICT y, CONST double * RESTRICT x, unsigned int n) {
  unsigned int i;

  for (i=0;i<n;i++) {
    y[i] = log(x[i]);
  }
}

void optimized_long_vector_sin(double * RESTRICT y, CONST double * RESTRICT x, unsigned int n) {
  unsigned int i;

  for (i=0;i<=(((n/VECTOR_LENGTH)-1)*VECTOR_LENGTH);i+=VECTOR_LENGTH) {
    vector_sin(y + i, x + i);
  }
  for (i=((n/VECTOR_LENGTH)*VECTOR_LENGTH);i<n;i++) {
    y[i] = sin(x[i]);
  }

}

void naive_long_vector_sin(double * RESTRICT y, CONST double * RESTRICT x, unsigned int n) {
  unsigned int i;

  for (i=0;i<n;i++) {
    y[i] = sin(x[i]);
  }
}

void optimized_long_vector_cos(double * RESTRICT y, CONST double * RESTRICT x, unsigned int n) {
  unsigned int i;

  for (i=0;i<=(((n/VECTOR_LENGTH)-1)*VECTOR_LENGTH);i+=VECTOR_LENGTH) {
    vector_cos(y + i, x + i);
  }
  for (i=((n/VECTOR_LENGTH)*VECTOR_LENGTH);i<n;i++) {
    y[i] = cos(x[i]);
  }

}

void naive_long_vector_cos(double * RESTRICT y, CONST double * RESTRICT x, unsigned int n) {
  unsigned int i;

  for (i=0;i<n;i++) {
    y[i] = cos(x[i]);
  }
}

void optimized_long_vector_tan(double * RESTRICT y, CONST double * RESTRICT x, unsigned int n) {
  unsigned int i;

  for (i=0;i<=(((n/VECTOR_LENGTH)-1)*VECTOR_LENGTH);i+=VECTOR_LENGTH) {
    vector_tan(y + i, x + i);
  }
  for (i=((n/VECTOR_LENGTH)*VECTOR_LENGTH);i<n;i++) {
    y[i] = tan(x[i]);
  }

}

void naive_long_vector_tan(double * RESTRICT y, CONST double * RESTRICT x, unsigned int n) {
  unsigned int i;

  for (i=0;i<n;i++) {
    y[i] = tan(x[i]);
  }
}

void optimized_long_vector_asin(double * RESTRICT y, CONST double * RESTRICT x, unsigned int n) {
  unsigned int i;

  for (i=0;i<=(((n/VECTOR_LENGTH)-1)*VECTOR_LENGTH);i+=VECTOR_LENGTH) {
    vector_asin(y + i, x + i);
  }
  for (i=((n/VECTOR_LENGTH)*VECTOR_LENGTH);i<n;i++) {
    y[i] = asin(x[i]);
  }

}

void naive_long_vector_asin(double * RESTRICT y, CONST double * RESTRICT x, unsigned int n) {
  unsigned int i;

  for (i=0;i<n;i++) {
    y[i] = asin(x[i]);
  }
}

void optimized_long_vector_acos(double * RESTRICT y, CONST double * RESTRICT x, unsigned int n) {
  unsigned int i;

  for (i=0;i<=(((n/VECTOR_LENGTH)-1)*VECTOR_LENGTH);i+=VECTOR_LENGTH) {
    vector_acos(y + i, x + i);
  }
  for (i=((n/VECTOR_LENGTH)*VECTOR_LENGTH);i<n;i++) {
    y[i] = acos(x[i]);
  }

}

void naive_long_vector_acos(double * RESTRICT y, CONST double * RESTRICT x, unsigned int n) {
  unsigned int i;

  for (i=0;i<n;i++) {
    y[i] = acos(x[i]);
  }
}

void optimized_long_vector_atan(double * RESTRICT y, CONST double * RESTRICT x, unsigned int n) {
  unsigned int i;

  for (i=0;i<=(((n/VECTOR_LENGTH)-1)*VECTOR_LENGTH);i+=VECTOR_LENGTH) {
    vector_atan(y + i, x + i);
  }
  for (i=((n/VECTOR_LENGTH)*VECTOR_LENGTH);i<n;i++) {
    y[i] = atan(x[i]);
  }

}

void naive_long_vector_atan(double * RESTRICT y, CONST double * RESTRICT x, unsigned int n) {
  unsigned int i;

  for (i=0;i<n;i++) {
    y[i] = atan(x[i]);
  }
}

void optimized_long_vector_cbrt(double * RESTRICT y, CONST double * RESTRICT x, unsigned int n) {
  unsigned int i;

  for (i=0;i<=(((n/VECTOR_LENGTH)-1)*VECTOR_LENGTH);i+=VECTOR_LENGTH) {
    vector_cbrt(y + i, x + i);
  }
  for (i=((n/VECTOR_LENGTH)*VECTOR_LENGTH);i<n;i++) {
    y[i] = cbrt(x[i]);
  }

}

void naive_long_vector_cbrt(double * RESTRICT y, CONST double * RESTRICT x, unsigned int n) {
  unsigned int i;

  for (i=0;i<n;i++) {
    y[i] = cbrt(x[i]);
  }
}


STATIC char *testcase_description(testcase_t testcase) {
  switch (testcase) {
  case TESTCASE_NORMAL_EXP:              return "  normal exp test cases          ";
  case TESTCASE_REGULAR_EXP:             return "* regular exp test cases         ";
  case TESTCASE_NORMAL_LOG:              return "  normal log test cases          ";
  case TESTCASE_REGULAR_LOG:             return "* regular log test cases         ";
  case TESTCASE_SMALL_SINE:              return "  small sin test cases           ";
  case TESTCASE_REASONABLE_SINE:         return "  reasonably large sin test cases";
  case TESTCASE_LARGE_SINE:              return "* large sin test cases           ";
  case TESTCASE_SUBNORMAL_SINE:          return "  subnormal sin test cases       ";
  case TESTCASE_SMALL_COSINE:            return "  small cos test cases           ";
  case TESTCASE_REASONABLE_COSINE:       return "  reasonably large cos test cases";
  case TESTCASE_LARGE_COSINE:            return "* large cos test cases           ";
  case TESTCASE_SUBNORMAL_INPUT_COSINE:  return "  subnormal input cos test cases ";
  case TESTCASE_SMALL_TANGENT:           return "  small tan test cases           ";
  case TESTCASE_REASONABLE_TANGENT:      return "  reasonably large tan test cases";
  case TESTCASE_LARGE_TANGENT:           return "* large tan test cases           ";
  case TESTCASE_SUBNORMAL_TANGENT:       return "  subnormal tan test cases       ";
  case TESTCASE_NORMAL_ASIN:             return "  normal asin test cases         ";
  case TESTCASE_REGULAR_ASIN:            return "  regular asin test cases        ";
  case TESTCASE_SMALL_ASIN:              return "  small asin test cases          ";
  case TESTCASE_REASONABLE_ASIN:         return "* reasonable asin test cases     ";
  case TESTCASE_REGULAR_ACOS:            return "  regular acos test cases        ";
  case TESTCASE_SMALL_ACOS:              return "  small acos test cases          ";
  case TESTCASE_REASONABLE_ACOS:         return "* reasonable acos test cases     ";
  case TESTCASE_NORMAL_ATAN:             return "  normal atan test cases         ";
  case TESTCASE_REGULAR_ATAN:            return "* regular atan test cases        ";
  case TESTCASE_SMALL_ATAN:              return "  small atan test cases          ";
  case TESTCASE_REASONABLE_ATAN:         return "  reasonable atan test cases     ";
  case TESTCASE_NORMAL_CBRT:             return "  normal cbrt test cases         ";
  case TESTCASE_REGULAR_CBRT:            return "  regular cbrt test cases        ";
  case TESTCASE_REASONABLE_CBRT:         return "* reasonable cbrt test cases     ";
  case TESTCASE_QNAN:                    return "  inputs are qNaN                ";
  case TESTCASE_SNAN:                    return "  inputs are sNaN                ";
  case TESTCASE_P_INF:                   return "  inputs are +Inf                ";
  case TESTCASE_N_INF:                   return "  inputs are -Inf                ";
  case TESTCASE_ALL_REAL:                return "  inputs are real numbers        ";
  case TESTCASE_P_REAL:                  return "  inputs are positive numbers    ";
  case TESTCASE_N_REAL:                  return "  inputs are negative numbers    ";
  case TESTCASE_P_ZERO:                  return "  inputs are all positive zero   ";
  case TESTCASE_N_ZERO:                  return "  inputs are all negative zero   ";
  case TESTCASE_ANY_ZERO:                return "  inputs are zeros               ";
  case TESTCASE_ALL:                     return "  arbitrary inputs               ";
  case TESTCASE_UNKNOWN:                 return "  unknown test case              ";
  default:                               return "  unknown test case              ";
  }
}

STATIC char *testfunc_description(testfunc_t testfunc) {
  switch (testfunc) {
  case TESTFUNC_EXP:             return "exponential";
  case TESTFUNC_LOG:             return "logarithm";
  case TESTFUNC_SIN:             return "sine";
  case TESTFUNC_COS:             return "cosine";
  case TESTFUNC_TAN:             return "tangent";
  case TESTFUNC_ASIN:            return "arcsine";
  case TESTFUNC_ACOS:            return "arccosine";
  case TESTFUNC_ATAN:            return "arctangent";
  case TESTFUNC_CBRT:            return "cubic root";
  default:                       return "unknown test function";
  }
}

STATIC char *testfunc_name(testfunc_t testfunc) {
  switch (testfunc) {
  case TESTFUNC_EXP:             return "exp";
  case TESTFUNC_LOG:             return "log";
  case TESTFUNC_SIN:             return "sin";
  case TESTFUNC_COS:             return "cos";
  case TESTFUNC_TAN:             return "tan";
  case TESTFUNC_ASIN:            return "asin";
  case TESTFUNC_ACOS:            return "acos";
  case TESTFUNC_ATAN:            return "atan";
  case TESTFUNC_CBRT:            return "cbrt";
  default:                       return "";
  }
}

STATIC double buildFPNumber(int sign, int expo, uint64_t mant) {
  dblcast xdb;
  double res;
  
  if (expo + 1023 >= 0) {
    xdb.i = (((uint64_t) (expo + 1023)) << (53 - 1)) | 
      ((mant >> (64 - 53) & (~(((uint64_t) 1) << (53 - 1)))));
  } else {
    xdb.i = ((mant >> (64 - 53 - (expo + 1023)) & (~(((uint64_t) 1) << (1023 - 1 + (expo + 1023))))));
  }
  res = (sign ? -xdb.d : xdb.d); 
  
  return res;
}

STATIC uint64_t randomUint64() {
  uint64_t res;
  int i;
  unsigned long int r;

  res = 0;
  for (i=0; i<8; i++) {
    res <<= 8;
    r = rand();
    res |= (uint64_t) (r & 0xff);
  }

  return res;
}

STATIC int randomIntInRange(int min, int max) {
  int res;

  do {
    res = min + ((int) (((double) (max - min + 1)) * (((double) rand()) / ((double) RAND_MAX))));
  } while ((res < min) || (res > max));
  
  return res;
}



STATIC double generate_test_case(testcase_t testcase) {
  double x;
  uint64_t mant;
  int expo, sign;
  dblcast dblcst;

  switch (testcase) {
  case TESTCASE_NORMAL_EXP: 
    do {
      expo = randomIntInRange(-57, 10);
      sign = (rand() <= (RAND_MAX / 2));
      mant = randomUint64();
      x = buildFPNumber(sign, expo, mant);
    } while (!((-708.3 <= x) && (x <= 709.7)));
    break;
  case TESTCASE_REGULAR_EXP: 
    do {
      expo = randomIntInRange(-57, 10);
      sign = (rand() <= (RAND_MAX / 2));
      mant = randomUint64();
      x = buildFPNumber(sign, expo, mant);
    } while (!((-744.4 <= x) && (x <= 709.7)));
    break;
  case TESTCASE_NORMAL_LOG:
    expo = randomIntInRange(-1022, 1023);
    mant = randomUint64();
    x = buildFPNumber(0, expo, mant);
    break;
  case TESTCASE_REGULAR_LOG:
    do {
      expo = randomIntInRange(-1074, 1023);
      mant = randomUint64();
      x = buildFPNumber(0, expo, mant);
    } while (x <= 0.0);
    break;
  case TESTCASE_SMALL_SINE:
    do {
      expo = randomIntInRange(-1022, 3);
      sign = (rand() <= (RAND_MAX / 2));
      mant = randomUint64();
      x = buildFPNumber(sign, expo, mant);
    } while (!((-7.0 <= x) && (x <= 7.0)));
    break;
  case TESTCASE_REASONABLE_SINE:
    do {
      expo = randomIntInRange(-1022, 16);
      sign = (rand() <= (RAND_MAX / 2));
      mant = randomUint64();
      x = buildFPNumber(sign, expo, mant);
    } while (!((-51471.0 <= x) && (x <= 51471.0)));
    break;    
  case TESTCASE_LARGE_SINE:
    do {
      expo = randomIntInRange(15, 1023);
      sign = (rand() <= (RAND_MAX / 2));
      mant = randomUint64();
      x = buildFPNumber(sign, expo, mant);
    } while ((-51471.0 <= x) && (x <= 51471.0));
    break;
  case TESTCASE_SUBNORMAL_SINE:
    expo = randomIntInRange(-1074, -1023);
    mant = randomUint64();
    sign = (rand() <= (RAND_MAX / 2));
    x = buildFPNumber(sign, expo, mant);
    break;
  case TESTCASE_SMALL_COSINE:
    do {
      expo = randomIntInRange(-1022, 3);
      sign = (rand() <= (RAND_MAX / 2));
      mant = randomUint64();
      x = buildFPNumber(sign, expo, mant);
    } while (!((-7.0 <= x) && (x <= 7.0)));
    break;
  case TESTCASE_REASONABLE_COSINE:
    do {
      expo = randomIntInRange(-1022, 16);
      sign = (rand() <= (RAND_MAX / 2));
      mant = randomUint64();
      x = buildFPNumber(sign, expo, mant);
    } while (!((-25735.0 <= x) && (x <= 25735.0)));
    break;    
  case TESTCASE_LARGE_COSINE:
    do {
      expo = randomIntInRange(13, 1023);
      sign = (rand() <= (RAND_MAX / 2));
      mant = randomUint64();
      x = buildFPNumber(sign, expo, mant);
    } while ((-25735.0 <= x) && (x <= 25735.0));
    break;
  case TESTCASE_SUBNORMAL_INPUT_COSINE:
    expo = randomIntInRange(-1074, -1023);
    mant = randomUint64();
    sign = (rand() <= (RAND_MAX / 2));
    x = buildFPNumber(sign, expo, mant);
    break;
  case TESTCASE_SMALL_TANGENT:
    do {
      expo = randomIntInRange(-1022, 3);
      sign = (rand() <= (RAND_MAX / 2));
      mant = randomUint64();
      x = buildFPNumber(sign, expo, mant);
    } while (!((-7.0 <= x) && (x <= 7.0)));
    break;
  case TESTCASE_REASONABLE_TANGENT:
    do {
      expo = randomIntInRange(-1022, 16);
      sign = (rand() <= (RAND_MAX / 2));
      mant = randomUint64();
      x = buildFPNumber(sign, expo, mant);
    } while (!((-25735.0 <= x) && (x <= 25735.0)));
    break;    
  case TESTCASE_LARGE_TANGENT:
    do {
      expo = randomIntInRange(13, 1023);
      sign = (rand() <= (RAND_MAX / 2));
      mant = randomUint64();
      x = buildFPNumber(sign, expo, mant);
    } while ((-25735.0 <= x) && (x <= 25735.0));
    break;
  case TESTCASE_SUBNORMAL_TANGENT:
    expo = randomIntInRange(-1074, -1023);
    mant = randomUint64();
    sign = (rand() <= (RAND_MAX / 2));
    x = buildFPNumber(sign, expo, mant);
    break;
  case TESTCASE_NORMAL_ASIN:
    do {
      expo = randomIntInRange(-1021, 1);
      sign = (rand() <= (RAND_MAX / 2));
      mant = randomUint64();
      x = buildFPNumber(sign, expo, mant);
    } while (!((-1.0 <= x) && (x <= 1.0)));
    break;
  case TESTCASE_REGULAR_ASIN:
    do {
      expo = randomIntInRange(-1074, 1);
      sign = (rand() <= (RAND_MAX / 2));
      mant = randomUint64();
      x = buildFPNumber(sign, expo, mant);
    } while (!((-1.0 <= x) && (x <= 1.0)));
    break;
  case TESTCASE_SMALL_ASIN:
    do {
      expo = randomIntInRange(-1021, -6);
      sign = (rand() <= (RAND_MAX / 2));
      mant = randomUint64();
      x = buildFPNumber(sign, expo, mant);
    } while (!((-0.01 <= x) && (x <= 0.01)));
    break;
  case TESTCASE_REASONABLE_ASIN:
    do {
      expo = randomIntInRange(-27, 1);
      sign = (rand() <= (RAND_MAX / 2));
      mant = randomUint64();
      x = buildFPNumber(sign, expo, mant);
    } while (!((-1.0 <= x) && (x <= 1.0)));
    break;
  case TESTCASE_REGULAR_ACOS:
    do {
      expo = randomIntInRange(-1074, 1);
      sign = (rand() <= (RAND_MAX / 2));
      mant = randomUint64();
      x = buildFPNumber(sign, expo, mant);
    } while (!((-1.0 <= x) && (x <= 1.0)));
    break;
  case TESTCASE_SMALL_ACOS:
    do {
      expo = randomIntInRange(-1021, -6);
      mant = randomUint64();
      x = buildFPNumber(1, expo, mant);
    } while (!((-0.01 <= x) && (x <= 0.0)));
    x += 1.0;
    break;
  case TESTCASE_REASONABLE_ACOS:
    do {
      expo = randomIntInRange(-55, 1);
      sign = (rand() <= (RAND_MAX / 2));
      mant = randomUint64();
      x = buildFPNumber(sign, expo, mant);
    } while (!((-1.0 <= x) && (x <= 1.0)));
    break;
  case TESTCASE_NORMAL_ATAN:
    expo = randomIntInRange(-1021, 1023);
    mant = randomUint64();
    sign = (rand() <= (RAND_MAX / 2));
    x = buildFPNumber(sign, expo, mant);
    break;
  case TESTCASE_REGULAR_ATAN:
    expo = randomIntInRange(-1074, 1023);
    mant = randomUint64();
    sign = (rand() <= (RAND_MAX / 2));
    x = buildFPNumber(sign, expo, mant);
    break;
  case TESTCASE_SMALL_ATAN:
    do {
      expo = randomIntInRange(-1021, -6);
      sign = (rand() <= (RAND_MAX / 2));
      mant = randomUint64();
      x = buildFPNumber(sign, expo, mant);
    } while (!((-0.01 <= x) && (x <= 0.01)));
    break;
  case TESTCASE_REASONABLE_ATAN:
    do {
      expo = randomIntInRange(-27, 5);
      sign = (rand() <= (RAND_MAX / 2));
      mant = randomUint64();
      x = buildFPNumber(sign, expo, mant);
    } while (!((-16.0 <= x) && (x <= 16.0)));
    break;
  case TESTCASE_NORMAL_CBRT:
    expo = randomIntInRange(-1021, 1023);
    mant = randomUint64();
    sign = (rand() <= (RAND_MAX / 2));
    x = buildFPNumber(sign, expo, mant);
    break;
  case TESTCASE_REGULAR_CBRT:
    expo = randomIntInRange(-1074, 1023);
    sign = (rand() <= (RAND_MAX / 2));
    mant = randomUint64();
    x = buildFPNumber(sign, expo, mant);
    break;
  case TESTCASE_REASONABLE_CBRT:
    expo = randomIntInRange(-27, 27);
    sign = (rand() <= (RAND_MAX / 2));
    mant = randomUint64();
    x = buildFPNumber(sign, expo, mant);
    break;
  case TESTCASE_QNAN:
    do {
      dblcst.i = randomUint64() | 0x7ff8000000000000ull;
    } while ((dblcst.i & 0x7fffffffffffffffull) == 0x7ff0000000000000ull);
    x = dblcst.d;
    break;
  case TESTCASE_SNAN:
    do {
      dblcst.i = (randomUint64() | 0x7ff0000000000000ull) & 0xfff7ffffffffffffull;
    } while ((dblcst.i & 0x7fffffffffffffffull) == 0x7ff0000000000000ull);
    x = dblcst.d;
    break;
  case TESTCASE_P_INF:
    dblcst.i = 0x7ff0000000000000ull;
    x = dblcst.d;    
    break;
  case TESTCASE_N_INF:
    dblcst.i = 0xfff0000000000000ull;
    x = dblcst.d;    
    break;
  case TESTCASE_ALL_REAL:
    expo = randomIntInRange(-1074, 1023);
    sign = (rand() <= (RAND_MAX / 2));
    mant = randomUint64();
    x = buildFPNumber(sign, expo, mant);
    break;
  case TESTCASE_P_REAL:
    do {
      expo = randomIntInRange(-1074, 1023);
      mant = randomUint64();
      x = buildFPNumber(0, expo, mant);
    } while (x <= 0.0); 
    break;    
  case TESTCASE_N_REAL:
    do {
      expo = randomIntInRange(-1074, 1023);
      mant = randomUint64();
      x = buildFPNumber(1, expo, mant);
    } while (x >= 0.0); 
    break;    
  case TESTCASE_P_ZERO:
    x = 0.0;    /* +/- 0.0 */
    x = x * x;  /* + 0.0 */
    break;
  case TESTCASE_N_ZERO:
    x = 0.0;    /* +/- 0.0 */
    x = x * x;  /* + 0.0 */
    x = -x;     /* - 0.0 */
    break;
  case TESTCASE_ANY_ZERO:
    x = 0.0;    /* +/- 0.0 */
    x = x * x;  /* + 0.0 */
    sign = (rand() <= (RAND_MAX / 2));
    if (sign) {
      x = -x;   /* Flip sign */
    }
    break;
  case TESTCASE_ALL:
  case TESTCASE_UNKNOWN:
    dblcst.i = randomUint64();
    x = dblcst.d;
    break;
  default:
    x = 0.0;
    break;
  }  

  return x;
}

STATIC void generate_test_vector(double * RESTRICT x, unsigned int n, testcase_t testcase) {
  unsigned int i;

  for (i=0;i<n;i++) {
    x[i] = generate_test_case(testcase);
  }
}

STATIC int isNumber(double a) {
  dblcast acst;

  acst.d = a;
  return ((acst.i & 0x7fffffffffffffffull) < 0x7ff0000000000000ull);
}

STATIC int isZero(double a) {
  dblcast acst;

  acst.d = a;
  return ((acst.i & 0x7fffffffffffffffull) == 0x0000000000000000ull);
}

STATIC int isInfinity(double a) {
  dblcast acst;
  
  acst.d = a;
  return ((acst.i & 0x7fffffffffffffffull) == 0x7ff0000000000000ull);
}

STATIC int isNan(double a) {
  dblcast acst;
  
  acst.d = a;
  return ((acst.i & 0x7fffffffffffffffull) > 0x7ff0000000000000ull);
}

STATIC int isNormal(double a) {
  dblcast acst;
  
  acst.d = a;
  acst.i &= 0x7fffffffffffffffull;
  
  return ((acst.i < 0x7ff0000000000000ull) && (acst.i >= 0x0010000000000000ull));
}

STATIC int isSubnormal(double a) {
  dblcast acst;
  
  acst.d = a;
  return ((acst.i & 0x7fffffffffffffffull) < 0x0010000000000000ull);
}

STATIC int isSignPositive(double a) {
  dblcast acst;

  acst.d = a;
  return ((acst.i & 0x8000000000000000ull) == 0ull);
}

STATIC double reference_exp(double x) {
  double y;
  int ternaryA;
  mpfr_exp_t oldEmin, oldEmax;
  mpfr_prec_t oldDefaultPrec;
  mpfr_t xMpfr, temp;
  
  if (!isNumber(x)) {
    if (isInfinity(x)) {
      if (isSignPositive(x)) {
	return x; /* exp(+inf) = +inf */
      }
      return 0.0; /* exp(-inf) = -inf */

    }
    /* Here, x is a NaN */
    return x + 1.0; /* exp(q/sNaN) = qNaN */
  } 

  /* Here, x is a real number */

  mpfr_init2(xMpfr, 64);
  mpfr_set_d(xMpfr, x, GMP_RNDN);
  oldEmin = mpfr_get_emin();
  oldEmax = mpfr_get_emax();
  oldDefaultPrec = mpfr_get_default_prec();
  mpfr_set_default_prec(53);
  mpfr_set_emin(-1073); mpfr_set_emax(1024);
  mpfr_init(temp);
  ternaryA = mpfr_exp(temp, xMpfr, GMP_RNDN);
  mpfr_subnormalize(temp, ternaryA, GMP_RNDN);
  y = mpfr_get_d(temp, GMP_RNDN); /* exact */
  mpfr_clear(temp);
  mpfr_set_emin(oldEmin);
  mpfr_set_emax(oldEmax);
  mpfr_set_default_prec(oldDefaultPrec);
  mpfr_clear(xMpfr);

  return y;
}

STATIC double reference_log(double x) {
  double y;
  int ternaryA;
  mpfr_exp_t oldEmin, oldEmax;
  mpfr_prec_t oldDefaultPrec;
  mpfr_t xMpfr, temp;
  volatile double volTemp1, volTemp2;
  
  if (!isNumber(x)) {
    /* Handle special cases 

       NaN  -> NaN
       +inf -> +inf
       -inf -> NaN, raising invalid

    */
    if (isInfinity(x)) {
      if (isSignPositive(x)) {
	return x; /* log(+inf) = +inf */
      }
      /* log(-inf) = NaN, raising invalid */
      volTemp1 = 0.0;
      volTemp2 = volTemp1;
      volTemp1 /= volTemp2; /* NaN, raising invalid */
      return volTemp1;
    }
    /* Here, x is a NaN */
    return x + 1.0; /* log(q/sNaN) = qNaN */
  } 

  if (isZero(x)) {
    /* Return -infinity and raise division-by-zero */
    volTemp1 = -1.0;
    volTemp2 = 0.0;
    volTemp2 = volTemp2 * volTemp2; /* +0.0 */
    volTemp1 /= volTemp2; /* -inf, raising division-by-zero */
    return volTemp1;
  }

  /* Here, x is a non-zero real number */
  if (x < 0.0) {
    /* Return NaN and raise invalid */
    volTemp1 = 0.0;
    volTemp2 = volTemp1;
    volTemp1 /= volTemp2; /* NaN, raising invalid */
    return volTemp1;
  }

  /* Here, x is a positive real number 

     Handle exact case log(1.0) = +0.0 by hand.

  */
  if (x == 1.0) {
    y = 0.0;   /* +/- 0.0 */
    y = y * y; /* +0.0 */
    return y;
  }
  mpfr_init2(xMpfr, 64);
  mpfr_set_d(xMpfr, x, GMP_RNDN);
  oldEmin = mpfr_get_emin();
  oldEmax = mpfr_get_emax();
  oldDefaultPrec = mpfr_get_default_prec();
  mpfr_set_default_prec(53);
  mpfr_set_emin(-1073); mpfr_set_emax(1024);
  mpfr_init(temp);
  ternaryA = mpfr_log(temp, xMpfr, GMP_RNDN);
  mpfr_subnormalize(temp, ternaryA, GMP_RNDN);
  y = mpfr_get_d(temp, GMP_RNDN); /* exact */
  mpfr_clear(temp);
  mpfr_set_emin(oldEmin);
  mpfr_set_emax(oldEmax);
  mpfr_set_default_prec(oldDefaultPrec);
  mpfr_clear(xMpfr);

  return y;
}

STATIC double reference_sin(double x) {
  double y;
  int ternaryA;
  mpfr_exp_t oldEmin, oldEmax;
  mpfr_prec_t oldDefaultPrec;
  mpfr_t xMpfr, temp;
  volatile double volTemp1, volTemp2;
  
  if (!isNumber(x)) {
    /* Handle special cases 

       NaN  -> NaN
       +/-inf -> NaN, raising invalid

    */
    if (isInfinity(x)) {
      /* sin(+/-inf) = NaN, raising invalid */
      volTemp1 = 0.0;
      volTemp2 = volTemp1;
      volTemp1 /= volTemp2; /* NaN, raising invalid */
      return volTemp1;
    }
    /* Here, x is a NaN */
    return x + 1.0; /* sin(q/sNaN) = qNaN */
  } 

  if (isZero(x)) {
    /* x is zero, return zero of the same sign */
    return x;
  }

  /* Here, x is a non-zero real number */
  mpfr_init2(xMpfr, 64);
  mpfr_set_d(xMpfr, x, GMP_RNDN);
  oldEmin = mpfr_get_emin();
  oldEmax = mpfr_get_emax();
  oldDefaultPrec = mpfr_get_default_prec();
  mpfr_set_default_prec(53);
  mpfr_set_emin(-1073); mpfr_set_emax(1024);
  mpfr_init(temp);
  ternaryA = mpfr_sin(temp, xMpfr, GMP_RNDN);
  mpfr_subnormalize(temp, ternaryA, GMP_RNDN);
  y = mpfr_get_d(temp, GMP_RNDN); /* exact */
  mpfr_clear(temp);
  mpfr_set_emin(oldEmin);
  mpfr_set_emax(oldEmax);
  mpfr_set_default_prec(oldDefaultPrec);
  mpfr_clear(xMpfr);

  return y;
}

STATIC double reference_cos(double x) {
  double y;
  int ternaryA;
  mpfr_exp_t oldEmin, oldEmax;
  mpfr_prec_t oldDefaultPrec;
  mpfr_t xMpfr, temp;
  volatile double volTemp1, volTemp2;
  
  if (!isNumber(x)) {
    /* Handle special cases 

       NaN  -> NaN
       +/-inf -> NaN, raising invalid

    */
    if (isInfinity(x)) {
      /* sin(+/-inf) = NaN, raising invalid */
      volTemp1 = 0.0;
      volTemp2 = volTemp1;
      volTemp1 /= volTemp2; /* NaN, raising invalid */
      return volTemp1;
    }
    /* Here, x is a NaN */
    return x + 1.0; /* sin(q/sNaN) = qNaN */
  } 

  if (isZero(x)) {
    /* x is zero, return 1.0 */
    return 1.0;
  }

  /* Here, x is a non-zero real number */
  mpfr_init2(xMpfr, 64);
  mpfr_set_d(xMpfr, x, GMP_RNDN);
  oldEmin = mpfr_get_emin();
  oldEmax = mpfr_get_emax();
  oldDefaultPrec = mpfr_get_default_prec();
  mpfr_set_default_prec(53);
  mpfr_set_emin(-1073); mpfr_set_emax(1024);
  mpfr_init(temp);
  ternaryA = mpfr_cos(temp, xMpfr, GMP_RNDN);
  mpfr_subnormalize(temp, ternaryA, GMP_RNDN);
  y = mpfr_get_d(temp, GMP_RNDN); /* exact */
  mpfr_clear(temp);
  mpfr_set_emin(oldEmin);
  mpfr_set_emax(oldEmax);
  mpfr_set_default_prec(oldDefaultPrec);
  mpfr_clear(xMpfr);

  return y;
}

STATIC double reference_tan(double x) {
  double y;
  int ternaryA;
  mpfr_exp_t oldEmin, oldEmax;
  mpfr_prec_t oldDefaultPrec;
  mpfr_t xMpfr, temp;
  volatile double volTemp1, volTemp2;
  
  if (!isNumber(x)) {
    /* Handle special cases 

       NaN  -> NaN
       +/-inf -> NaN, raising invalid

    */
    if (isInfinity(x)) {
      /* tan(+/-inf) = NaN, raising invalid */
      volTemp1 = 0.0;
      volTemp2 = volTemp1;
      volTemp1 /= volTemp2; /* NaN, raising invalid */
      return volTemp1;
    }
    /* Here, x is a NaN */
    return x + 1.0; /* tan(q/sNaN) = qNaN */
  } 

  if (isZero(x)) {
    /* x is zero, return zero of the same sign */
    return x;
  }

  /* Here, x is a non-zero real number */
  mpfr_init2(xMpfr, 64);
  mpfr_set_d(xMpfr, x, GMP_RNDN);
  oldEmin = mpfr_get_emin();
  oldEmax = mpfr_get_emax();
  oldDefaultPrec = mpfr_get_default_prec();
  mpfr_set_default_prec(53);
  mpfr_set_emin(-1073); mpfr_set_emax(1024);
  mpfr_init(temp);
  ternaryA = mpfr_tan(temp, xMpfr, GMP_RNDN);
  mpfr_subnormalize(temp, ternaryA, GMP_RNDN);
  y = mpfr_get_d(temp, GMP_RNDN); /* exact */
  mpfr_clear(temp);
  mpfr_set_emin(oldEmin);
  mpfr_set_emax(oldEmax);
  mpfr_set_default_prec(oldDefaultPrec);
  mpfr_clear(xMpfr);

  return y;
}

STATIC double reference_asin(double x) {
  double y;
  int ternaryA;
  mpfr_exp_t oldEmin, oldEmax;
  mpfr_prec_t oldDefaultPrec;
  mpfr_t xMpfr, temp;
  volatile double volTemp1, volTemp2;
  
  if (!isNumber(x)) {
    /* Handle special cases 

       NaN  -> NaN
       +/-inf -> NaN, raising invalid

    */
    if (isInfinity(x)) {
      /* asin(+/-inf) = NaN, raising invalid */
      volTemp1 = 0.0;
      volTemp2 = volTemp1;
      volTemp1 /= volTemp2; /* NaN, raising invalid */
      return volTemp1;
    }
    /* Here, x is a NaN */
    return x + 1.0; /* asin(q/sNaN) = qNaN */
  } 

  if (isZero(x)) {
    /* x is zero, return zero of the same sign */
    return x;
  }

  /* Here, x is a non-zero real number 

     Check if x is greater than 1.0 or less than -1.0.
     In this case, return NaN, raising invalid.
  
  */
  if ((x < -1.0) || (1.0 < x)) {
      /* abs(x) > 1.0: asin(x) = NaN, raising invalid */
      volTemp1 = 0.0;
      volTemp2 = volTemp1;
      volTemp1 /= volTemp2; /* NaN, raising invalid */
      return volTemp1;
  }

  /* Here, x is a non-zero real number such that abs(x) <= 1.0 */
  mpfr_init2(xMpfr, 64);
  mpfr_set_d(xMpfr, x, GMP_RNDN);
  oldEmin = mpfr_get_emin();
  oldEmax = mpfr_get_emax();
  oldDefaultPrec = mpfr_get_default_prec();
  mpfr_set_default_prec(53);
  mpfr_set_emin(-1073); mpfr_set_emax(1024);
  mpfr_init(temp);
  ternaryA = mpfr_asin(temp, xMpfr, GMP_RNDN);
  mpfr_subnormalize(temp, ternaryA, GMP_RNDN);
  y = mpfr_get_d(temp, GMP_RNDN); /* exact */
  mpfr_clear(temp);
  mpfr_set_emin(oldEmin);
  mpfr_set_emax(oldEmax);
  mpfr_set_default_prec(oldDefaultPrec);
  mpfr_clear(xMpfr);

  return y;
}

STATIC double reference_acos(double x) {
  double y;
  int ternaryA;
  mpfr_exp_t oldEmin, oldEmax;
  mpfr_prec_t oldDefaultPrec;
  mpfr_t xMpfr, temp;
  volatile double volTemp1, volTemp2;
  
  if (!isNumber(x)) {
    /* Handle special cases 

       NaN  -> NaN
       +/-inf -> NaN, raising invalid

    */
    if (isInfinity(x)) {
      /* acos(+/-inf) = NaN, raising invalid */
      volTemp1 = 0.0;
      volTemp2 = volTemp1;
      volTemp1 /= volTemp2; /* NaN, raising invalid */
      return volTemp1;
    }
    /* Here, x is a NaN */
    return x + 1.0; /* acos(q/sNaN) = qNaN */
  } 

  /* Here, x is a real number 

     Check if x is greater than 1.0 or less than -1.0.
     In this case, return NaN, raising invalid.
  
  */
  if ((x < -1.0) || (1.0 < x)) {
      /* abs(x) > 1.0: acos(x) = NaN, raising invalid */
      volTemp1 = 0.0;
      volTemp2 = volTemp1;
      volTemp1 /= volTemp2; /* NaN, raising invalid */
      return volTemp1;
  }

  /* Here, x is a non-zero real number such that abs(x) <= 1.0 */
  mpfr_init2(xMpfr, 64);
  mpfr_set_d(xMpfr, x, GMP_RNDN);
  oldEmin = mpfr_get_emin();
  oldEmax = mpfr_get_emax();
  oldDefaultPrec = mpfr_get_default_prec();
  mpfr_set_default_prec(53);
  mpfr_set_emin(-1073); mpfr_set_emax(1024);
  mpfr_init(temp);
  ternaryA = mpfr_acos(temp, xMpfr, GMP_RNDN);
  mpfr_subnormalize(temp, ternaryA, GMP_RNDN);
  y = mpfr_get_d(temp, GMP_RNDN); /* exact */
  mpfr_clear(temp);
  mpfr_set_emin(oldEmin);
  mpfr_set_emax(oldEmax);
  mpfr_set_default_prec(oldDefaultPrec);
  mpfr_clear(xMpfr);

  return y;
}

STATIC double reference_atan(double x) {
  double y;
  int ternaryA;
  mpfr_exp_t oldEmin, oldEmax;
  mpfr_prec_t oldDefaultPrec;
  mpfr_t xMpfr, temp;
  volatile double volTemp1, volTemp2;
  
  if (!isNumber(x)) {
    /* Handle special cases 

       NaN  -> NaN
       +/-inf -> pi/2, raising inexact

    */
    if (isInfinity(x)) {
      /* atan(+/-inf) = +/-pi/2, raising inexact */
      volTemp1 = 1.5707963267948965579989817342720925807952880859375;
      volTemp2 = 8.67361737988403547205962240695953369140625e-19;
      volTemp1 += volTemp2; /* pi/2, raising invalid */
      if (!isSignPositive(x)) {
	volTemp1 = -volTemp1;
      }
      return volTemp1;
    }
    /* Here, x is a NaN */
    return x + 1.0; /* atan(q/sNaN) = qNaN */
  } 

  if (isZero(x)) {
    /* x is zero, return zero of the same sign */
    return x;
  }

  /* Here, x is a non-zero real number */
  mpfr_init2(xMpfr, 64);
  mpfr_set_d(xMpfr, x, GMP_RNDN);
  oldEmin = mpfr_get_emin();
  oldEmax = mpfr_get_emax();
  oldDefaultPrec = mpfr_get_default_prec();
  mpfr_set_default_prec(53);
  mpfr_set_emin(-1073); mpfr_set_emax(1024);
  mpfr_init(temp);
  ternaryA = mpfr_atan(temp, xMpfr, GMP_RNDN);
  mpfr_subnormalize(temp, ternaryA, GMP_RNDN);
  y = mpfr_get_d(temp, GMP_RNDN); /* exact */
  mpfr_clear(temp);
  mpfr_set_emin(oldEmin);
  mpfr_set_emax(oldEmax);
  mpfr_set_default_prec(oldDefaultPrec);
  mpfr_clear(xMpfr);

  return y;
}

STATIC double reference_cbrt(double x) {
  double y;
  int ternaryA;
  mpfr_exp_t oldEmin, oldEmax;
  mpfr_prec_t oldDefaultPrec;
  mpfr_t xMpfr, temp;
  
  if (!isNumber(x)) {
    /* Handle special cases 

       NaN  -> NaN
       +/-inf -> +/-inf

    */
    if (isInfinity(x)) {
      /* cbrt(+/-inf) = +/-inf*/
      return x;
    }
    /* Here, x is a NaN */
    return x + 1.0; /* cbrt(q/sNaN) = qNaN */
  } 

  /* Here, x is a real number 

     Check if x is zero.
     In this case, return x.
  
  */
  if (x == 0.0) {
    return x;
  }

  /* Here, x is a non-zero real number such that abs(x) <= 1.0 */
  mpfr_init2(xMpfr, 64);
  mpfr_set_d(xMpfr, x, GMP_RNDN);
  oldEmin = mpfr_get_emin();
  oldEmax = mpfr_get_emax();
  oldDefaultPrec = mpfr_get_default_prec();
  mpfr_set_default_prec(53);
  mpfr_set_emin(-1073); mpfr_set_emax(1024);
  mpfr_init(temp);
  ternaryA = mpfr_cbrt(temp, xMpfr, GMP_RNDN);
  mpfr_subnormalize(temp, ternaryA, GMP_RNDN);
  y = mpfr_get_d(temp, GMP_RNDN); /* exact */
  mpfr_clear(temp);
  mpfr_set_emin(oldEmin);
  mpfr_set_emax(oldEmax);
  mpfr_set_default_prec(oldDefaultPrec);
  mpfr_clear(xMpfr);

  return y;
}


STATIC int doubleIsEqualOrSameNonRealClass(double a, double b) {
  dblcast acst, bcst;

  acst.d = a;
  bcst.d = b;

  if (acst.i == bcst.i) return 1;

  if (((acst.i & 0x7fffffffffffffffull) > 0x7ff0000000000000ull) &&
      ((bcst.i & 0x7fffffffffffffffull) > 0x7ff0000000000000ull) && 
      ((acst.i & 0x0008000000000000ull) == (bcst.i & 0x0008000000000000ull))) 
    return 1;

  return 0;
}

STATIC void displayWrongTestCase(double x, double y, double refY, double ulpErr) {
  dblcast xdb, ydb, refydb;

  xdb.d = x;
  ydb.d = y;
  refydb.d = refY;
  fprintf(stderr, 
	  "Wrong test case: x = 0x%016" PRIx64 ", y = 0x%016" PRIx64 ", reference y = 0x%016" PRIx64 ", ulp err = %1.9e\n", 
	  xdb.i, ydb.i, refydb.i, ulpErr);
}

void check_exp(double * RESTRICT ulpErr, int *okay, double y, double x, double maxAllowedUlpErr, int display) {
  double refY, ulp;
  mpfr_exp_t oldEmin, oldEmax;
  mpfr_prec_t oldDefaultPrec;
  int ternaryA;
  mpfr_t xMpfr, yMpfr, deltaMpfr, temp, ulpMpfr, ulpErrMpfr;
  dblcast yDblRZ, yDblRZPlus;

  /* Get a reference result, we are a priori okay if we produce the
     same value.
  */
  refY = reference_exp(x);
  *okay = doubleIsEqualOrSameNonRealClass(y, refY);

  /* If one of x, y or the reference y is not a real number, 
     we cannot say anything more.

     We set the ulp error to zero, even if this does not really
     convey any information.
  */
  if (!(isNumber(x) && isNumber(y) && isNumber(refY))) {
    *ulpErr = 0.0;
    if ((!*okay) && display) displayWrongTestCase(x,y,refY,*ulpErr);
    return;
  }

  /* Here, x, y and the reference y are all real numbers. 

     We get a 165bit accurate approximation to exp and we
     compute the double precision ulp of the binade we are in.

  */
  mpfr_init2(xMpfr, 64);
  mpfr_init2(yMpfr, 165);
  mpfr_init2(deltaMpfr, 165);
  mpfr_init2(ulpMpfr, 64);
  mpfr_init2(ulpErrMpfr, 64);
  mpfr_set_d(xMpfr, x, GMP_RNDN);    /* exact */
  mpfr_exp(yMpfr, xMpfr, GMP_RNDN);  
  mpfr_sub_d(deltaMpfr, yMpfr, y, GMP_RNDN); 
  mpfr_abs(deltaMpfr, deltaMpfr, GMP_RNDN); /* exact */
  
  oldEmin = mpfr_get_emin();
  oldEmax = mpfr_get_emax();
  oldDefaultPrec = mpfr_get_default_prec();
  mpfr_set_default_prec(53);
  mpfr_set_emin(-1073); mpfr_set_emax(1024);
  mpfr_init(temp);
  ternaryA = mpfr_exp(temp, xMpfr, GMP_RNDZ);
  mpfr_subnormalize(temp, ternaryA, GMP_RNDZ);
  yDblRZ.d = mpfr_get_d(temp, GMP_RNDN); /* exact */
  mpfr_clear(temp);
  mpfr_set_emin(oldEmin);
  mpfr_set_emax(oldEmax);
  mpfr_set_default_prec(oldDefaultPrec);

  yDblRZPlus.i = yDblRZ.i + 1;
  ulp = fabs(yDblRZPlus.d - yDblRZ.d);
  mpfr_set_d(ulpMpfr, ulp, GMP_RNDN); /* exact */
  
  mpfr_div(ulpErrMpfr, deltaMpfr, ulpMpfr, GMP_RNDN);
  *ulpErr = mpfr_get_d(ulpErrMpfr, GMP_RNDU);

  mpfr_clear(ulpErrMpfr);
  mpfr_clear(ulpMpfr);
  mpfr_clear(deltaMpfr);
  mpfr_clear(yMpfr);
  mpfr_clear(xMpfr);

  /* Set "okay" result to yes if the ulp error is less than or equal to 
     the maximally allowed ulp error.
  */
  if (fabs(*ulpErr) <= fabs(maxAllowedUlpErr)) *okay = 1;

  if ((!*okay) && display) displayWrongTestCase(x,y,refY,*ulpErr);
}

void check_log(double * RESTRICT ulpErr, int *okay, double y, double x, double maxAllowedUlpErr, int display) {
  double refY, ulp;
  mpfr_exp_t oldEmin, oldEmax;
  mpfr_prec_t oldDefaultPrec;
  int ternaryA;
  mpfr_t xMpfr, yMpfr, deltaMpfr, temp, ulpMpfr, ulpErrMpfr;
  dblcast yDblRZ, yDblRZPlus;

  /* Get a reference result, we are a priori okay if we produce the
     same value.
  */
  refY = reference_log(x);
  *okay = doubleIsEqualOrSameNonRealClass(y, refY);

  /* If one of x, y or the reference y is not a real number, 
     we cannot say anything more.

     We set the ulp error to zero, even if this does not really
     convey any information.
  */
  if (!(isNumber(x) && isNumber(y) && isNumber(refY))) {
    *ulpErr = 0.0;
    if ((!*okay) && display) displayWrongTestCase(x,y,refY,*ulpErr);
    return;
  }

  /* Here, x, y and the reference y are all real numbers. 

     We get a 165bit accurate approximation to log and we
     compute the double precision ulp of the binade we are in.

  */
  mpfr_init2(xMpfr, 64);
  mpfr_init2(yMpfr, 165);
  mpfr_init2(deltaMpfr, 165);
  mpfr_init2(ulpMpfr, 64);
  mpfr_init2(ulpErrMpfr, 64);
  mpfr_set_d(xMpfr, x, GMP_RNDN);    /* exact */
  mpfr_log(yMpfr, xMpfr, GMP_RNDN);  
  mpfr_sub_d(deltaMpfr, yMpfr, y, GMP_RNDN); 
  mpfr_abs(deltaMpfr, deltaMpfr, GMP_RNDN); /* exact */
  
  oldEmin = mpfr_get_emin();
  oldEmax = mpfr_get_emax();
  oldDefaultPrec = mpfr_get_default_prec();
  mpfr_set_default_prec(53);
  mpfr_set_emin(-1073); mpfr_set_emax(1024);
  mpfr_init(temp);
  ternaryA = mpfr_log(temp, xMpfr, GMP_RNDZ);
  mpfr_subnormalize(temp, ternaryA, GMP_RNDZ);
  yDblRZ.d = mpfr_get_d(temp, GMP_RNDN); /* exact */
  mpfr_clear(temp);
  mpfr_set_emin(oldEmin);
  mpfr_set_emax(oldEmax);
  mpfr_set_default_prec(oldDefaultPrec);

  yDblRZPlus.i = yDblRZ.i + 1;
  ulp = fabs(yDblRZPlus.d - yDblRZ.d);
  mpfr_set_d(ulpMpfr, ulp, GMP_RNDN); /* exact */
  
  mpfr_div(ulpErrMpfr, deltaMpfr, ulpMpfr, GMP_RNDN);
  *ulpErr = mpfr_get_d(ulpErrMpfr, GMP_RNDU);

  mpfr_clear(ulpErrMpfr);
  mpfr_clear(ulpMpfr);
  mpfr_clear(deltaMpfr);
  mpfr_clear(yMpfr);
  mpfr_clear(xMpfr);

  /* Set "okay" result to yes if the ulp error is less than or equal to 
     the maximally allowed ulp error.
  */
  if (fabs(*ulpErr) <= fabs(maxAllowedUlpErr)) *okay = 1;

  if ((!*okay) && display) displayWrongTestCase(x,y,refY,*ulpErr);
}

void check_sin(double * RESTRICT ulpErr, int *okay, double y, double x, double maxAllowedUlpErr, int display) {
  double refY, ulp;
  mpfr_exp_t oldEmin, oldEmax;
  mpfr_prec_t oldDefaultPrec;
  int ternaryA;
  mpfr_t xMpfr, yMpfr, deltaMpfr, temp, ulpMpfr, ulpErrMpfr;
  dblcast yDblRZ, yDblRZPlus;

  /* Get a reference result, we are a priori okay if we produce the
     same value.
  */
  refY = reference_sin(x);
  *okay = doubleIsEqualOrSameNonRealClass(y, refY);

  /* If one of x, y or the reference y is not a real number, 
     we cannot say anything more.

     We set the ulp error to zero, even if this does not really
     convey any information.
  */
  if (!(isNumber(x) && isNumber(y) && isNumber(refY))) {
    *ulpErr = 0.0;
    if ((!*okay) && display) displayWrongTestCase(x,y,refY,*ulpErr);
    return;
  }

  /* Here, x, y and the reference y are all real numbers. 

     We get a 165bit accurate approximation to sin and we
     compute the double precision ulp of the binade we are in.

  */
  mpfr_init2(xMpfr, 64);
  mpfr_init2(yMpfr, 165);
  mpfr_init2(deltaMpfr, 165);
  mpfr_init2(ulpMpfr, 64);
  mpfr_init2(ulpErrMpfr, 64);
  mpfr_set_d(xMpfr, x, GMP_RNDN);    /* exact */
  mpfr_sin(yMpfr, xMpfr, GMP_RNDN);  
  mpfr_sub_d(deltaMpfr, yMpfr, y, GMP_RNDN); 
  mpfr_abs(deltaMpfr, deltaMpfr, GMP_RNDN); /* exact */
  
  oldEmin = mpfr_get_emin();
  oldEmax = mpfr_get_emax();
  oldDefaultPrec = mpfr_get_default_prec();
  mpfr_set_default_prec(53);
  mpfr_set_emin(-1073); mpfr_set_emax(1024);
  mpfr_init(temp);
  ternaryA = mpfr_sin(temp, xMpfr, GMP_RNDZ);
  mpfr_subnormalize(temp, ternaryA, GMP_RNDZ);
  yDblRZ.d = mpfr_get_d(temp, GMP_RNDN); /* exact */
  mpfr_clear(temp);
  mpfr_set_emin(oldEmin);
  mpfr_set_emax(oldEmax);
  mpfr_set_default_prec(oldDefaultPrec);

  yDblRZPlus.i = yDblRZ.i + 1;
  ulp = fabs(yDblRZPlus.d - yDblRZ.d);
  mpfr_set_d(ulpMpfr, ulp, GMP_RNDN); /* exact */
  
  mpfr_div(ulpErrMpfr, deltaMpfr, ulpMpfr, GMP_RNDN);
  *ulpErr = mpfr_get_d(ulpErrMpfr, GMP_RNDU);

  mpfr_clear(ulpErrMpfr);
  mpfr_clear(ulpMpfr);
  mpfr_clear(deltaMpfr);
  mpfr_clear(yMpfr);
  mpfr_clear(xMpfr);

  /* Set "okay" result to yes if the ulp error is less than or equal to 
     the maximally allowed ulp error.
  */
  if (fabs(*ulpErr) <= fabs(maxAllowedUlpErr)) *okay = 1;

  if ((!*okay) && display) displayWrongTestCase(x,y,refY,*ulpErr);
}

void check_cos(double * RESTRICT ulpErr, int *okay, double y, double x, double maxAllowedUlpErr, int display) {
  double refY, ulp;
  mpfr_exp_t oldEmin, oldEmax;
  mpfr_prec_t oldDefaultPrec;
  int ternaryA;
  mpfr_t xMpfr, yMpfr, deltaMpfr, temp, ulpMpfr, ulpErrMpfr;
  dblcast yDblRZ, yDblRZPlus;

  /* Get a reference result, we are a priori okay if we produce the
     same value.
  */
  refY = reference_cos(x);
  *okay = doubleIsEqualOrSameNonRealClass(y, refY);

  /* If one of x, y or the reference y is not a real number, 
     we cannot say anything more.

     We set the ulp error to zero, even if this does not really
     convey any information.
  */
  if (!(isNumber(x) && isNumber(y) && isNumber(refY))) {
    *ulpErr = 0.0;
    if ((!*okay) && display) displayWrongTestCase(x,y,refY,*ulpErr);
    return;
  }

  /* Here, x, y and the reference y are all real numbers. 

     We get a 165bit accurate approximation to cos and we
     compute the double precision ulp of the binade we are in.

  */
  mpfr_init2(xMpfr, 64);
  mpfr_init2(yMpfr, 165);
  mpfr_init2(deltaMpfr, 165);
  mpfr_init2(ulpMpfr, 64);
  mpfr_init2(ulpErrMpfr, 64);
  mpfr_set_d(xMpfr, x, GMP_RNDN);    /* exact */
  mpfr_cos(yMpfr, xMpfr, GMP_RNDN);  
  mpfr_sub_d(deltaMpfr, yMpfr, y, GMP_RNDN); 
  mpfr_abs(deltaMpfr, deltaMpfr, GMP_RNDN); /* exact */
  
  oldEmin = mpfr_get_emin();
  oldEmax = mpfr_get_emax();
  oldDefaultPrec = mpfr_get_default_prec();
  mpfr_set_default_prec(53);
  mpfr_set_emin(-1073); mpfr_set_emax(1024);
  mpfr_init(temp);
  ternaryA = mpfr_cos(temp, xMpfr, GMP_RNDZ);
  mpfr_subnormalize(temp, ternaryA, GMP_RNDZ);
  yDblRZ.d = mpfr_get_d(temp, GMP_RNDN); /* exact */
  mpfr_clear(temp);
  mpfr_set_emin(oldEmin);
  mpfr_set_emax(oldEmax);
  mpfr_set_default_prec(oldDefaultPrec);

  yDblRZPlus.i = yDblRZ.i + 1;
  ulp = fabs(yDblRZPlus.d - yDblRZ.d);
  mpfr_set_d(ulpMpfr, ulp, GMP_RNDN); /* exact */
  
  mpfr_div(ulpErrMpfr, deltaMpfr, ulpMpfr, GMP_RNDN);
  *ulpErr = mpfr_get_d(ulpErrMpfr, GMP_RNDU);

  mpfr_clear(ulpErrMpfr);
  mpfr_clear(ulpMpfr);
  mpfr_clear(deltaMpfr);
  mpfr_clear(yMpfr);
  mpfr_clear(xMpfr);

  /* Set "okay" result to yes if the ulp error is less than or equal to 
     the maximally allowed ulp error.
  */
  if (fabs(*ulpErr) <= fabs(maxAllowedUlpErr)) *okay = 1;

  if ((!*okay) && display) displayWrongTestCase(x,y,refY,*ulpErr);
}

void check_tan(double * RESTRICT ulpErr, int *okay, double y, double x, double maxAllowedUlpErr, int display) {
  double refY, ulp;
  mpfr_exp_t oldEmin, oldEmax;
  mpfr_prec_t oldDefaultPrec;
  int ternaryA;
  mpfr_t xMpfr, yMpfr, deltaMpfr, temp, ulpMpfr, ulpErrMpfr;
  dblcast yDblRZ, yDblRZPlus;

  /* Get a reference result, we are a priori okay if we produce the
     same value.
  */
  refY = reference_tan(x);
  *okay = doubleIsEqualOrSameNonRealClass(y, refY);

  /* If one of x, y or the reference y is not a real number, 
     we cannot say anything more.

     We set the ulp error to zero, even if this does not really
     convey any information.
  */
  if (!(isNumber(x) && isNumber(y) && isNumber(refY))) {
    *ulpErr = 0.0;
    if ((!*okay) && display) displayWrongTestCase(x,y,refY,*ulpErr);
    return;
  }

  /* Here, x, y and the reference y are all real numbers. 

     We get a 165bit accurate approximation to tan and we
     compute the double precision ulp of the binade we are in.

  */
  mpfr_init2(xMpfr, 64);
  mpfr_init2(yMpfr, 165);
  mpfr_init2(deltaMpfr, 165);
  mpfr_init2(ulpMpfr, 64);
  mpfr_init2(ulpErrMpfr, 64);
  mpfr_set_d(xMpfr, x, GMP_RNDN);    /* exact */
  mpfr_tan(yMpfr, xMpfr, GMP_RNDN);  
  mpfr_sub_d(deltaMpfr, yMpfr, y, GMP_RNDN); 
  mpfr_abs(deltaMpfr, deltaMpfr, GMP_RNDN); /* exact */
  
  oldEmin = mpfr_get_emin();
  oldEmax = mpfr_get_emax();
  oldDefaultPrec = mpfr_get_default_prec();
  mpfr_set_default_prec(53);
  mpfr_set_emin(-1073); mpfr_set_emax(1024);
  mpfr_init(temp);
  ternaryA = mpfr_tan(temp, xMpfr, GMP_RNDZ);
  mpfr_subnormalize(temp, ternaryA, GMP_RNDZ);
  yDblRZ.d = mpfr_get_d(temp, GMP_RNDN); /* exact */
  mpfr_clear(temp);
  mpfr_set_emin(oldEmin);
  mpfr_set_emax(oldEmax);
  mpfr_set_default_prec(oldDefaultPrec);

  yDblRZPlus.i = yDblRZ.i + 1;
  ulp = fabs(yDblRZPlus.d - yDblRZ.d);
  mpfr_set_d(ulpMpfr, ulp, GMP_RNDN); /* exact */
  
  mpfr_div(ulpErrMpfr, deltaMpfr, ulpMpfr, GMP_RNDN);
  *ulpErr = mpfr_get_d(ulpErrMpfr, GMP_RNDU);

  mpfr_clear(ulpErrMpfr);
  mpfr_clear(ulpMpfr);
  mpfr_clear(deltaMpfr);
  mpfr_clear(yMpfr);
  mpfr_clear(xMpfr);

  /* Set "okay" result to yes if the ulp error is less than or equal to 
     the maximally allowed ulp error.
  */
  if (fabs(*ulpErr) <= fabs(maxAllowedUlpErr)) *okay = 1;

  if ((!*okay) && display) displayWrongTestCase(x,y,refY,*ulpErr);
}

void check_asin(double * RESTRICT ulpErr, int *okay, double y, double x, double maxAllowedUlpErr, int display) {
  double refY, ulp;
  mpfr_exp_t oldEmin, oldEmax;
  mpfr_prec_t oldDefaultPrec;
  int ternaryA;
  mpfr_t xMpfr, yMpfr, deltaMpfr, temp, ulpMpfr, ulpErrMpfr;
  dblcast yDblRZ, yDblRZPlus;

  /* Get a reference result, we are a priori okay if we produce the
     same value.
  */
  refY = reference_asin(x);
  *okay = doubleIsEqualOrSameNonRealClass(y, refY);

  /* If one of x, y or the reference y is not a real number, 
     we cannot say anything more.

     We set the ulp error to zero, even if this does not really
     convey any information.
  */
  if (!(isNumber(x) && isNumber(y) && isNumber(refY))) {
    *ulpErr = 0.0;
    if ((!*okay) && display) displayWrongTestCase(x,y,refY,*ulpErr);
    return;
  }

  /* Here, x, y and the reference y are all real numbers. 

     We get a 165bit accurate approximation to asin and we
     compute the double precision ulp of the binade we are in.

  */
  mpfr_init2(xMpfr, 64);
  mpfr_init2(yMpfr, 165);
  mpfr_init2(deltaMpfr, 165);
  mpfr_init2(ulpMpfr, 64);
  mpfr_init2(ulpErrMpfr, 64);
  mpfr_set_d(xMpfr, x, GMP_RNDN);    /* exact */
  mpfr_asin(yMpfr, xMpfr, GMP_RNDN);  
  mpfr_sub_d(deltaMpfr, yMpfr, y, GMP_RNDN); 
  mpfr_abs(deltaMpfr, deltaMpfr, GMP_RNDN); /* exact */
  
  oldEmin = mpfr_get_emin();
  oldEmax = mpfr_get_emax();
  oldDefaultPrec = mpfr_get_default_prec();
  mpfr_set_default_prec(53);
  mpfr_set_emin(-1073); mpfr_set_emax(1024);
  mpfr_init(temp);
  ternaryA = mpfr_asin(temp, xMpfr, GMP_RNDZ);
  mpfr_subnormalize(temp, ternaryA, GMP_RNDZ);
  yDblRZ.d = mpfr_get_d(temp, GMP_RNDN); /* exact */
  mpfr_clear(temp);
  mpfr_set_emin(oldEmin);
  mpfr_set_emax(oldEmax);
  mpfr_set_default_prec(oldDefaultPrec);

  yDblRZPlus.i = yDblRZ.i + 1;
  ulp = fabs(yDblRZPlus.d - yDblRZ.d);
  mpfr_set_d(ulpMpfr, ulp, GMP_RNDN); /* exact */
  
  mpfr_div(ulpErrMpfr, deltaMpfr, ulpMpfr, GMP_RNDN);
  *ulpErr = mpfr_get_d(ulpErrMpfr, GMP_RNDU);

  mpfr_clear(ulpErrMpfr);
  mpfr_clear(ulpMpfr);
  mpfr_clear(deltaMpfr);
  mpfr_clear(yMpfr);
  mpfr_clear(xMpfr);

  /* Set "okay" result to yes if the ulp error is less than or equal to 
     the maximally allowed ulp error.
  */
  if (fabs(*ulpErr) <= fabs(maxAllowedUlpErr)) *okay = 1;

  if ((!*okay) && display) displayWrongTestCase(x,y,refY,*ulpErr);
}

void check_acos(double * RESTRICT ulpErr, int *okay, double y, double x, double maxAllowedUlpErr, int display) {
  double refY, ulp;
  mpfr_exp_t oldEmin, oldEmax;
  mpfr_prec_t oldDefaultPrec;
  int ternaryA;
  mpfr_t xMpfr, yMpfr, deltaMpfr, temp, ulpMpfr, ulpErrMpfr;
  dblcast yDblRZ, yDblRZPlus;

  /* Get a reference result, we are a priori okay if we produce the
     same value.
  */
  refY = reference_acos(x);
  *okay = doubleIsEqualOrSameNonRealClass(y, refY);

  /* If one of x, y or the reference y is not a real number, 
     we cannot say anything more.

     We set the ulp error to zero, even if this does not really
     convey any information.
  */
  if (!(isNumber(x) && isNumber(y) && isNumber(refY))) {
    *ulpErr = 0.0;
    if ((!*okay) && display) displayWrongTestCase(x,y,refY,*ulpErr);
    return;
  }

  /* Here, x, y and the reference y are all real numbers. 

     We get a 165bit accurate approximation to acos and we
     compute the double precision ulp of the binade we are in.

  */
  mpfr_init2(xMpfr, 64);
  mpfr_init2(yMpfr, 165);
  mpfr_init2(deltaMpfr, 165);
  mpfr_init2(ulpMpfr, 64);
  mpfr_init2(ulpErrMpfr, 64);
  mpfr_set_d(xMpfr, x, GMP_RNDN);    /* exact */
  mpfr_acos(yMpfr, xMpfr, GMP_RNDN);  
  mpfr_sub_d(deltaMpfr, yMpfr, y, GMP_RNDN); 
  mpfr_abs(deltaMpfr, deltaMpfr, GMP_RNDN); /* exact */
  
  oldEmin = mpfr_get_emin();
  oldEmax = mpfr_get_emax();
  oldDefaultPrec = mpfr_get_default_prec();
  mpfr_set_default_prec(53);
  mpfr_set_emin(-1073); mpfr_set_emax(1024);
  mpfr_init(temp);
  ternaryA = mpfr_acos(temp, xMpfr, GMP_RNDZ);
  mpfr_subnormalize(temp, ternaryA, GMP_RNDZ);
  yDblRZ.d = mpfr_get_d(temp, GMP_RNDN); /* exact */
  mpfr_clear(temp);
  mpfr_set_emin(oldEmin);
  mpfr_set_emax(oldEmax);
  mpfr_set_default_prec(oldDefaultPrec);

  yDblRZPlus.i = yDblRZ.i + 1;
  ulp = fabs(yDblRZPlus.d - yDblRZ.d);
  mpfr_set_d(ulpMpfr, ulp, GMP_RNDN); /* exact */
  
  mpfr_div(ulpErrMpfr, deltaMpfr, ulpMpfr, GMP_RNDN);
  *ulpErr = mpfr_get_d(ulpErrMpfr, GMP_RNDU);

  mpfr_clear(ulpErrMpfr);
  mpfr_clear(ulpMpfr);
  mpfr_clear(deltaMpfr);
  mpfr_clear(yMpfr);
  mpfr_clear(xMpfr);

  /* Set "okay" result to yes if the ulp error is less than or equal to 
     the maximally allowed ulp error.
  */
  if (fabs(*ulpErr) <= fabs(maxAllowedUlpErr)) *okay = 1;

  if ((!*okay) && display) displayWrongTestCase(x,y,refY,*ulpErr);
}

void check_atan(double * RESTRICT ulpErr, int *okay, double y, double x, double maxAllowedUlpErr, int display) {
  double refY, ulp;
  mpfr_exp_t oldEmin, oldEmax;
  mpfr_prec_t oldDefaultPrec;
  int ternaryA;
  mpfr_t xMpfr, yMpfr, deltaMpfr, temp, ulpMpfr, ulpErrMpfr;
  dblcast yDblRZ, yDblRZPlus;

  /* Get a reference result, we are a priori okay if we produce the
     same value.
  */
  refY = reference_atan(x);
  *okay = doubleIsEqualOrSameNonRealClass(y, refY);

  /* If one of x, y or the reference y is not a real number, 
     we cannot say anything more.

     We set the ulp error to zero, even if this does not really
     convey any information.
  */
  if (!(isNumber(x) && isNumber(y) && isNumber(refY))) {
    *ulpErr = 0.0;
    if ((!*okay) && display) displayWrongTestCase(x,y,refY,*ulpErr);
    return;
  }

  /* Here, x, y and the reference y are all real numbers. 

     We get a 165bit accurate approximation to atan and we
     compute the double precision ulp of the binade we are in.

  */
  mpfr_init2(xMpfr, 64);
  mpfr_init2(yMpfr, 165);
  mpfr_init2(deltaMpfr, 165);
  mpfr_init2(ulpMpfr, 64);
  mpfr_init2(ulpErrMpfr, 64);
  mpfr_set_d(xMpfr, x, GMP_RNDN);    /* exact */
  mpfr_atan(yMpfr, xMpfr, GMP_RNDN);  
  mpfr_sub_d(deltaMpfr, yMpfr, y, GMP_RNDN); 
  mpfr_abs(deltaMpfr, deltaMpfr, GMP_RNDN); /* exact */
  
  oldEmin = mpfr_get_emin();
  oldEmax = mpfr_get_emax();
  oldDefaultPrec = mpfr_get_default_prec();
  mpfr_set_default_prec(53);
  mpfr_set_emin(-1073); mpfr_set_emax(1024);
  mpfr_init(temp);
  ternaryA = mpfr_atan(temp, xMpfr, GMP_RNDZ);
  mpfr_subnormalize(temp, ternaryA, GMP_RNDZ);
  yDblRZ.d = mpfr_get_d(temp, GMP_RNDN); /* exact */
  mpfr_clear(temp);
  mpfr_set_emin(oldEmin);
  mpfr_set_emax(oldEmax);
  mpfr_set_default_prec(oldDefaultPrec);

  yDblRZPlus.i = yDblRZ.i + 1;
  ulp = fabs(yDblRZPlus.d - yDblRZ.d);
  mpfr_set_d(ulpMpfr, ulp, GMP_RNDN); /* exact */
  
  mpfr_div(ulpErrMpfr, deltaMpfr, ulpMpfr, GMP_RNDN);
  *ulpErr = mpfr_get_d(ulpErrMpfr, GMP_RNDU);

  mpfr_clear(ulpErrMpfr);
  mpfr_clear(ulpMpfr);
  mpfr_clear(deltaMpfr);
  mpfr_clear(yMpfr);
  mpfr_clear(xMpfr);

  /* Set "okay" result to yes if the ulp error is less than or equal to 
     the maximally allowed ulp error.
  */
  if (fabs(*ulpErr) <= fabs(maxAllowedUlpErr)) *okay = 1;

  if ((!*okay) && display) displayWrongTestCase(x,y,refY,*ulpErr);
}

void check_cbrt(double * RESTRICT ulpErr, int *okay, double y, double x, double maxAllowedUlpErr, int display) {
  double refY, ulp;
  mpfr_exp_t oldEmin, oldEmax;
  mpfr_prec_t oldDefaultPrec;
  int ternaryA;
  mpfr_t xMpfr, yMpfr, deltaMpfr, temp, ulpMpfr, ulpErrMpfr;
  dblcast yDblRZ, yDblRZPlus;

  /* Get a reference result, we are a priori okay if we produce the
     same value.
  */
  refY = reference_cbrt(x);
  *okay = doubleIsEqualOrSameNonRealClass(y, refY);

  /* If one of x, y or the reference y is not a real number, 
     we cannot say anything more.

     We set the ulp error to zero, even if this does not really
     convey any information.
  */
  if (!(isNumber(x) && isNumber(y) && isNumber(refY))) {
    *ulpErr = 0.0;
    if ((!*okay) && display) displayWrongTestCase(x,y,refY,*ulpErr);
    return;
  }

  /* Here, x, y and the reference y are all real numbers. 

     We get a 165bit accurate approximation to atan and we
     compute the double precision ulp of the binade we are in.

  */
  mpfr_init2(xMpfr, 64);
  mpfr_init2(yMpfr, 165);
  mpfr_init2(deltaMpfr, 165);
  mpfr_init2(ulpMpfr, 64);
  mpfr_init2(ulpErrMpfr, 64);
  mpfr_set_d(xMpfr, x, GMP_RNDN);    /* exact */
  mpfr_cbrt(yMpfr, xMpfr, GMP_RNDN);  
  mpfr_sub_d(deltaMpfr, yMpfr, y, GMP_RNDN); 
  mpfr_abs(deltaMpfr, deltaMpfr, GMP_RNDN); /* exact */
  
  oldEmin = mpfr_get_emin();
  oldEmax = mpfr_get_emax();
  oldDefaultPrec = mpfr_get_default_prec();
  mpfr_set_default_prec(53);
  mpfr_set_emin(-1073); mpfr_set_emax(1024);
  mpfr_init(temp);
  ternaryA = mpfr_cbrt(temp, xMpfr, GMP_RNDZ);
  mpfr_subnormalize(temp, ternaryA, GMP_RNDZ);
  yDblRZ.d = mpfr_get_d(temp, GMP_RNDN); /* exact */
  mpfr_clear(temp);
  mpfr_set_emin(oldEmin);
  mpfr_set_emax(oldEmax);
  mpfr_set_default_prec(oldDefaultPrec);

  yDblRZPlus.i = yDblRZ.i + 1;
  ulp = fabs(yDblRZPlus.d - yDblRZ.d);
  mpfr_set_d(ulpMpfr, ulp, GMP_RNDN); /* exact */
  
  mpfr_div(ulpErrMpfr, deltaMpfr, ulpMpfr, GMP_RNDN);
  *ulpErr = mpfr_get_d(ulpErrMpfr, GMP_RNDU);

  mpfr_clear(ulpErrMpfr);
  mpfr_clear(ulpMpfr);
  mpfr_clear(deltaMpfr);
  mpfr_clear(yMpfr);
  mpfr_clear(xMpfr);

  /* Set "okay" result to yes if the ulp error is less than or equal to 
     the maximally allowed ulp error.
  */
  if (fabs(*ulpErr) <= fabs(maxAllowedUlpErr)) *okay = 1;

  if ((!*okay) && display) displayWrongTestCase(x,y,refY,*ulpErr);
}

STATIC void check_vector_func(void (*check_func)(double * RESTRICT, int *, double, double, double, int),
			      double * RESTRICT maxUlp, int *okay, 
			      CONST double * RESTRICT y, CONST double * RESTRICT x, 
			      unsigned int n, double maxAllowedUlpErr, int display) {
  int pointOkay;
  unsigned int i;
  double pointUlpErr;

  *maxUlp = 0.0;
  *okay = 1;
  for (i=0;i<n;i++) {
    pointOkay = 0;
    pointUlpErr = 0.0;
    check_func(&pointUlpErr, &pointOkay, y[i], x[i], maxAllowedUlpErr, display);
    *okay = *okay && pointOkay;
    if (fabs(pointUlpErr) > *maxUlp) {
      *maxUlp = fabs(pointUlpErr);
    }
  }
}

STATIC void time_vector_func(uint64_t *timeUnits, 
			     void (*long_vector_func)(double * RESTRICT, CONST double * RESTRICT, unsigned int),
			     double * RESTRICT y, CONST double * RESTRICT x, unsigned int n, int runs) {
  int i;
  uint64_t before, after, time, overallTime;

  long_vector_func(y, x, n);
  overallTime = 0ull;
  for (i=0;i<runs;i++) {
    do {
      READ_TIME_COUNTER(&before);
      long_vector_func(y, x, n);
      READ_TIME_COUNTER(&after);
    } while (before >= after);
    time = after - before;
    overallTime += time;
  }

  *timeUnits = overallTime;
}

STATIC void count_outputs(uint64_t *normalOutputs, uint64_t *subnormalOutputs, uint64_t *infinityOutputs, uint64_t *nanOutputs, 
			  CONST double * RESTRICT y, unsigned int n) {
  unsigned int i;

  for (i=0;i<n;i++) {
    if (isNormal(y[i])) {
      (*normalOutputs)++;
    }
    if (isSubnormal(y[i])) {
      (*subnormalOutputs)++;
    }
    if (isInfinity(y[i])) {
      (*infinityOutputs)++;
    }
    if (isNan(y[i])) {
      (*nanOutputs)++;
    }
  }
}

STATIC void run_time_and_check_vector_func(uint64_t *timeUnits,
					   double * RESTRICT maxUlp, int *okay, 
					   uint64_t *normalOutputs, uint64_t *subnormalOutputs, uint64_t *infinityOutputs, uint64_t *nanOutputs,
					   void (*long_vector_func)(double * RESTRICT, CONST double * RESTRICT, unsigned int),
					   void (*check_func)(double * RESTRICT, int *, double, double, double, int),
					   unsigned int n, int k, int runs, double maxAllowedUlpErr,
					   testcase_t testcase,
					   int display) {
  int i;
  double *x, *y;
  uint64_t myTimeUnits;
  double maxUlpErr;
  int myOkay;

  *timeUnits = 0ull;
  *maxUlp = 0.0;
  *okay = 1;

  if ((x = (double *) memalign(VECTOR_LENGTH * sizeof(double), n * sizeof(double))) == NULL) {
    fprintf(stderr, "Could not allocate enough memory\n");
    exit(3);
  }
  if ((y = (double *) memalign(VECTOR_LENGTH * sizeof(double), n * sizeof(double))) == NULL) {
    free(x);
    fprintf(stderr, "Could not allocate enough memory\n");
    exit(3);
  }
  
  for (i=0;i<k;i++) {
    myTimeUnits = 0ull;
    maxUlpErr = 0.0;
    myOkay = 0;
    generate_test_vector(x, n, testcase);
    long_vector_func(y, x, n);
    check_vector_func(check_func, &maxUlpErr, &myOkay, y, x, n, maxAllowedUlpErr, display);
    count_outputs(normalOutputs, subnormalOutputs, infinityOutputs, nanOutputs, y, n);
    time_vector_func(&myTimeUnits, long_vector_func, y, x, n, runs);
    *okay = *okay && myOkay;
    if (fabs(maxUlpErr) > fabs(*maxUlp)) {
      *maxUlp = fabs(maxUlpErr);
    }
    *timeUnits += myTimeUnits;
  }

  free(y);
  free(x);
}

STATIC int display_time_and_check_vector_func(void (*long_vector_exp)(double * RESTRICT, CONST double * RESTRICT, unsigned int),
					      void (*check_func)(double * RESTRICT, int *, double, double, double, int),
					      unsigned int n, int k, int runs, double maxAllowedUlpErr,
					      testcase_t testcase, int display, double timeScale) {
  uint64_t timeUnits, normalOutputs, subnormalOutputs, infinityOutputs, nanOutputs; 
  double maxUlpErr; 
  int okay;
  
  timeUnits = 0ull;
  maxUlpErr = 0.0;
  okay = 1;
  normalOutputs = 0ull;
  subnormalOutputs = 0ull;
  infinityOutputs = 0ull;
  nanOutputs = 0ull;
  run_time_and_check_vector_func(&timeUnits, &maxUlpErr, &okay, 
				 &normalOutputs, &subnormalOutputs, &infinityOutputs, &nanOutputs,
				 long_vector_exp, check_func, n, k, runs, maxAllowedUlpErr, testcase, display);
  printf("Testcase \"%s\": test \"%s\", max ulp err: %3.4f, avg. timing: %10.4f time units per element, normal outputs %6.2f%%, subnormal outputs %6.2f%%, infinity outputs %6.2f%%, NaN outputs %6.2f%%\n", 
	 testcase_description(testcase), 
	 (okay ? "okay" : "not okay"), 
	 maxUlpErr,
	 (((double) timeUnits) / (((double) n) * ((double) k) * ((double) runs))) * timeScale,
	 (((double) normalOutputs) / (((double) n) * ((double) k))) * 100.0,
	 (((double) subnormalOutputs) / (((double) n) * ((double) k))) * 100.0,
	 (((double) infinityOutputs) / (((double) n) * ((double) k))) * 100.0,
	 (((double) nanOutputs) / (((double) n) * ((double) k))) * 100.0);
	 
  return okay;
}

STATIC void printUsage(char *str) {
  fprintf(stderr, "Usage: %s <func> <max ulp err> <vector length> <number of tests> <timing runs> <time scale> <random seed>\n",str);
  fprintf(stderr, "\n");
}

STATIC int readArg(int *r, char *str, int min) {
  int res;
  char *end;
  
  if (str[0] == '\0') return 0;
  res = strtol(str, &end, 10);
  if (end[0] != '\0') return 0;

  if (res < min) return 0;

  *r = res;
  return 1;
}

STATIC int readArgUnsigned(unsigned int *r, char *str, int min) {
  unsigned int res;
  char *end;
  
  if (str[0] == '\0') return 0;
  res = strtoul(str, &end, 10);
  if (end[0] != '\0') return 0;

  if (res < min) return 0;

  *r = res;
  return 1;
}

STATIC int readDoubleArg(double *r, char *str, double min) {
  double res;
  char *end;
  
  if (str[0] == '\0') return 0;
  res = strtod(str, &end);
  if (end[0] != '\0') return 0;

  if (res < min) return 0;

  *r = res;
  return 1;
}

STATIC testfuncdescription_t *readFuncDescription(char *str) {
  int i;
  for (i=0;i<TESTCASE_VALID_TESTFUNCDESCRIPTIONS;i++) {
    if (strcmp(testfunc_name(testFuncDescriptions[i].testFunc), str) == 0) {
      return &(testFuncDescriptions[i]);
    }
  }
  return NULL;
}

int main(int argc, char **argv) {
  int okay = 1;
  int k, runs;
  unsigned int n;
  double maxAllowedUlpErr;
  double timeScale;
  int seed, i;
  testfuncdescription_t *testFuncDescription;
  
  if (argc < 8) {
    printUsage(argv[0]);
    return 2;
  }

  if ((testFuncDescription = readFuncDescription(argv[1])) == NULL) {
    printUsage(argv[0]);
    return 2;
  }

  if (!readDoubleArg(&maxAllowedUlpErr, argv[2], 0.0)) {
    printUsage(argv[0]);
    return 2;
  }
  maxAllowedUlpErr = fabs(maxAllowedUlpErr);

  if (!readArgUnsigned(&n, argv[3], 1)) {
    printUsage(argv[0]);
    return 2;
  }

  if (!readArg(&k, argv[4], 1)) {
    printUsage(argv[0]);
    return 2;
  }

  if (!readArg(&runs, argv[5], 1)) {
    printUsage(argv[0]);
    return 2;
  }

  if (!readDoubleArg(&timeScale, argv[6], 0.0)) {
    printUsage(argv[0]);
    return 2;
  }
  timeScale = fabs(timeScale);
  if (timeScale == 0.0) timeScale = 1.0;

  if (!readArg(&seed, argv[7], 0)) {
    printUsage(argv[0]);
    return 2;
  }

  printf("Vector function tester invoked with\n");
  for (i=0;i<argc;i++) {
    printf("%s ",argv[i]);
  }
  printf("\n\n");

  srand(seed);
  printf("Results for naive vector %s:\n", testfunc_description(testFuncDescription->testFunc));
  for (i=0;i<TESTCASE_VALID_TESTCASES;i++) {
    if (testFuncDescription->testCases[i] == TESTCASE_UNKNOWN) 
      break;
    display_time_and_check_vector_func(testFuncDescription->naive_long_vector_func, 
				       testFuncDescription->check_func, 
				       n, k, runs, maxAllowedUlpErr, testFuncDescription->testCases[i], 1, timeScale);
  }
  printf("\n");

  srand(seed);
  printf("Results for optimized vector %s:\n", testfunc_description(testFuncDescription->testFunc));
  okay = 1;
  for (i=0;i<TESTCASE_VALID_TESTCASES;i++) {
    if (testFuncDescription->testCases[i] == TESTCASE_UNKNOWN) 
      break;
    okay &= !!display_time_and_check_vector_func(testFuncDescription->optimized_long_vector_func, 
						 testFuncDescription->check_func, 
						 n, k, runs, maxAllowedUlpErr, testFuncDescription->testCases[i], 1, timeScale);
  }
  printf("\n");

  if (!okay) return 1;
  return 0;
}
