# Copyright 2014-2018 Christoph Lauter Sorbonne Université

# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:

# 1. Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer.

# 2. Redistributions in binary form must reproduce the above
# copyright notice, this list of conditions and the following
# disclaimer in the documentation and/or other materials provided
# with the distribution.

# 3. Neither the name of the copyright holder nor the names of its
# contributors may be used to endorse or promote products derived
# from this software without specific prior written permission.

# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
# INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
# STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
# OF THE POSSIBILITY OF SUCH DAMAGE.

CFLAGS = -std=c99 -O3 -march=native -ftree-vectorize -ftree-vectorizer-verbose=1 -fno-math-errno
LDLIBS = -lm -lmpfr

FUNC_SRC = exp.c log.c sin.c cos.c tan.c asin.c acos.c atan.c cbrt.c
FUNC_OBJ = $(FUNC_SRC:.c=.o)

TEST_SRC = vector_tester.c
TEST_OBJ = $(TEST_SRC:.c=.o)

TESTS = $(FUNC_SRC:.c=.t)

TEST_MAX_ULP = 8
TEST_VECTOR_LEN = 1024
TEST_NUMBER = 128
TEST_TIMING_RUNS = 16
TEST_TIME_SCALE = 1
TEST_SEED = 1

TABLE = table.tex

TEST_EXEC = vector_tester

GLOBAL_HEADERS = vector.h

all: $(TEST_EXEC)

%.o: %.c %.h $(GLOBAL_HEADERS)
	gcc $(CFLAGS) -c $<

$(TEST_OBJ): $(TEST_SRC) $(GLOBAL_HEADERS)
	gcc $(CFLAGS) -c $<

$(TEST_EXEC): $(FUNC_OBJ) $(TEST_OBJ)
	gcc $(CFLAGS) -o $@ $(FUNC_OBJ) $(TEST_OBJ) $(LDLIBS)

test: $(TESTS)

$(TESTS): $(TEST_EXEC)
	./$(TEST_EXEC) $(@:.t=) $(TEST_MAX_ULP) $(TEST_VECTOR_LEN) $(TEST_NUMBER) $(TEST_TIMING_RUNS) $(TEST_TIME_SCALE) $(TEST_SEED) > $@

$(TABLE): $(TESTS)
	./write-table $^ > $@


table: $(TABLE)
	cat $^

check: table

clean:
	rm -f $(TEST_OBJ) $(FUNC_OBJ) $(TEST_EXEC) $(TESTS) $(TABLE)


.PHONY: all clean test table check

