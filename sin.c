/* This program implements a show-case vector (vectorizable) double
   precision sine with a 4 ulp error bound.

   Author: Christoph Lauter,

           Sorbonne Université - LIP6 - PEQUAN team.

   This program uses code generated using Sollya and Metalibm; see the
   licences and exception texts below.

   This program is

   Copyright 2014-2018 Christoph Lauter Sorbonne Université

   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions
   are met:

   1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.

   2. Redistributions in binary form must reproduce the above
   copyright notice, this list of conditions and the following
   disclaimer in the documentation and/or other materials provided
   with the distribution.

   3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived
   from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
   FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
   COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
   INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
   (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
   SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
   HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
   STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
   OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/* 

    This code was generated using non-trivial code generation commands
    of the Metalibm software program.
    
    Before using, modifying and/or integrating this code into other
    software, review the copyright and license status of this
    generated code. In particular, see the exception below.

    This generated program is partly or entirely based on a program
    generated using non-trivial code generation commands of the Sollya
    software program. See the copyright notice and exception text
    referring to that Sollya-generated part of this program generated
    with Metalibm below.

    Metalibm is
 
    Copyright 2008-2013 by 

    Laboratoire de l'Informatique du Parallélisme, 
    UMR CNRS - ENS Lyon - UCB Lyon 1 - INRIA 5668

    and by

    Laboratoire d'Informatique de Paris 6, equipe PEQUAN,
    UPMC Universite Paris 06 - CNRS - UMR 7606 - LIP6, Paris, France.

    Contributors: Christoph Quirin Lauter 
                  (UPMC LIP6 PEQUAN formerly LIP/ENS Lyon) 
                  christoph.lauter@lip6.fr

		  and

		  Olga Kupriianova 
		  (UPMC LIP6 PEQUAN)
		  olga.kupriianova@lip6.fr

    Metalibm was formerly developed by the Arenaire project at Ecole
    Normale Superieure de Lyon and is now developed by Equipe PEQUAN
    at Universite Pierre et Marie Curie Paris 6.

    The Metalibm software program is free software; you can
    redistribute it and/or modify it under the terms of the GNU Lesser
    General Public License as published by the Free Software
    Foundation; either version 2 of the License, or (at your option)
    any later version.

    Metalibm is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with the Metalibm program; if not, write to the Free
    Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
    02111-1307, USA.

    This generated program is distributed WITHOUT ANY WARRANTY; without
    even the implied warranty of MERCHANTABILITY or FITNESS FOR A
    PARTICULAR PURPOSE.
    
    As a special exception, you may create a larger work that contains
    part or all of this software generated using Metalibm and
    distribute that work under terms of your choice, so long as that
    work isn't itself a numerical code generator using the skeleton of
    this code or a modified version thereof as a code skeleton.
    Alternatively, if you modify or redistribute this generated code
    itself, or its skeleton, you may (at your option) remove this
    special exception, which will cause this generated code and its
    skeleton and the resulting Metalibm output files to be licensed
    under the General Public licence (version 2) without this special
    exception.
    
    This special exception was added by the Metalibm copyright holders 
    on November 20th 2013.
    
*/



/*
    This code was generated using non-trivial code generation commands of
    the Sollya software program.
    
    Before using, modifying and/or integrating this code into other
    software, review the copyright and license status of this generated
    code. In particular, see the exception below.
    
    Sollya is
    
    Copyright 2006-2013 by
    
    Laboratoire de l'Informatique du Parallelisme, UMR CNRS - ENS Lyon -
    UCB Lyon 1 - INRIA 5668,
    
    Laboratoire d'Informatique de Paris 6, equipe PEQUAN, UPMC Universite
    Paris 06 - CNRS - UMR 7606 - LIP6, Paris, France
    
    and by
    
    Centre de recherche INRIA Sophia-Antipolis Mediterranee, equipe APICS,
    Sophia Antipolis, France.
    
    Contributors Ch. Lauter, S. Chevillard, M. Joldes
    
    christoph.lauter@ens-lyon.org
    sylvain.chevillard@ens-lyon.org
    joldes@lass.fr
    
    The Sollya software is a computer program whose purpose is to provide
    an environment for safe floating-point code development. It is
    particularily targeted to the automatized implementation of
    mathematical floating-point libraries (libm). Amongst other features,
    it offers a certified infinity norm, an automatic polynomial
    implementer and a fast Remez algorithm.
    
    The Sollya software is governed by the CeCILL-C license under French
    law and abiding by the rules of distribution of free software.  You
    can use, modify and/ or redistribute the software under the terms of
    the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
    following URL "http://www.cecill.info".
    
    As a counterpart to the access to the source code and rights to copy,
    modify and redistribute granted by the license, users are provided
    only with a limited warranty and the software's author, the holder of
    the economic rights, and the successive licensors have only limited
    liability.
    
    In this respect, the user's attention is drawn to the risks associated
    with loading, using, modifying and/or developing or reproducing the
    software by the user in light of its specific status of free software,
    that may mean that it is complicated to manipulate, and that also
    therefore means that it is reserved for developers and experienced
    professionals having in-depth computer knowledge. Users are therefore
    encouraged to load and test the software's suitability as regards
    their requirements in conditions enabling the security of their
    systems and/or data to be ensured and, more generally, to use and
    operate it in the same conditions as regards security.
    
    The fact that you are presently reading this means that you have had
    knowledge of the CeCILL-C license and that you accept its terms.
    
    The Sollya program is distributed WITHOUT ANY WARRANTY; without even
    the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
    PURPOSE.
    
    This generated program is distributed WITHOUT ANY WARRANTY; without
    even the implied warranty of MERCHANTABILITY or FITNESS FOR A
    PARTICULAR PURPOSE.
    
    As a special exception, you may create a larger work that contains
    part or all of this software generated using Sollya and distribute
    that work under terms of your choice, so long as that work isn't
    itself a numerical code generator using the skeleton of this code or a
    modified version thereof as a code skeleton.  Alternatively, if you
    modify or redistribute this generated code itself, or its skeleton,
    you may (at your option) remove this special exception, which will
    cause this generated code and its skeleton and the resulting Sollya
    output files to be licensed under the CeCILL-C licence without this
    special exception.
    
    This special exception was added by the Sollya copyright holders in
    version 4.1 of Sollya.
    
*/

#include <stdint.h>
#include "trigo.h"
#include "sin.h"

/* Two caster types */
typedef union _dblcast {
  double   d;
  uint64_t i;
} dblcast;

typedef union {
  int64_t l;
  double d;
} db_number;

/* Macro implementations of some double-double operations */
#define Add12(s, r, a, b)                       \
  {double _z, _a=a, _b=b;                       \
    s = _a + _b;                                \
    _z = s - _a;                                \
    r = _b - _z;   }

#define Add12Cond(s, r, a, b)                   \
  {                                             \
    double _u1, _u2, _u3, _u4;                  \
    double  _a=a, _b=b;                         \
                                                \
    s = _a + _b;                                \
    _u1 = s - _a;                               \
    _u2 = s - _u1;                              \
    _u3 = _b - _u1;                             \
    _u4 = _a - _u2;                             \
    r = _u4 + _u3;                              \
  }

#define Mul12(rh,rl,u,v)                                \
  {                                                     \
    CONST double c  = 134217729.; /* 2^27 +1 */         \
    double up, u1, u2, vp, v1, v2;                      \
    double _u =u, _v=v;                                 \
                                                        \
    up = _u*c;        vp = _v*c;                        \
    u1 = (_u-up)+up;  v1 = (_v-vp)+vp;                  \
    u2 = _u-u1;       v2 = _v-v1;                       \
                                                        \
    *rh = _u*_v;                                        \
    *rl = (((u1*v1-*rh)+(u1*v2))+(u2*v1))+(u2*v2);      \
  }

#define Mul122(resh,resl,a,bh,bl)               \
  {                                             \
    double _t1, _t2, _t3, _t4;                  \
                                                \
    Mul12(&_t1,&_t2,(a),(bh));                  \
    _t3 = (a) * (bl);                           \
    _t4 = _t2 + _t3;                            \
    Add12((*(resh)),(*(resl)),_t1,_t4);         \
  }

#define Mul22(zh,zl,xh,xl,yh,yl)                        \
  {                                                     \
    double mh, ml;                                      \
                                                        \
    CONST double c = 134217729.;                        \
    double up, u1, u2, vp, v1, v2;                      \
                                                        \
    up = (xh)*c;        vp = (yh)*c;                    \
    u1 = ((xh)-up)+up;  v1 = ((yh)-vp)+vp;              \
    u2 = (xh)-u1;       v2 = (yh)-v1;                   \
                                                        \
    mh = (xh)*(yh);                                     \
    ml = (((u1*v1-mh)+(u1*v2))+(u2*v1))+(u2*v2);        \
                                                        \
    ml += (xh)*(yl) + (xl)*(yh);                        \
    *zh = mh+ml;                                        \
    *zl = mh - (*zh) + ml;                              \
  }

#define Add122(resh,resl,a,bh,bl)               \
  {                                             \
    double _t1, _t2, _t3;                       \
                                                \
    Add12(_t1,_t2,(a),(bh));                    \
    _t3 = _t2 + (bl);                           \
    Add12((*(resh)),(*(resl)),_t1,_t3);         \
  }

#define Renormalize3(resh, resm, resl, ah, am, al)      \
  {                                                     \
    double _t1h, _t1l, _t2l;                            \
                                                        \
    Add12(_t1h, _t1l, (am), (al));                      \
    Add12((*(resh)), _t2l, (ah), (_t1h));               \
    Add12((*(resm)), (*(resl)), _t2l, _t1l);            \
  }

#define Mul133(resh, resm, resl, a, bh, bm, bl)         \
  {                                                     \
    double _t2, _t3, _t4, _t5, _t7, _t8, _t9, _t10;     \
                                                        \
    Mul12((resh),&_t2,(a),(bh));                        \
    Mul12(&_t3,&_t4,(a),(bm));                          \
    _t5 = (a) * (bl);                                   \
    Add12Cond(_t9,_t7,_t2,_t3);                         \
    _t8 = _t4 + _t5;                                    \
    _t10 = _t7 + _t8;                                   \
    Add12Cond((*(resm)),(*(resl)),_t9,_t10);            \
  }

/* Need fabs */
double fabs(double);


/* Some constants */
#define SINE_EASY_PATH_BOUND            5.1470283240088378079235553741455078125e4
#define SINE_SPURIOUS_UNDERFLOW_BOUND   3.0549363634996046820519793932136176997894027405723e-151
#define SINE_RCPR_PI                    0.318309886183790691216444201927515678107738494873047
#define SHIFTER                         6755399441055744.0
#define SINE_PI_HI                      3.1415926535919425077736377716064453125                     /* 14 trailing zeros in double precision */
#define SINE_PI_MI                      (-2.1492693109956619903751304928585241782457160297781e-12)  /* 14 trailing zeros in double precision */
#define SINE_PI_LO                      1.27366343270274062722736216588063328297815573234383e-24    /* 14 trailing zeros in double precision */
#define SINE_ARG_RED_BOUND              1.57079632679787239979418700386304408311843872070312
#define SCALAR_SINE_SHORT_PATH          0x3e60000000000000ull                                       /* 2^-25 */
#define SCALAR_SINE_NO_ARG_RED_BOUND    1.87999999999999989341858963598497211933135986328125 
#define SCALAR_SINE_CONST_PI            3.141592653589793115997963468544185161590576171875
#define SCALAR_SINE_NEAREST_INT_TWO_53  9007199254740992.0
#define SCALAR_SINE_NEAREST_INT_TWO_52  4503599627370496.0

/* A generated polynomial for the scalar callout */
#define scalar_sin_callout_poly_coeff_1h 1.00000000000000000000000000000000000000000000000000000000000000000000000000000000e+00
#define scalar_sin_callout_poly_coeff_3h -1.66666666666666574148081281236954964697360992431640625000000000000000000000000000e-01
#define scalar_sin_callout_poly_coeff_5h 8.33333333333223340300133230584833654575049877166748046875000000000000000000000000e-03
#define scalar_sin_callout_poly_coeff_7h -1.98412698408651004501265791013508987816749140620231628417968750000000000000000000e-04
#define scalar_sin_callout_poly_coeff_9h 2.75573191572531937373262238277771274397309753112494945526123046875000000000000000e-06
#define scalar_sin_callout_poly_coeff_11h -2.50521025524437665133008483909354824348270085465628653764724731445312500000000000e-08
#define scalar_sin_callout_poly_coeff_13h 1.60587532417240507279041066091636487389582299556423095054924488067626953125000000e-10
#define scalar_sin_callout_poly_coeff_15h -7.63885420198016487931413481616261081574500990676312994764884933829307556152343750e-13
#define scalar_sin_callout_poly_coeff_16h 6.42537098984337587870039391790357587374749594123175285484812985573044308296175586e-23
#define scalar_sin_callout_poly_coeff_17h 2.68402940758285017551982796876246242978911833373945050595921202329918742179870605e-15


STATIC INLINE void scalar_sin_callout_poly_inner(double * RESTRICT scalar_sin_callout_poly_resh, double * RESTRICT scalar_sin_callout_poly_resm, double x) {
  double scalar_sin_callout_poly_x_0_pow2h, scalar_sin_callout_poly_x_0_pow2m;


  Mul12(&scalar_sin_callout_poly_x_0_pow2h,&scalar_sin_callout_poly_x_0_pow2m,x,x);


  double scalar_sin_callout_poly_t_1_0h;
  double scalar_sin_callout_poly_t_2_0h;
  double scalar_sin_callout_poly_t_3_0h;
  double scalar_sin_callout_poly_t_4_0h;
  double scalar_sin_callout_poly_t_5_0h;
  double scalar_sin_callout_poly_t_6_0h;
  double scalar_sin_callout_poly_t_7_0h;
  double scalar_sin_callout_poly_t_8_0h;
  double scalar_sin_callout_poly_t_9_0h;
  double scalar_sin_callout_poly_t_10_0h;
  double scalar_sin_callout_poly_t_11_0h;
  double scalar_sin_callout_poly_t_12_0h;
  double scalar_sin_callout_poly_t_13_0h;
  double scalar_sin_callout_poly_t_14_0h;
  double scalar_sin_callout_poly_t_15_0h;
  double scalar_sin_callout_poly_t_16_0h;
  double scalar_sin_callout_poly_t_17_0h, scalar_sin_callout_poly_t_17_0m;
  double scalar_sin_callout_poly_t_18_0h, scalar_sin_callout_poly_t_18_0m;
  double scalar_sin_callout_poly_t_19_0h, scalar_sin_callout_poly_t_19_0m;
  double scalar_sin_callout_poly_t_20_0h, scalar_sin_callout_poly_t_20_0m;
 


  scalar_sin_callout_poly_t_1_0h = scalar_sin_callout_poly_coeff_17h;
  scalar_sin_callout_poly_t_2_0h = scalar_sin_callout_poly_t_1_0h * x;
  scalar_sin_callout_poly_t_3_0h = scalar_sin_callout_poly_coeff_16h + scalar_sin_callout_poly_t_2_0h;
  scalar_sin_callout_poly_t_4_0h = scalar_sin_callout_poly_t_3_0h * x;
  scalar_sin_callout_poly_t_5_0h = scalar_sin_callout_poly_coeff_15h + scalar_sin_callout_poly_t_4_0h;
  scalar_sin_callout_poly_t_6_0h = scalar_sin_callout_poly_t_5_0h * scalar_sin_callout_poly_x_0_pow2h;
  scalar_sin_callout_poly_t_7_0h = scalar_sin_callout_poly_coeff_13h + scalar_sin_callout_poly_t_6_0h;
  scalar_sin_callout_poly_t_8_0h = scalar_sin_callout_poly_t_7_0h * scalar_sin_callout_poly_x_0_pow2h;
  scalar_sin_callout_poly_t_9_0h = scalar_sin_callout_poly_coeff_11h + scalar_sin_callout_poly_t_8_0h;
  scalar_sin_callout_poly_t_10_0h = scalar_sin_callout_poly_t_9_0h * scalar_sin_callout_poly_x_0_pow2h;
  scalar_sin_callout_poly_t_11_0h = scalar_sin_callout_poly_coeff_9h + scalar_sin_callout_poly_t_10_0h;
  scalar_sin_callout_poly_t_12_0h = scalar_sin_callout_poly_t_11_0h * scalar_sin_callout_poly_x_0_pow2h;
  scalar_sin_callout_poly_t_13_0h = scalar_sin_callout_poly_coeff_7h + scalar_sin_callout_poly_t_12_0h;
  scalar_sin_callout_poly_t_14_0h = scalar_sin_callout_poly_t_13_0h * scalar_sin_callout_poly_x_0_pow2h;
  scalar_sin_callout_poly_t_15_0h = scalar_sin_callout_poly_coeff_5h + scalar_sin_callout_poly_t_14_0h;
  scalar_sin_callout_poly_t_16_0h = scalar_sin_callout_poly_t_15_0h * scalar_sin_callout_poly_x_0_pow2h;
  Add12(scalar_sin_callout_poly_t_17_0h,scalar_sin_callout_poly_t_17_0m,scalar_sin_callout_poly_coeff_3h,scalar_sin_callout_poly_t_16_0h);
  Mul22(&scalar_sin_callout_poly_t_18_0h,&scalar_sin_callout_poly_t_18_0m,scalar_sin_callout_poly_t_17_0h,scalar_sin_callout_poly_t_17_0m,scalar_sin_callout_poly_x_0_pow2h,scalar_sin_callout_poly_x_0_pow2m);
  Add122(&scalar_sin_callout_poly_t_19_0h,&scalar_sin_callout_poly_t_19_0m,scalar_sin_callout_poly_coeff_1h,scalar_sin_callout_poly_t_18_0h,scalar_sin_callout_poly_t_18_0m);
  Mul122(&scalar_sin_callout_poly_t_20_0h,&scalar_sin_callout_poly_t_20_0m,x,scalar_sin_callout_poly_t_19_0h,scalar_sin_callout_poly_t_19_0m);
  *scalar_sin_callout_poly_resh = scalar_sin_callout_poly_t_20_0h; *scalar_sin_callout_poly_resm = scalar_sin_callout_poly_t_20_0m;


}

STATIC INLINE double scalar_sin_callout_poly(double x) {
  double yh, yl;

  scalar_sin_callout_poly_inner(&yh, &yl, x);

  return yh + yl;
}

STATIC INLINE void scalar_sin_callout_easy_arg_red(double * RESTRICT sign, double * RESTRICT r, double x) {
  double xTimesRcprPi;
  double shiftedXTimesRcprPi;
  double k;
  double kTimesPiHi, kTimesPiMi, kTimesPiLo;
  double tempR1, tempR2;
  dblcast signBit;

  xTimesRcprPi = x * SINE_RCPR_PI;
  shiftedXTimesRcprPi = xTimesRcprPi + SHIFTER;
  k = shiftedXTimesRcprPi - SHIFTER;
  kTimesPiHi = k * SINE_PI_HI;  /* exact, trailing zeros */
  kTimesPiMi = k * SINE_PI_MI;  /* exact, trailing zeros */
  kTimesPiLo = k * SINE_PI_LO;  /* exact, trailing zeros */
  tempR1 = x - kTimesPiHi;      /* exact, Sterbenz */
  tempR2 = tempR1 - kTimesPiMi; /* exact or one rounding error */
  *r = tempR2 - kTimesPiLo;     /* r = tempR2 if operation yielding
				   tempR2 has not been exact */

  signBit.d = shiftedXTimesRcprPi;
  signBit.i <<= 63;
  signBit.i |= 0x3ff0000000000000ull;
  *sign = signBit.d;
}

/* Computes a triple-double approximation to 

   t(E) = 2^E * 1/pi - 2 * floor(2^(E - 1) * 1/pi)

   where E is in the range 

   -39 <= E <= 971.

   For the time being, we use a simple table. In principle, the value
   of t(E) for any E can be computed out of a single
   multiple-precision expansion of 1/pi.

*/
STATIC INLINE void scalar_sin_callout_large_arg_red_get_rcpr_pi(double * RESTRICT tHi, double * RESTRICT tMi, double * RESTRICT tLo, int E) {
  *tHi = trigo_Payne_Hanek_Table_Hi[E - TRIGO_PAYNE_HANEK_E_MIN];
  *tMi = trigo_Payne_Hanek_Table_Mi[E - TRIGO_PAYNE_HANEK_E_MIN];
  *tLo = trigo_Payne_Hanek_Table_Lo[E - TRIGO_PAYNE_HANEK_E_MIN];
}

/* Computes n = nearestint(x) and sets odd to zero if n is even or to
   a non-zero value if n is odd. 

   The input x is supposed to be non-negative and larger than 2.

*/
STATIC INLINE void scalar_sin_callout_large_arg_red_nearestint_large(double * RESTRICT n, int * RESTRICT odd, double x) {
  dblcast temp, shiftedX;

  /* Double precision floating-point numbers larger than or equal to
     2^53 are even integers. 
  */
  if (x >= SCALAR_SINE_NEAREST_INT_TWO_53) {
    *n = x;
    *odd = 0;
    return;
  }

  /* Double precision floating-point numbers larger than or equal to
     2^52 are integers; their parity is in their ulp bit.
  */
  if (x >= SCALAR_SINE_NEAREST_INT_TWO_52) {
    *n = x;
    temp.d = x;
    *odd = temp.i & 1;
    return;
  }

  /* Here, the input is non-negative, greater than 2 and less than
     2^52.  The shifter trick therefore works, taking 2^52 as a
     shifter constant (and not 2^52 + 2^51).

     The parity of the nearest integer is in the ulp bit of the
     shifted value.
  */
  shiftedX.d = SCALAR_SINE_NEAREST_INT_TWO_52 + x;
  *odd = shiftedX.i & 1;
  *n = shiftedX.d - SCALAR_SINE_NEAREST_INT_TWO_52;
}

/* Computes n = nearestint(x) and sets odd to zero if n is even or to
   a non-zero value if n is odd. 

   The input x is supposed to be smaller than 2^48 in magnitude.

*/
STATIC INLINE void scalar_sin_callout_large_arg_red_nearestint_small(double * RESTRICT n, int * RESTRICT odd, double x) {
  dblcast shiftedX;

  /* The input is less than 2^48, hence less than 2^51-1. The shifter
     trick therefore works. The parity of the integer is in the ulp
     bit of the shifted value.
  */
  shiftedX.d = SHIFTER + x;
  *odd = shiftedX.i & 1;
  *n = shiftedX.d - SHIFTER;
}

/* Computes sign and r such that sign * sin(r) approximates sin(x) for x such
   that abs(x) >= 2^15 

   The reduced argument satisfies 

    abs(r) <= 1.75

   and 

    sign * sin(r) = sin(x) * (1 + eps) 

   where 

    abs(eps) <= 2^-51.5

*/
STATIC INLINE void scalar_sin_callout_large_arg_red(double * RESTRICT sign, double * RESTRICT r, double x) {
  dblcast xAbs;
  int E;
  double n;
  double tHi, tMi, tLo;
  double hHi, hMi, hLo;
  int oddHHi, oddHMi, oddHLo;
  double nTHi, nTMi, nTLo, nTRHi, nTRMi, nTRLo;
  double t1, t2, t3, t4, t5;
  double b;

  /* Strip off sign of x */
  if (x > 0.0) {
    xAbs.d = x;
    *sign = 1.0;
  } else {
    xAbs.d = -x;
    *sign = -1.0;
  }
  
  /* Decompose abs(x) into E and n such that 2^E * n = abs(x) */
  E = (xAbs.i >> 52) - 1023 - 52;
  xAbs.i = (xAbs.i & 0x000fffffffffffffull) | 0x4330000000000000ull;
  n = xAbs.d;

  /* Get a triple-double approximation to 

     t(E) = 2^E * 1/pi - 2 * floor(2^(E - 1) * 1/pi)

     Possible values of E that may reach this stage are in the range

     -39 <= E <= 971

  */
  scalar_sin_callout_large_arg_red_get_rcpr_pi(&tHi, &tMi, &tLo, E);


  /* Compute a renormalized triple-double approximation to 

     nTHi + nTMi + nTlo = n * (tHi + tMi + tLo) * (1 + eps) 

     with 

     abs(eps) <= 2^150.

  */
  Mul133(&nTRHi, &nTRMi, &nTRLo, n, tHi, tMi, tLo);
  Renormalize3(&nTHi, &nTMi, &nTLo, nTRHi, nTRMi, nTRLo);
  
  /* Compute hHi = nearestint(nTHi).

     Also compute the parity of hHi.

  */
  scalar_sin_callout_large_arg_red_nearestint_large(&hHi, &oddHHi, nTHi);
  
  /* Compute t1 = nTHi - hHi */
  t1 = nTHi - hHi; /* exact, Sterbenz */

  /* Compute hMi = nearestint(nTMi).

     Also compute the parity of hMi.

  */
  scalar_sin_callout_large_arg_red_nearestint_small(&hMi, &oddHMi, nTMi);
  
  /* Compute t2 = nTMi - hMi */
  t2 = nTMi - hMi; /* exact, Sterbenz or hMi zero */

  /* Now we have 

     hHi + hMi + t1 + t2 + nTLo = n * t * (1 + eps) 

     with abs(eps) <= 2^-150.

     Now normalize t1 + t2 into t3 + t4, such that 

     t3 + t4 = t1 + t2

     and t3 = RN(t3 + t4) where RN() is rounding to the nearest.

  */
  Add12Cond(t3, t4, t1, t2);

  /* Compute hLo = nearestint(t3).

     Also compute the parity of hLo.

  */
  scalar_sin_callout_large_arg_red_nearestint_small(&hLo, &oddHLo, t3);
  
  /* Compute t5 = t3 - hLo */
  t5 = t3 - hLo; /* exact, Sterbenz or hLo zero */
  
  /* We now have

     hHi + hMi + hLo + t5 + t4 + nTLo = n * t * (1 + eps) 

     where abs(eps) <= 2^-150 and hHi, hMi and hLo are integer.

     Now compute an approximation b to t5 + t4 + nTLo, with at most 2
     roundings.

  */
  b = t5 + (t4 + nTLo);

  /* Multiply b by pi, yielding r = b * pi */
  *r = b * SCALAR_SINE_CONST_PI; /* one rounding error */

  /* Now, nonwithstanding rounding and approximation errors we have:

     s = 2 * floor(2^(E - 1) * 1/pi), an even integer

     and therefore 

     2^E * 1/pi = s + t

     which yields

     1/pi = 2^-E * (s + t)

     Taking h = hHi + hMi + hLo and setting k = n * s + h, which is an
     integer while n * s is an even integer, we obtain

     sin(x) = sign * sin(abs(x)) 
            = sign * sin(2^E * n)
	    = sign * (-1)^h * sin(2^E * n - k * pi)
            = sign * (-1)^h * sin(pi * (2^E * n * 1/pi - k))
	    = sign * (-1)^h * sin(pi * (2^E * n * 2^(-E) * (s + t) - k))
	    = sign * (-1)^h * sin(pi * (n * s + n * t - k))
	    = sign * (-1)^h * sin(pi * (n * s + n * t - n * s - h))
	    = sign * (-1)^h * sin(pi * (n * t - h))
	    = sign * (-1)^h * sin(pi * b)
	    = sign * (-1)^h * sin(r)
	    = sign * (-1)^hHi * (-1)^hMi * (-1)^hLo * sin(r)

     So we are left with accounting for the sign.
  */
  if (oddHHi) {
    *sign = - *sign;
  }
  if (oddHMi) {
    *sign = - *sign;
  }
  if (oddHLo) {
    *sign = - *sign;
  }
}

/* A scalar sine for the callout -- main case 

   This function assumes that x is real and 
   larger than the short-path bound.

*/
STATIC INLINE double scalar_sin_callout_inner(double x) {
  double xAbs, r, sign;

  /* We have three paths:

     (i)   abs(x) <= SCALAR_SINE_NO_ARG_RED_BOUND: we simply use a polynomial
     (ii)  abs(x) <= SINE_EASY_PATH_BOUND: we perform easy argument reduction and 
                                           reuse the same polynomial
     (iii) otherwise: we perform large input argument reduction and 
                      reuse the same polynomial 

  */
  xAbs = fabs(x);
  if (xAbs <= SCALAR_SINE_NO_ARG_RED_BOUND) {
    return scalar_sin_callout_poly(x);
  }
  
  if (xAbs <= SINE_EASY_PATH_BOUND) {
    scalar_sin_callout_easy_arg_red(&sign, &r, x);
    return sign * scalar_sin_callout_poly(r);
  }
  
  scalar_sin_callout_large_arg_red(&sign, &r, x);
  return sign * scalar_sin_callout_poly(r);
}

/* A scalar sine for the callout */
STATIC INLINE double scalar_sin_callout(double x) {
  double xAbs;
  dblcast xAbsdb;

  /* A short-path for a lot of entries */
  xAbs = fabs(x);
  xAbsdb.d = xAbs;
  if (xAbsdb.i < SCALAR_SINE_SHORT_PATH) {
    /* For x s.t. abs(x) < 2^-25, abs(x/sin(x)-1)<2^-52.5 */
    return x;
  }

  /* Perform special case handling */
  if (xAbsdb.i >= 0x7ff0000000000000ull) {
    /* Input is Infinity or NaN 

       sin(+/- Infinity) = NaN, raising Invalid
       sin(q/sNaN) = qNaN, signaling for signaling NaN 

       Computing x - x does exactly that.

    */
    return x - x;
  }

  /* The input is real and larger that the short-part bound 

     Encapsulate that code in a function.

  */
  return scalar_sin_callout_inner(x);
}

/* A vector sine callout */
STATIC INLINE void vector_sin_callout(double * RESTRICT y, CONST double * RESTRICT x) {
  int i;

  for (i=0;i<VECTOR_LENGTH;i++) {
    y[i] = scalar_sin_callout(x[i]);
  }
}

/* Generated polynomial for vector sine */
#define vector_sin_poly_coeff_1h 9.99999999999999888977697537484345957636833190917968750000000000000000000000000000e-01
#define vector_sin_poly_coeff_3h -1.66666666666660107098962839700106997042894363403320312500000000000000000000000000e-01
#define vector_sin_poly_coeff_5h 8.33333333327784288313555549621014506556093692779541015625000000000000000000000000e-03
#define vector_sin_poly_coeff_7h -1.98412698235830153989031621186711618065601214766502380371093750000000000000000000e-04
#define vector_sin_poly_coeff_9h 2.75573164561690855321436990910122233344736741855740547180175781250000000000000000e-06
#define vector_sin_poly_coeff_11h -2.50518726968974993489337548500939245599283822230063378810882568359375000000000000e-08
#define vector_sin_poly_coeff_13h 1.60478927890930342097909610748845098127102204443872324191033840179443359375000000e-10
#define vector_sin_poly_coeff_15h -7.37117549637383984840573132919316648946871095571253817979595623910427093505859375e-13


STATIC INLINE void vector_sin_poly(double *  RESTRICT vector_sin_poly_resh, double x) {
  double vector_sin_poly_x_0_pow2h;


  vector_sin_poly_x_0_pow2h = x * x;


  double vector_sin_poly_t_1_0h;
  double vector_sin_poly_t_2_0h;
  double vector_sin_poly_t_3_0h;
  double vector_sin_poly_t_4_0h;
  double vector_sin_poly_t_5_0h;
  double vector_sin_poly_t_6_0h;
  double vector_sin_poly_t_7_0h;
  double vector_sin_poly_t_8_0h;
  double vector_sin_poly_t_9_0h;
  double vector_sin_poly_t_10_0h;
  double vector_sin_poly_t_11_0h;
  double vector_sin_poly_t_12_0h;
  double vector_sin_poly_t_13_0h;
  double vector_sin_poly_t_14_0h;
  double vector_sin_poly_t_15_0h;
  double vector_sin_poly_t_16_0h;
 


  vector_sin_poly_t_1_0h = vector_sin_poly_coeff_15h;
  vector_sin_poly_t_2_0h = vector_sin_poly_t_1_0h * vector_sin_poly_x_0_pow2h;
  vector_sin_poly_t_3_0h = vector_sin_poly_coeff_13h + vector_sin_poly_t_2_0h;
  vector_sin_poly_t_4_0h = vector_sin_poly_t_3_0h * vector_sin_poly_x_0_pow2h;
  vector_sin_poly_t_5_0h = vector_sin_poly_coeff_11h + vector_sin_poly_t_4_0h;
  vector_sin_poly_t_6_0h = vector_sin_poly_t_5_0h * vector_sin_poly_x_0_pow2h;
  vector_sin_poly_t_7_0h = vector_sin_poly_coeff_9h + vector_sin_poly_t_6_0h;
  vector_sin_poly_t_8_0h = vector_sin_poly_t_7_0h * vector_sin_poly_x_0_pow2h;
  vector_sin_poly_t_9_0h = vector_sin_poly_coeff_7h + vector_sin_poly_t_8_0h;
  vector_sin_poly_t_10_0h = vector_sin_poly_t_9_0h * vector_sin_poly_x_0_pow2h;
  vector_sin_poly_t_11_0h = vector_sin_poly_coeff_5h + vector_sin_poly_t_10_0h;
  vector_sin_poly_t_12_0h = vector_sin_poly_t_11_0h * vector_sin_poly_x_0_pow2h;
  vector_sin_poly_t_13_0h = vector_sin_poly_coeff_3h + vector_sin_poly_t_12_0h;
  vector_sin_poly_t_14_0h = vector_sin_poly_t_13_0h * vector_sin_poly_x_0_pow2h;
  vector_sin_poly_t_15_0h = vector_sin_poly_coeff_1h + vector_sin_poly_t_14_0h;
  vector_sin_poly_t_16_0h = vector_sin_poly_t_15_0h * x;
  *vector_sin_poly_resh = vector_sin_poly_t_16_0h;


}

/* A vector sine */
void vector_sin(double * RESTRICT yArg, CONST double * RESTRICT xArg) {
  int i;
  int okaySlots, argRedSlots;
  double * RESTRICT y;
  CONST double * RESTRICT x;
  double xTimesRcprPi, shiftedXTimesRcprPi;
  double k;
  double kTimesPiHi, kTimesPiMi, kTimesPiLo;
  double tempR1, tempR2, r;
  double currX;
  dblcast signBit, rc;
  double sinR;
  double xAbs;

  /* Assume alignment */
#ifdef NO_ASSUME_ALIGNED
  x = xArg;
  y = yArg;
#else
  x = __builtin_assume_aligned(xArg, VECTOR_LENGTH * __alignof__(double));
  y = __builtin_assume_aligned(yArg, VECTOR_LENGTH * __alignof__(double));
#endif  

  /* Check if we can handle all inputs on the easy path and compute
     indication if argument reduction is not needed anywhere. 
  */
  okaySlots = 0;
  argRedSlots = 0;
  for (i=0;i<VECTOR_LENGTH;i++) {
    xAbs = fabs(x[i]);
    okaySlots += ((xAbs < SINE_EASY_PATH_BOUND) & (xAbs > SINE_SPURIOUS_UNDERFLOW_BOUND)); 
    argRedSlots += (xAbs > SINE_ARG_RED_BOUND);
  }

  /* Perform a callout if we cannot handle the input in one slot */
  if (okaySlots != VECTOR_LENGTH) {
    vector_sin_callout(yArg, xArg);
    return;
  }

  /* If no argument reduction is needed anywhere, just use the
     polynomial 
  */
  if (argRedSlots == 0) {
    for (i=0;i<VECTOR_LENGTH;i++) {
      vector_sin_poly(&(y[i]), x[i]);
    }
    return;
  }

  /* Here we know that all inputs are real and small enough to
     allow for our easy argument reduction, using only a 122 bit accurate
     approximation to pi.
  */
  for (i=0;i<VECTOR_LENGTH;i++) {
    currX = x[i];
    
    /* Compute the reduced argument 
       
       r = x - k * pi 

       where k = nearestint(x / pi) 
    */
    xTimesRcprPi = currX * SINE_RCPR_PI;
    shiftedXTimesRcprPi = xTimesRcprPi + SHIFTER;
    k = shiftedXTimesRcprPi - SHIFTER;
    kTimesPiHi = k * SINE_PI_HI;  /* exact, trailing zeros */
    kTimesPiMi = k * SINE_PI_MI;  /* exact, trailing zeros */
    kTimesPiLo = k * SINE_PI_LO;  /* exact, trailing zeros */
    tempR1 = currX - kTimesPiHi;  /* exact, Sterbenz */
    tempR2 = tempR1 - kTimesPiMi; /* exact or one rounding error */
    r = tempR2 - kTimesPiLo;      /* r = tempR2 if operation yielding
				     tempR2 has not been exact */
    /* Overall, we have 

       * r = (x - k * pi) * (1 + eps), abs(eps) <= 2^-55.3 

       * abs(r) <= 0x3ff921fb544496d2.
       
       We have 

       sin(x) = (-1)^k * sin(x - k * pi) = (-1)^k * sin(r).

       The parity of k is reflected by the last bit of
       shiftedXTimesRcprPi. This last bit is equal to the sign bit of
       (-1)^k.

    */
    signBit.d = shiftedXTimesRcprPi;
    signBit.i <<= 63;
    rc.d = r;
    rc.i = rc.i ^ signBit.i;
    r = rc.d;
    
    /* Now compute an approximation to sin(r) using a polynomial */
    vector_sin_poly(&sinR, r);
    
    /* Write result back */
    y[i] = sinR;
  }
}
