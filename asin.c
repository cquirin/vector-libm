/* This program implements a show-case vector (vectorizable) double
   precision arcsine with a 6 ulp error bound.

   Author: Christoph Lauter,

           Sorbonne Université - LIP6 - PEQUAN team.

   This program is

   Copyright 2014-2018 Christoph Lauter Sorbonne Université

   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions
   are met:

   1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.

   2. Redistributions in binary form must reproduce the above
   copyright notice, this list of conditions and the following
   disclaimer in the documentation and/or other materials provided
   with the distribution.

   3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived
   from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
   FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
   COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
   INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
   (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
   SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
   HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
   STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
   OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#include <stdint.h>
#include "asin.h"

/* Two caster types */
typedef union _dblcast {
  double   d;
  uint64_t i;
} dblcast;

typedef union {
  int64_t l;
  double d;
} db_number;

/* Need fabs and sqrt */
double fabs(double);
double sqrt(double);


/* Some constants */
#define ARCSINE_SPURIOUS_UNDERFLOW_BOUND   3.0549363634996046820519793932136176997894027405723e-151
#define ARCSINE_EASY_RETURN_BOUND          7.450580596923828125e-9
#define ARCSINE_REDUCED_BOUND              5.41825823666468864292511398161877878010272979736328125e-2

/* Coefficients for polynomials used */

#define asin_seed_coeff_1   1.295994705699798021925062130321748554706573486328125
#define asin_seed_coeff_3   -1.248794924583670695739101574872620403766632080078125
#define asin_seed_coeff_5   1.469387402239763940059447122621349990367889404296875

#define asin_sin_coeff_1   0.99999999999999988897769753748434595763683319091796875
#define asin_sin_coeff_3   -0.1666666666666619389669534712083986960351467132568359375
#define asin_sin_coeff_5   8.333333333293758971027642701301374472677707672119140625e-3
#define asin_sin_coeff_7   -1.984126982819289937952367264273334512836299836635589599609375e-4
#define asin_sin_coeff_9   2.75573170691192516194335804247117494014673866331577301025390625e-6
#define asin_sin_coeff_11   -2.50519138083022270956998624018285948267248386400751769542694091796875e-8
#define asin_sin_coeff_13   1.60492458979084981302000128722978246342822927772431285120546817779541015625e-10
#define asin_sin_coeff_15   -7.38854750146316297709408291458454848287841498777339666048646904528141021728515625e-13

#define asin_cos_coeff_0   1.0
#define asin_cos_coeff_2   -0.4999999999999997779553950749686919152736663818359375
#define asin_cos_coeff_4   4.166666666666395124618560430462821386754512786865234375e-2
#define asin_cos_coeff_6   -1.38888888887737384746079438713195486343465745449066162109375e-3
#define asin_cos_coeff_8   2.480158727763067888299162999654612349331728182733058929443359375e-5
#define asin_cos_coeff_10   -2.755731643147327879561701673150064806350201251916587352752685546875e-7
#define asin_cos_coeff_12   2.08765651878857456744168519008515716439688958416809327900409698486328125e-9
#define asin_cos_coeff_14   -1.1463026688060091327723356424079571326546389542500037350691854953765869140625e-11
#define asin_cos_coeff_16   4.6107067538874377191029317102359825133604982749790934803968411870300769805908203125e-14

#define asin_reduced_coeff_1   1.0
#define asin_reduced_coeff_3   0.1666666666665469198616023049908108077943325042724609375
#define asin_reduced_coeff_5   7.500000039139285978873061822014278732240200042724609375e-2
#define asin_reduced_coeff_7   4.464245745627186401360830814155633561313152313232421875e-2
#define asin_reduced_coeff_9   3.05430947622086466919366642969180247746407985687255859375e-2

STATIC INLINE double scalar_asin_callout_inner(double x) {
  double xAbs, xsq;
  double y;
  double q, qsq, c, s, r, rsq, d;

  /* Get absolute value of x for checks */
  xAbs = fabs(x);

  /* If x is smaller than 2^-27 (in magnitude), we can simply return x */
  if (xAbs < ARCSINE_EASY_RETURN_BOUND) {
    return x;
  }

  /* Square x once */
  xsq = x * x;

  /* If x is smaller (in magnitude) than the bound for the reduced
     range polynomial, we can use no other polynomial besides that
     reduced range polynomial.
  */
  if (xAbs < ARCSINE_REDUCED_BOUND) {
    /* Evaluate reduced range polynomial */
    y = x * (asin_reduced_coeff_1 +
      xsq * (asin_reduced_coeff_3 +
      xsq * (asin_reduced_coeff_5 +
      xsq * (asin_reduced_coeff_7 +
      xsq *  asin_reduced_coeff_9))));

    /* Return the result */
    return y;
  }

  /* Main path: we are sure that abs(x) >= ARCSINE_REDUCED_BOUND and that abs(x) <= 1.

     Start by getting a first guess q of asin(x); the accuracy does not matter.

  */
  q = x * (asin_seed_coeff_1 + xsq * (asin_seed_coeff_3 + xsq * asin_seed_coeff_5));

  /* Square first guess once */
  qsq = q * q;

  /* Compute sine s and cosine c of first guess q up to 52.5 bits */
  s = q * (asin_sin_coeff_1 +
    qsq * (asin_sin_coeff_3 +
    qsq * (asin_sin_coeff_5 +
    qsq * (asin_sin_coeff_7 +
    qsq * (asin_sin_coeff_9 +
    qsq * (asin_sin_coeff_11 +
    qsq * (asin_sin_coeff_13 +
    qsq *  asin_sin_coeff_15)))))));

  c =      asin_cos_coeff_0 +
    qsq * (asin_cos_coeff_2 + 
    qsq * (asin_cos_coeff_4 +
    qsq * (asin_cos_coeff_6 +
    qsq * (asin_cos_coeff_8 +
    qsq * (asin_cos_coeff_10 +
    qsq * (asin_cos_coeff_12 +
    qsq * (asin_cos_coeff_14 +
    qsq *  asin_cos_coeff_16)))))));

  /* Compute reduced argument r

     We have 

     asin(x) = asin(s) + asin(x * sqrt(1 - s^2) - s * sqrt(1 - x^2))
             = q       + asin(x * c - s * sqrt(1 - x^2))
             = q       + asin(r) 

     with 

     r = x * c - s * sqrt(1 - x^2)
       = x * c - s * sqrt((1 + x) * (1 - x)) 

     The cancellation around +/- 1 does not matter too much because r (and 
     hence asin(r)) will be small with respect to q in this case.

  */
  r = x * c - s * sqrt((1.0 + x) * (1.0 - x));

  /* Square r once */
  rsq = r * r;

  /* Compute d = asin(r) */
  d = r * (asin_reduced_coeff_1 +
    rsq * (asin_reduced_coeff_3 +
    rsq * (asin_reduced_coeff_5 +
    rsq * (asin_reduced_coeff_7 +
    rsq * asin_reduced_coeff_9))));

  /* Reconstruct asin(x) as asin(x) = q + d */
  y = q + d;
  
  /* Return the result */
  return y;
}

/* A scalar arcsine for the callout */
STATIC INLINE double scalar_asin_callout(double x) {
  dblcast xdb, xAbsdb;
  double yh, yl;
  double temp;

  xdb.d = x;
  xAbsdb.i = xdb.i & 0x7fffffffffffffffull;
  if (xAbsdb.i > 0x3ff0000000000000ull) {
    /* If we are here, the input is larger than 1.0 or Inf or NaN.
       
       Continue with testing if the input is NaN.

    */
    if (xAbsdb.i > 0x7ff0000000000000ull) {
      /* Here the input is NaN, return that (quietized) NaN */
      return 1.0 + x;
    }

    /* Here the input is larger than 1.0, but not NaN.

       Raise invalid and return a NaN.
       
    */
    temp = 0.0;
    return temp / temp;
  }

  /* Here, the input is real and in range. */
  return scalar_asin_callout_inner(x);
}

/* A vector arcsine callout */
STATIC INLINE void vector_asin_callout(double * RESTRICT y, CONST double * RESTRICT x) {
  int i;

  for (i=0;i<VECTOR_LENGTH;i++) {
    y[i] = scalar_asin_callout(x[i]);
  }
}

/* A vector arcsine */
void vector_asin(double * RESTRICT yArg, CONST double * RESTRICT xArg) {
  int i;
  int okaySlots;
  double * RESTRICT y;
  CONST double * RESTRICT x;
  double currX, currY;
  double xsq, q;
  double qsq, s, c, r, rsq, d;
  double xAbs;

  /* Assume alignment */
#ifdef NO_ASSUME_ALIGNED
  x = xArg;
  y = yArg;
#else
  x = __builtin_assume_aligned(xArg, VECTOR_LENGTH * __alignof__(double));
  y = __builtin_assume_aligned(yArg, VECTOR_LENGTH * __alignof__(double));
#endif  

  /* Check if we can handle all inputs */
  okaySlots = 0;
  for (i=0;i<VECTOR_LENGTH;i++) {
    xAbs = fabs(x[i]);
    okaySlots += ((xAbs < 1.0) & (xAbs >= ARCSINE_SPURIOUS_UNDERFLOW_BOUND)); 
  }

  /* Perform a callout if we cannot handle the input in one slot */
  if (okaySlots != VECTOR_LENGTH) {
    vector_asin_callout(yArg, xArg);
    return;
  }

  /* Here we know that all inputs are real and in range. */
  for (i=0;i<VECTOR_LENGTH;i++) {
    /* Get current element in vector */
    currX = x[i];

    /* Square x once */
    xsq = currX * currX;

    /* Get a first guess qq of asin(x); the accuracy does not matter */
    q = currX * (asin_seed_coeff_1 + xsq * (asin_seed_coeff_3 + xsq * asin_seed_coeff_5));
	  
    /* Square first guess once */
    qsq = q * q;

    /* Compute s = sin(q) and c = cos(q) - 1 of first guess q.

       We typically need s and c + 1 to be approximations to 
       sin(x) and cos(x) up to 52.5 bits.

    */
    s = q * (asin_sin_coeff_1 +
      qsq * (asin_sin_coeff_3 +
      qsq * (asin_sin_coeff_5 +
      qsq * (asin_sin_coeff_7 +
      qsq * (asin_sin_coeff_9 +
      qsq * (asin_sin_coeff_11 +
      qsq * (asin_sin_coeff_13 +
      qsq *  asin_sin_coeff_15)))))));

    c = qsq * (asin_cos_coeff_2 + 
        qsq * (asin_cos_coeff_4 +
        qsq * (asin_cos_coeff_6 +
        qsq * (asin_cos_coeff_8 +
        qsq * (asin_cos_coeff_10 +
        qsq * (asin_cos_coeff_12 +
        qsq * (asin_cos_coeff_14 +
        qsq *  asin_cos_coeff_16)))))));

    /* Compute reduced argument r

       We have 

       asin(x) = asin(s) + asin(x * sqrt(1 - s^2) - s * sqrt(1 - x^2))
               = q       + asin(x * (c + 1) - s * sqrt(1 - x^2))
               = q       + asin(r) 

       with 

       r = x * (c + 1) - s * sqrt(1 - x^2) 
         = (x - s) + (x * c - s * (sqrt((1 + x) * (1 - x)) - 1))
         
       The cancellation around +/- 1 does not matter too much because r (and 
       hence asin(r)) will be small with respect to q in this case.

    */
    r = (currX - s) + currX * c - s * (sqrt((1.0 + currX) * (1.0 - currX)) - 1.0);

    /* Square r once */
    rsq = r * r;

    /* Compute d = asin(r) */
    d = r * (asin_reduced_coeff_1 +
      rsq * (asin_reduced_coeff_3 +
      rsq * (asin_reduced_coeff_5 +
      rsq * (asin_reduced_coeff_7 +
      rsq * asin_reduced_coeff_9))));

    /* Reconstruct asin(x) as asin(x) = q + d */
    currY = q + d;

    /* Write result back into output vector */
    y[i] = currY;
  }
}
